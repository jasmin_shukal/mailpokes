<?php
  include_once 'config.php';

  $t=time();
  $filename= "upload-file-".$t;

  if ( 0 < $_FILES['file']['error'] ) {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
  }
  else {
      $ext=explode('.',$_FILES['file']['name'])[1];
      move_uploaded_file($_FILES['file']['tmp_name'], UPLOADS_DIRECTORY.$_GET['id']."/".$filename.'.'.$ext);
  }

?>
