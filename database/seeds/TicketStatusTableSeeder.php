<?php

use App\Models\TicketStatus\TicketStatus;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TicketStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        TicketStatus::insert([
                ['status' => 'Answered', 'created_at' => '2016-09-03 14:49:04', 'updated_at' => '2016-09-03 14:49:04'],
                ['status' => 'Closed', 'created_at' => '2016-09-03 14:49:04', 'updated_at' => '2016-09-03 14:49:04'],
                ['status' => 'Open', 'created_at' => '2016-09-03 14:49:04', 'updated_at' => '2016-09-03 14:49:04'],
                ['status' => 'In Progress', 'created_at' => '2016-09-03 14:49:04', 'updated_at' => '2016-09-03 14:49:04'],

                ]);
    }
}
