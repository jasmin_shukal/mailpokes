<?php

use Illuminate\Database\Seeder;
use App\Models\Package\Package;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::insert([

                array('name' => 'free','validity' => '30','limit' => '1000', 'user_limit' => '1', 'price' => '0','created_at' => '2016-09-03 14:48:31','updated_at' => '2016-09-03 14:48:31'),
                array('name' => 'silver','validity' => '60','limit' => '5000', 'user_limit' => '5', 'price' => '5','created_at' => '2016-09-03 14:48:48','updated_at' => '2016-09-03 14:48:48'),
                array('name' => 'gold','validity' => '90','limit' => '50000', 'user_limit' => '10', 'price' => '10','created_at' => '2016-09-03 14:49:04','updated_at' => '2016-09-03 14:49:04'),
                array('name' => 'platinum','validity' => '120','limit' => '100000', 'user_limit' => '15', 'price' => '15','created_at' => '2016-09-03 14:49:04','updated_at' => '2016-09-03 14:49:04')

        ]);
    }
}
