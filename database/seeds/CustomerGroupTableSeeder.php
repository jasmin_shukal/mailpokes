<?php

use App\Models\CustomerGroup\CustomerGroup;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CustomerGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        CustomerGroup::insert([
            ['name' => 'Individual'],
            ['name' => 'Company']
        ]);
    }
}