<?php

use App\Models\Department\Department;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Department::insert(array(
                      array('name' => 'Mail Complain','created_at' => '2017-04-23 13:24:24','updated_at' => '2017-04-23 13:24:24'),
                      array('name' => 'Modarator','created_at' => '2017-04-23 13:24:46','updated_at' => '2017-04-23 13:24:46'),
                      array('name' => 'Ticket Complain','created_at' => '2017-04-23 13:25:09','updated_at' => '2017-04-23 13:25:09'),
                      array('name' => 'Software Development','created_at' => '2017-04-23 13:26:00','updated_at' => '2017-04-23 13:26:00')
                    ));
    }
}
