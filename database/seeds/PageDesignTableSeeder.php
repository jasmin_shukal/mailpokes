<?php

use Illuminate\Database\Seeder;
use App\Models\PageDesign\PageDesign;
use Illuminate\Database\Eloquent\Model;

class PageDesignTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        PageDesign::insert([
            ['user_id' => 1, 'name' => 'sign up', 'html_template' => '', 
            'template' => '<p style=""><br></p><h1 class="size-30" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #b59859;font-size: 26px;line-height: 34px;text-align: center;" lang="x-size-30">— Thanks for Sign Up —</h1><p><br></p><p><br></p><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16"><strong>Hello {USERNAME}</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16"><span style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #cca95e;font-family: "PT Serif", Georgia, serif;">Love you!</span> <br></p><div style="Margin-left: 20px;Margin-right: 20px;">      <div style="line-height:5px;font-size:1px">&nbsp;</div></div>',
            'show_hide' => 1
        ],
            ['user_id' => 1, 'name' => 'opt-in', 'html_template' => '', 
            'template' => '<p style=""><br></p><h1 class="size-30" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #b59859;font-size: 26px;line-height: 34px;text-align: center;" lang="x-size-30">— Thanks for subscribing —</h1><p><br></p><p><br></p><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16"><strong>Hello {USERNAME}</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16"><span style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #cca95e;font-family: "PT Serif", Georgia, serif;">Love you!</span> <br></p><div style="Margin-left: 20px;Margin-right: 20px;">      <div style="line-height:5px;font-size:1px">&nbsp;</div></div>',
            'show_hide' => 1
        ],
        ]);
    }
}
