<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        User::insert([
                ['name' => 'John Doe','email' => 'support@xcoder.io','address'=>"<hr/><h3>Please update our physical address</h3><p>xCoder Street<br /> 28 2nd Street<br /> 3rd Floor #93923<br /> San Francisco, CA 90010</p>",'status' => 'Active','password' => '$2y$10$K4wHIQ/wuHHqlx5LH7nOveaUDrqY/0d6AZfj7n6mzY9RwQ7csHdFO', 'role' => 'superadmin', 'mail_driver'=> 'smtp', 'api_key' => '08ff51748bfe45a4bb02c6ec89c8e37f'],
                ['name' => 'David Backhum','email' => 'david@backhum.com','address'=>"<hr/><h3>Please update our physical address</h3><p>xCoder Street<br /> 28 2nd Street<br /> 3rd Floor #93923<br /> San Francisco, CA 90010</p>",'status' => 'Active','password' => '$2y$10$K4wHIQ/wuHHqlx5LH7nOveaUDrqY/0d6AZfj7n6mzY9RwQ7csHdFO', 'role' => '','mail_driver'=> 'mail', 'api_key' => 'a23527ea32ef4471ad70dca2daea8d30'],
                ['name' => 'data nardo','email' => 'data@nardo.com','address'=>"<hr/><h3>Please update our physical address</h3><p>xCoder Street<br /> 28 2nd Street<br /> 3rd Floor #93923<br /> San Francisco, CA 90010</p>",'status' => 'Banned','password' => '$2y$10$H.OJx5i6K59BwWMVBPI4suVuXo/nWYgD7tTjpjqvG.xbWGQmMrsqW', 'role' => '','mail_driver'=> 'mail', 'api_key' => 'fbbc8d4ef7fb40aab922dc1c9e568115'],
                ['name' => 'demo','email' => 'demo@grameentek.com','address'=>"<hr/><h3>Please update our physical address</h3><p>xCoder Street<br /> 28 2nd Street<br /> 3rd Floor #93923<br /> San Francisco, CA 90010</p>",'status' => 'Banned','password' => '$2y$10$PlHX6h5So1F00cg1p6ozYegeqE0j96J4cMnIm8sG/dS2XgUh8IZZ2', 'role' => '','mail_driver'=> 'mail', 'api_key' => 'ff8703cb8ad34accbad8ea9fc21bc45d'],
                ['name' => 'pola','email' => 'pola@pola.com','address'=>"<hr/><h3>Please update our physical address</h3><p>xCoder Street<br /> 28 2nd Street<br /> 3rd Floor #93923<br /> San Francisco, CA 90010</p>",'status' => 'Active','password' => '$2y$10$oqUUhTCQwAH5q93XsKdPFON/LYaOOZJlngz45jgAyNlxb9D6JpyCS', 'role' => '','mail_driver'=> 'mail', 'api_key' => 'fe8c5a92724b41caa916d07b7be9428d']

                ]);
    }
}
