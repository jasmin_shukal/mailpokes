<?php

use Illuminate\Database\Seeder;
use App\Models\Email\MailProvider;

class MailProviderTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	  MailProvider::insert([
		      array('id' => '1','name' => 'Amazon API','provider' => 'ses','credentials' => '["key","secret","region"]'),
			  array('id' => '2','name' => 'Amazon SMTP','provider' => 'smtp','credentials' => '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
			  array('id' => '3','name' => 'Mailgun API','provider' => 'mailgun','credentials' => '["domain","secret"]'),
			  array('id' => '4','name' => 'Mailgun SMTP','provider' => 'smtp','credentials' => '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
			  array('id' => '5','name' => 'Sparkpost','provider' => 'sparkpost','credentials' => '["secret"]'),
			  array('id' => '6','name' => 'Sparkpost SMTP','provider' => 'smtp','credentials' => '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
			  array('id' => '7','name' => 'SMTP','provider' => 'smtp','credentials' => '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
			  array('id' => '8','name' => 'Elastic Email API','provider' => 'elastic','credentials' => '["key"]'),
			  array('id' => '9','name' => 'SendGrid API','provider' => 'sendgrid','credentials' => '["key"]'),
			  array('id' => '10','name' => 'PHP Mail','provider' => 'mail','credentials' => '')
		  ]);
	}
}
