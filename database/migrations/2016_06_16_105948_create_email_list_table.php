<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned()->nullable();
//            $table->foreign('group_id')->references('id')->on('group')->onDelete('cascade');
            $table->string('name', 70)->nullable();
            $table->string('email', 190)->nullable();
            $table->tinyInteger('subscribed')->nullable();
            $table->tinyInteger('free_email_check')->nullable();
            $table->text('free_email_value')->nullable();
            $table->tinyInteger('bulk_check')->nullable();
            $table->text('bulk_value')->nullable();
            $table->tinyInteger('email_list_check')->nullable();
            $table->text('email_list_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_list');
    }
}
