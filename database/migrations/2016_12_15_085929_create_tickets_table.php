<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 190)->nullable();
            $table->string('subject', 190)->nullable();
            $table->integer('reporter')->unsigned()->nullable();
            $table->string('priority', 190)->nullable();
            $table->integer('department_id')->unsigned()->nullable();
            $table->text('reason')->nullable();
            $table->text('attachments')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('status')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
