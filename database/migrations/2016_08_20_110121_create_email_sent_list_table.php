<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSentListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_sent_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_history_id')->unsigned()->nullable();
            $table->string('email', 190)->nullable();
            $table->tinyInteger('seen_status')->nullable();
            $table->tinyInteger('link_open')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_sent_list');
    }
}
