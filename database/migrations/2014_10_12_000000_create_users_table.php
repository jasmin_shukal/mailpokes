<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 190)->nullable();
            $table->string('email', 190)->nullable();
            $table->integer('group_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->string('department_id_list', 190)->nullable();
            $table->string('status', 50)->nullable();
            $table->string('password', 60)->nullable();
            $table->string('address', 190)->nullable();
            $table->string('platform', 190)->nullable();
            $table->text('oauth_user_id')->nullable();
            $table->string('photo', 190)->nullable();
            $table->string('role', 10)->nullable();
            $table->string('mail_driver', 50)->default('mail')->nullable();
            $table->string('api_key', 190)->nullable();
            $table->integer('package_id')->unsigned()->nullable();
            $table->integer('email_limit')->unsigned()->nullable();
            $table->integer('user_limit')->unsigned()->nullable();
            $table->string('package_activated_at', 190)->nullable();
            $table->string('from_email', 190)->nullable();
            $table->string('sender_name', 190)->nullable();
            $table->string('domain', 190)->nullable();
            $table->string('DKIM', 190)->nullable();
            $table->string('SPF', 190)->nullable();
            $table->integer('email_settings_id')->unsigned()->nullable();
            $table->rememberToken()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
