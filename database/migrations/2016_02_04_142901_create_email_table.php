<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('domain', 110)->nullable();
            $table->string('key', 110)->nullable();
            $table->string('secret', 110)->nullable();
            $table->string('region')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('mail_provider_id')->unsigned()->nullable();
            $table->string('smtp_host', 110)->nullable();
            $table->string('smtp_username', 110)->nullable();
            $table->string('smtp_password', 110)->nullable();
            $table->string('smtp_port', 6)->nullable();
            $table->string('encryption', 200)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('from_email', 100)->nullable();
            $table->string('reply_to', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_settings');
    }
}
