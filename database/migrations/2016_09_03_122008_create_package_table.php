<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120)->nullable();
            $table->string('validity', 120)->nullable();
            $table->integer('limit')->nullable();
            $table->integer('user_limit')->nullable();
            $table->integer('price')->nullable();
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::drop('package');
    }
}
