<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailLaterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_later', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sender_id')->unsigned()->nullable();
            $table->text('email_list')->nullable();
            $table->integer('template_id')->unsigned()->nullable();
            $table->string('subject', 190)->nullable();
            $table->string('send_time', 50)->nullable();
            $table->text('attachment_list')->nullable();
            $table->string('url', 190)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_later');
    }
}
