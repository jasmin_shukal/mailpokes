<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 70)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('free_email_check')->nullable();
            $table->string('free_email_check_date', 100)->nullable();
            $table->tinyInteger('bulk_check')->nullable();
            $table->string('bulk_check_date', 100)->nullable();
            $table->tinyInteger('email_list_check')->nullable();
            $table->text('email_list_verify_date', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('group');
    }
}
