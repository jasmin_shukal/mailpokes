-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 01, 2018 at 12:46 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1-log
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `postman`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

CREATE TABLE `api` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blacklists`
--

CREATE TABLE `blacklists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `for` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `user_id` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` text COLLATE utf8mb4_unicode_ci,
  `value` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `key`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'Email Marketing', NULL, NULL, NULL),
(2, 'company_email', 'support@xcoder.io', NULL, NULL, NULL),
(3, 'company_address', '120, Lake Circus, Maryland, USA', NULL, NULL, NULL),
(4, 'sidebar_theme', 'red-light', NULL, NULL, NULL),
(5, 'privacy', 'Please update you privacy policy. This is dummy text.', NULL, NULL, NULL),
(6, 'termCondition', '<p>Please update you Terms And Condition. This is dummy text. Once you <a href=\'/privacy\'>update</a>, your content will appear here</p>\n', NULL, NULL, NULL),
(7, 'QUEUE_DRIVER', 'sync', NULL, NULL, NULL),
(8, 'copyright', 'Copyright © 2017 <a href=\'//xcoder.io\'>xcoder.io</a>', NULL, NULL, NULL),
(9, 'cron', '', NULL, NULL, NULL),
(10, 'disqusShortname', 'postman-1', NULL, NULL, NULL),
(11, 'offline_message', 'This is under maintenance', NULL, NULL, NULL),
(12, 'enable_registration', '1', NULL, NULL, NULL),
(13, 'enable_social_login', '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_type` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_groups`
--

CREATE TABLE `customer_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_groups`
--

INSERT INTO `customer_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Individual', NULL, NULL),
(2, 'Company', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Mail Complain', '2017-04-23 07:24:24', '2017-04-23 07:24:24'),
(2, 'Modarator', '2017-04-23 07:24:46', '2017-04-23 07:24:46'),
(3, 'Ticket Complain', '2017-04-23 07:25:09', '2017-04-23 07:25:09'),
(4, 'Software Development', '2017-04-23 07:26:00', '2017-04-23 07:26:00');

-- --------------------------------------------------------

--
-- Table structure for table `email_history`
--

CREATE TABLE `email_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED DEFAULT NULL,
  `template_id` int(10) UNSIGNED DEFAULT NULL,
  `attachment_list` text COLLATE utf8mb4_unicode_ci,
  `subject` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_later`
--

CREATE TABLE `email_later` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED DEFAULT NULL,
  `email_list` text COLLATE utf8mb4_unicode_ci,
  `template_id` int(10) UNSIGNED DEFAULT NULL,
  `subject` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_list` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_list`
--

CREATE TABLE `email_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscribed` tinyint(4) DEFAULT NULL,
  `free_email_check` tinyint(4) DEFAULT NULL,
  `free_email_value` text COLLATE utf8mb4_unicode_ci,
  `bulk_check` tinyint(4) DEFAULT NULL,
  `bulk_value` text COLLATE utf8mb4_unicode_ci,
  `email_list_check` tinyint(4) DEFAULT NULL,
  `email_list_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_sent_list`
--

CREATE TABLE `email_sent_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_history_id` int(10) UNSIGNED DEFAULT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seen_status` tinyint(4) DEFAULT NULL,
  `link_open` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE `email_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `domain` varchar(110) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(110) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` varchar(110) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `mail_provider_id` int(10) UNSIGNED DEFAULT NULL,
  `smtp_host` varchar(110) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_username` varchar(110) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_password` varchar(110) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_port` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encryption` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_to` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_faq` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_end`
--

CREATE TABLE `front_end` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_end`
--

INSERT INTO `front_end` (`id`, `menu`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Poster', '<section id="cta2">\n    <div class="container">\n        <div class="text-center">\n            <h2 class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms"><span>Postman</span> is an Email Marketing Tool</h2>\n            <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="100ms">You can send mail with any SMTP credentials or other email provider service like <a href="https://www.mailgun.com" target="_blank">Mailgun</a> <a  target="_blank" href="https://aws.amazon.com/ses/">Amazon SES</a> <a href="https://elasticemail.com/" target="_blank">Elastic Mail</a> <a href="https://sendgrid.com/"  target="_blank">SendGrid</a> <a href="https://www.sparkpost.com/" target="_blank">SparkPost</a></p>\n            <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="200ms"><a class="btn btn-primary btn-lg" href="#pricing">Pricing</a></p>\n            <img class="img-responsive wow fadeIn" src="{{ asset(\'plugins/pricing/images/cta2/cta2-img.png\') }}" alt="" data-wow-duration="300ms" data-wow-delay="300ms">\n        </div>\n    </div>\n</section>', NULL, '2017-05-06 05:37:47', '2017-05-06 05:37:47'),
(2, 'Slogan', '<section id="cta" class="wow fadeIn">\n    <div class="container">\n        <div class="row">\n            <div class="col-sm-9">\n                <h2>Premium quality email marketing service</h2>\n                <p>Forget expensive email services like Mailchimp, ActiveCampaign or your own server setup. Use our email sending platform with your any SMTP credentials. \n                </p>\n            </div>\n            <div class="col-sm-3 text-right">\n                <a class="btn btn-primary btn-lg" href="#pricing">Start Now!</a>\n            </div>\n        </div>\n    </div>\n</section><!--/#cta-->', NULL, '2017-05-06 05:38:15', '2017-05-06 05:38:15'),
(3, 'Our Works', '<section id="portfolio">\n    <div class="container">\n        <div class="section-header">\n            <h2 class="section-title text-center wow fadeInDown">Our Works</h2>\n            <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>\n        </div>\n\n        <div class="text-center">\n            <ul class="portfolio-filter">\n                <li><a class="active" href="#" data-filter="*">All Works</a></li>\n                <li><a href="#" data-filter=".creative">Creative</a></li>\n                <li><a href="#" data-filter=".corporate">Corporate</a></li>\n                <li><a href="#" data-filter=".portfolio">Portfolio</a></li>\n            </ul><!--/#portfolio-filter-->\n        </div>\n\n        <div class="portfolio-items">\n            <div class="portfolio-item creative">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/01.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 1</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n\n            <div class="portfolio-item corporate portfolio">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/02.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 2</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n\n            <div class="portfolio-item creative">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/03.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 3</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n\n            <div class="portfolio-item corporate">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/04.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 4</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n\n            <div class="portfolio-item creative portfolio">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/05.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 5</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n\n            <div class="portfolio-item corporate">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/06.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 5</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n\n            <div class="portfolio-item creative portfolio">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/07.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 7</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n\n            <div class="portfolio-item corporate">\n                <div class="portfolio-item-inner">\n                    <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/portfolio/08.jpg\') }}" alt="">\n                    <div class="portfolio-info">\n                        <h3>Portfolio Item 8</h3>\n                        Lorem Ipsum Dolor Sit\n                        <a class="preview" href="{{ asset(\'plugins/pricing/images/portfolio/full.jpg\') }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>\n                    </div>\n                </div>\n            </div><!--/.portfolio-item-->\n        </div>\n    </div><!--/.container-->\n</section><!--/#portfolio-->', NULL, '2017-05-06 05:38:59', '2017-05-06 05:38:59'),
(4, 'About', '<section id="about">\n    <div class="container">\n\n        <div class="section-header">\n            <h2 class="section-title text-center wow fadeInDown">About Us</h2>\n            <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>\n        </div>\n\n        <div class="row">\n            <div class="col-sm-6 wow fadeInLeft">\n                <h3 class="column-title">Video Intro</h3>\n                <!-- 16:9 aspect ratio -->\n                <div class="embed-responsive embed-responsive-16by9">\n                    <iframe src="//player.vimeo.com/video/58093852?title=0&amp;byline=0&amp;portrait=0&amp;color=e79b39" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n                </div>\n            </div>\n\n            <div class="col-sm-6 wow fadeInRight">\n                <h3 class="column-title">xCoder Capability</h3>\n                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n\n                <p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n\n                <div class="row">\n                    <div class="col-sm-6">\n                        <ul class="nostyle">\n                            <li><i class="fa fa-check-square"></i> Ipsum is simply dummy</li>\n                            <li><i class="fa fa-check-square"></i> When an unknown</li>\n                        </ul>\n                    </div>\n\n                    <div class="col-sm-6">\n                        <ul class="nostyle">\n                            <li><i class="fa fa-check-square"></i> The printing and typesetting</li>\n                            <li><i class="fa fa-check-square"></i> Lorem Ipsum has been</li>\n                        </ul>\n                    </div>\n                </div>\n\n                <a class="btn btn-primary" href="#">Learn More</a>\n\n            </div>\n        </div>\n    </div>\n</section><!--/#about-->', NULL, '2017-05-06 05:45:52', '2017-05-06 05:45:52'),
(5, 'Work Process', '<section id="work-process">\n    <div class="container">\n        <div class="section-header">\n            <h2 class="section-title text-center wow fadeInDown">Our Process</h2>\n            <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>\n        </div>\n\n        <div class="row text-center">\n            <div class="col-md-2 col-md-4 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">\n                    <div class="icon-circle">\n                        <span>1</span>\n                        <i class="fa fa-coffee fa-2x"></i>\n                    </div>\n                    <h3>MEET</h3>\n                </div>\n            </div>\n            <div class="col-md-2 col-md-4 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">\n                    <div class="icon-circle">\n                        <span>2</span>\n                        <i class="fa fa-bullhorn fa-2x"></i>\n                    </div>\n                    <h3>PLAN</h3>\n                </div>\n            </div>\n            <div class="col-md-2 col-md-4 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">\n                    <div class="icon-circle">\n                        <span>3</span>\n                        <i class="fa fa-image fa-2x"></i>\n                    </div>\n                    <h3>DESIGN</h3>\n                </div>\n            </div>\n            <div class="col-md-2 col-md-4 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">\n                    <div class="icon-circle">\n                        <span>4</span>\n                        <i class="fa fa-heart fa-2x"></i>\n                    </div>\n                    <h3>DEVELOP</h3>\n                </div>\n            </div>\n            <div class="col-md-2 col-md-4 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">\n                    <div class="icon-circle">\n                        <span>5</span>\n                        <i class="fa fa-shopping-cart fa-2x"></i>\n                    </div>\n                    <h3>TESTING</h3>\n                </div>\n            </div>\n            <div class="col-md-2 col-md-4 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms">\n                    <div class="icon-circle">\n                        <span>6</span>\n                        <i class="fa fa-space-shuttle fa-2x"></i>\n                    </div>\n                    <h3>LAUNCH</h3>\n                </div>\n            </div>\n        </div>\n    </div>\n</section><!--/#work-process-->', NULL, '2017-05-06 05:46:38', '2017-05-06 05:46:38'),
(6, 'Meet Team', '<section id="meet-team">\n    <div class="container">\n        <div class="section-header">\n            <h2 class="section-title text-center wow fadeInDown">Meet The Team</h2>\n            <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>\n        </div>\n\n        <div class="row">\n            <div class="col-sm-6 col-md-3">\n                <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">\n                    <div class="team-img">\n                        <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/team/01.jpg\') }}" alt="">\n                    </div>\n                    <div class="team-info">\n                        <h3>Bin Burhan</h3>\n                        <span>Co-Founder</span>\n                    </div>\n                    <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>\n                    <ul class="social-icons">\n                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>\n                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>\n                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>\n                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>\n                    </ul>\n                </div>\n            </div>\n            <div class="col-sm-6 col-md-3">\n                <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">\n                    <div class="team-img">\n                        <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/team/02.jpg\') }}" alt="">\n                    </div>\n                    <div class="team-info">\n                        <h3>Jane Man</h3>\n                        <span>Project Manager</span>\n                    </div>\n                    <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>\n                    <ul class="social-icons">\n                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>\n                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>\n                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>\n                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>\n                    </ul>\n                </div>\n            </div>\n            <div class="col-sm-6 col-md-3">\n                <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">\n                    <div class="team-img">\n                        <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/team/03.jpg\') }}" alt="">\n                    </div>\n                    <div class="team-info">\n                        <h3>Pahlwan</h3>\n                        <span>Designer</span>\n                    </div>\n                    <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>\n                    <ul class="social-icons">\n                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>\n                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>\n                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>\n                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>\n                    </ul>\n                </div>\n            </div>\n            <div class="col-sm-6 col-md-3">\n                <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">\n                    <div class="team-img">\n                        <img class="img-responsive" src="{{ asset(\'plugins/pricing/images/team/04.jpg\') }}" alt="">\n                    </div>\n                    <div class="team-info">\n                        <h3>Nasir uddin</h3>\n                        <span>UI/UX</span>\n                    </div>\n                    <p>Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater</p>\n                    <ul class="social-icons">\n                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>\n                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>\n                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>\n                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n\n        <div class="divider"></div>\n\n        <div class="row">\n            <div class="col-sm-4">\n                <h3 class="column-title">Our Skills</h3>\n                <strong>GRAPHIC DESIGN</strong>\n                <div class="progress">\n                    <div class="progress-bar progress-bar-primary" role="progressbar" data-width="85">85%</div>\n                </div>\n                <strong>WEB DESIGN</strong>\n                <div class="progress">\n                    <div class="progress-bar progress-bar-primary" role="progressbar" data-width="70">70%</div>\n                </div>\n                <strong>WORDPRESS DEVELOPMENT</strong>\n                <div class="progress">\n                    <div class="progress-bar progress-bar-primary" role="progressbar" data-width="90">90%</div>\n                </div>\n                <strong>JOOMLA DEVELOPMENT</strong>\n                <div class="progress">\n                    <div class="progress-bar progress-bar-primary" role="progressbar" data-width="65">65%</div>\n                </div>\n            </div>\n\n            <div class="col-sm-4">\n                <h3 class="column-title">Our History</h3>\n                <div role="tabpanel">\n                    <ul class="nav main-tab nav-justified" role="tablist">\n                        <li role="presentation" class="active">\n                            <a href="#tab1" role="tab" data-toggle="tab" aria-controls="tab1" aria-expanded="true">2010</a>\n                        </li>\n                        <li role="presentation">\n                            <a href="#tab2" role="tab" data-toggle="tab" aria-controls="tab2" aria-expanded="false">2011</a>\n                        </li>\n                        <li role="presentation">\n                            <a href="#tab3" role="tab" data-toggle="tab" aria-controls="tab3" aria-expanded="false">2013</a>\n                        </li>\n                        <li role="presentation">\n                            <a href="#tab4" role="tab" data-toggle="tab" aria-controls="tab4" aria-expanded="false">2014</a>\n                        </li>\n                    </ul>\n                    <div id="tab-content" class="tab-content">\n                        <div role="tabpanel" class="tab-pane fade active in" id="tab1" aria-labelledby="tab1">\n                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>\n                            <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters readable English.</p>\n                        </div>\n                        <div role="tabpanel" class="tab-pane fade" id="tab2" aria-labelledby="tab2">\n                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>\n                            <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters readable English.</p>\n                        </div>\n                        <div role="tabpanel" class="tab-pane fade" id="tab3" aria-labelledby="tab3">\n                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>\n                            <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters readable English.</p>\n                        </div>\n                        <div role="tabpanel" class="tab-pane fade" id="tab4" aria-labelledby="tab3">\n                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>\n                            <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters readable English.</p>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <div class="col-sm-4">\n                <h3 class="column-title">Faqs</h3>\n                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">\n                    <div class="panel panel-default">\n                        <div class="panel-heading" role="tab" id="headingOne">\n                            <h4 class="panel-title">\n                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">\n                                    Enim eiusmod high life accusamus\n                                </a>\n                            </h4>\n                        </div>\n                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">\n                            <div class="panel-body">\n                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum.\n                            </div>\n                        </div>\n                    </div>\n                    <div class="panel panel-default">\n                        <div class="panel-heading" role="tab" id="headingTwo">\n                            <h4 class="panel-title">\n                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">\n                                    Nihil anim keffiyeh helvetica\n                                </a>\n                            </h4>\n                        </div>\n                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">\n                            <div class="panel-body">\n                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum.\n                            </div>\n                        </div>\n                    </div>\n                    <div class="panel panel-default">\n                        <div class="panel-heading" role="tab" id="headingThree">\n                            <h4 class="panel-title">\n                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">\n                                    Vegan excepteur butcher vice lomo\n                                </a>\n                            </h4>\n                        </div>\n                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">\n                            <div class="panel-body">\n                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum.\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n</section><!--/#meet-team-->', NULL, '2017-05-06 05:47:19', '2017-05-06 05:47:19'),
(7, 'Animation Number', '<section id="animated-number">\n    <div class="container">\n        <div class="section-header">\n            <h2 class="section-title text-center wow fadeInDown">Fun Facts</h2>\n            <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>\n        </div>\n\n        <div class="row text-center">\n            <div class="col-sm-3 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">\n                    <div class="animated-number" data-digit="2305" data-duration="1000"></div>\n                    <strong>CUPS OF COFFEE CONSUMED</strong>\n                </div>\n            </div>\n            <div class="col-sm-3 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">\n                    <div class="animated-number" data-digit="1231" data-duration="1000"></div>\n                    <strong>CLIENT WORKED WITH</strong>\n                </div>\n            </div>\n            <div class="col-sm-3 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">\n                    <div class="animated-number" data-digit="3025" data-duration="1000"></div>\n                    <strong>PROJECT COMPLETED</strong>\n                </div>\n            </div>\n            <div class="col-sm-3 col-xs-6">\n                <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">\n                    <div class="animated-number" data-digit="1199" data-duration="1000"></div>\n                    <strong>QUESTIONS ANSWERED</strong>\n                </div>\n            </div>\n        </div>\n    </div>\n</section><!--/#animated-number-->', NULL, '2017-05-06 05:48:00', '2017-05-06 05:48:00'),
(8, 'Footer', '<footer id="footer">\n    <div class="container">\n        <div class="row">\n            <div class="col-sm-6">\n                &copy; 2017<a target="_blank" href="http://xcoder.io">xCoder.io</a>\n            </div>\n            <div class="col-sm-6">\n                <ul class="social-icons">\n                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>\n                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>\n                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>\n                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>\n                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>\n                    <li><a href="#"><i class="fa fa-behance"></i></a></li>\n                    <li><a href="#"><i class="fa fa-flickr"></i></a></li>\n                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>\n                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>\n                    <li><a href="#"><i class="fa fa-github"></i></a></li>\n                </ul>\n            </div>\n        </div>\n    </div>\n</footer><!--/#footer-->', NULL, '2017-05-06 05:53:40', '2017-05-06 05:53:40');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `free_email_check` tinyint(4) DEFAULT NULL,
  `free_email_check_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bulk_check` tinyint(4) DEFAULT NULL,
  `bulk_check_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_list_check` tinyint(4) DEFAULT NULL,
  `email_list_verify_date` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `locale`, `status`, `created_at`, `updated_at`) VALUES
(1, 'en_US', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locales`
--

CREATE TABLE `locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locales`
--

INSERT INTO `locales` (`id`, `locale`, `code`, `language`, `name`) VALUES
(1, 'aa_DJ', 'aa', 'afar', 'Afar (Djibouti)'),
(2, 'aa_ER', 'aa', 'afar', 'Afar (Eritrea)'),
(3, 'aa_ET', 'aa', 'afar', 'Afar (Ethiopia)'),
(4, 'af_ZA', 'af', 'afrikaans', 'Afrikaans (South Africa)'),
(5, 'am_ET', 'am', 'amharic', 'Amharic (Ethiopia)'),
(6, 'an_ES', 'an', 'aragonese', 'Aragonese (Spain)'),
(7, 'ar_AE', 'ar', 'arabic', 'Arabic (United Arab Emirates)'),
(8, 'ar_BH', 'ar', 'arabic', 'Arabic (Bahrain)'),
(9, 'ar_DZ', 'ar', 'arabic', 'Arabic (Algeria)'),
(10, 'ar_EG', 'ar', 'arabic', 'Arabic (Egypt)'),
(11, 'ar_IN', 'ar', 'arabic', 'Arabic (India)'),
(12, 'ar_IQ', 'ar', 'arabic', 'Arabic (Iraq)'),
(13, 'ar_JO', 'ar', 'arabic', 'Arabic (Jordan)'),
(14, 'ar_KW', 'ar', 'arabic', 'Arabic (Kuwait)'),
(15, 'ar_LB', 'ar', 'arabic', 'Arabic (Lebanon)'),
(16, 'ar_LY', 'ar', 'arabic', 'Arabic (Libya)'),
(17, 'ar_MA', 'ar', 'arabic', 'Arabic (Morocco)'),
(18, 'ar_OM', 'ar', 'arabic', 'Arabic (Oman)'),
(19, 'ar_QA', 'ar', 'arabic', 'Arabic (Qatar)'),
(20, 'ar_SA', 'ar', 'arabic', 'Arabic (Saudi Arabia)'),
(21, 'ar_SD', 'ar', 'arabic', 'Arabic (Sudan)'),
(22, 'ar_SY', 'ar', 'arabic', 'Arabic (Syria)'),
(23, 'ar_TN', 'ar', 'arabic', 'Arabic (Tunisia)'),
(24, 'ar_YE', 'ar', 'arabic', 'Arabic (Yemen)'),
(25, 'ast_ES', 'ast', 'asturian', 'Asturian (Spain)'),
(26, 'as_IN', 'as', 'assamese', 'Assamese (India)'),
(27, 'az_AZ', 'az', 'azerbaijani', 'Azerbaijani (Azerbaijan)'),
(28, 'az_TR', 'az', 'azerbaijani', 'Azerbaijani (Turkey)'),
(29, 'bem_ZM', 'bem', 'bemba', 'Bemba (Zambia)'),
(30, 'ber_DZ', 'ber', 'berber', 'Berber (Algeria)'),
(31, 'ber_MA', 'ber', 'berber', 'Berber (Morocco)'),
(32, 'be_BY', 'be', 'belarusian', 'Belarusian (Belarus)'),
(33, 'bg_BG', 'bg', 'bulgarian', 'Bulgarian (Bulgaria)'),
(34, 'bn_BD', 'bn', 'bengali', 'Bengali (Bangladesh)'),
(35, 'bn_IN', 'bn', 'bengali', 'Bengali (India)'),
(36, 'bo_CN', 'bo', 'tibetan', 'Tibetan (China)'),
(37, 'bo_IN', 'bo', 'tibetan', 'Tibetan (India)'),
(38, 'br_FR', 'br', 'breton', 'Breton (France)'),
(39, 'bs_BA', 'bs', 'bosnian', 'Bosnian (Bosnia and Herzegovina)'),
(40, 'byn_ER', 'byn', 'blin', 'Blin (Eritrea)'),
(41, 'ca_AD', 'ca', 'catalan', 'Catalan (Andorra)'),
(42, 'ca_ES', 'ca', 'catalan', 'Catalan (Spain)'),
(43, 'ca_FR', 'ca', 'catalan', 'Catalan (France)'),
(44, 'ca_IT', 'ca', 'catalan', 'Catalan (Italy)'),
(45, 'crh_UA', 'crh', 'crimean turkish', 'Crimean Turkish (Ukraine)'),
(46, 'csb_PL', 'csb', 'kashubian', 'Kashubian (Poland)'),
(47, 'cs_CZ', 'cs', 'czech', 'Czech (Czech Republic)'),
(48, 'cv_RU', 'cv', 'chuvash', 'Chuvash (Russia)'),
(49, 'cy_GB', 'cy', 'welsh', 'Welsh (United Kingdom)'),
(50, 'da_DK', 'da', 'danish', 'Danish (Denmark)'),
(51, 'de_AT', 'de', 'german', 'German (Austria)'),
(52, 'de_BE', 'de', 'german', 'German (Belgium)'),
(53, 'de_CH', 'de', 'german', 'German (Switzerland)'),
(54, 'de_DE', 'de', 'german', 'German (Germany)'),
(55, 'de_LI', 'de', 'german', 'German (Liechtenstein)'),
(56, 'de_LU', 'de', 'german', 'German (Luxembourg)'),
(57, 'dv_MV', 'dv', 'divehi', 'Divehi (Maldives)'),
(58, 'dz_BT', 'dz', 'dzongkha', 'Dzongkha (Bhutan)'),
(59, 'ee_GH', 'ee', 'ewe', 'Ewe (Ghana)'),
(60, 'el_CY', 'el', 'greek', 'Greek (Cyprus)'),
(61, 'el_GR', 'el', 'greek', 'Greek (Greece)'),
(62, 'en_AG', 'en', 'english', 'English (Antigua and Barbuda)'),
(63, 'en_AS', 'en', 'english', 'English (American Samoa)'),
(64, 'en_AU', 'en', 'english', 'English (Australia)'),
(65, 'en_BW', 'en', 'english', 'English (Botswana)'),
(66, 'en_CA', 'en', 'english', 'English (Canada)'),
(67, 'en_DK', 'en', 'english', 'English (Denmark)'),
(68, 'en_GB', 'en', 'english', 'English (United Kingdom)'),
(69, 'en_GU', 'en', 'english', 'English (Guam)'),
(70, 'en_HK', 'en', 'english', 'English (Hong Kong SAR China)'),
(71, 'en_IE', 'en', 'english', 'English (Ireland)'),
(72, 'en_IN', 'en', 'english', 'English (India)'),
(73, 'en_JM', 'en', 'english', 'English (Jamaica)'),
(74, 'en_MH', 'en', 'english', 'English (Marshall Islands)'),
(75, 'en_MP', 'en', 'english', 'English (Northern Mariana Islands)'),
(76, 'en_MU', 'en', 'english', 'English (Mauritius)'),
(77, 'en_NG', 'en', 'english', 'English (Nigeria)'),
(78, 'en_NZ', 'en', 'english', 'English (New Zealand)'),
(79, 'en_PH', 'en', 'english', 'English (Philippines)'),
(80, 'en_SG', 'en', 'english', 'English (Singapore)'),
(81, 'en_TT', 'en', 'english', 'English (Trinidad and Tobago)'),
(82, 'en_US', 'en', 'english', 'English (United States)'),
(83, 'en_VI', 'en', 'english', 'English (Virgin Islands)'),
(84, 'en_ZA', 'en', 'english', 'English (South Africa)'),
(85, 'en_ZM', 'en', 'english', 'English (Zambia)'),
(86, 'en_ZW', 'en', 'english', 'English (Zimbabwe)'),
(87, 'eo', 'eo', 'esperanto', 'Esperanto'),
(88, 'es_AR', 'es', 'spanish', 'Spanish (Argentina)'),
(89, 'es_BO', 'es', 'spanish', 'Spanish (Bolivia)'),
(90, 'es_CL', 'es', 'spanish', 'Spanish (Chile)'),
(91, 'es_CO', 'es', 'spanish', 'Spanish (Colombia)'),
(92, 'es_CR', 'es', 'spanish', 'Spanish (Costa Rica)'),
(93, 'es_DO', 'es', 'spanish', 'Spanish (Dominican Republic)'),
(94, 'es_EC', 'es', 'spanish', 'Spanish (Ecuador)'),
(95, 'es_ES', 'es', 'spanish', 'Spanish (Spain)'),
(96, 'es_GT', 'es', 'spanish', 'Spanish (Guatemala)'),
(97, 'es_HN', 'es', 'spanish', 'Spanish (Honduras)'),
(98, 'es_MX', 'es', 'spanish', 'Spanish (Mexico)'),
(99, 'es_NI', 'es', 'spanish', 'Spanish (Nicaragua)'),
(100, 'es_PA', 'es', 'spanish', 'Spanish (Panama)'),
(101, 'es_PE', 'es', 'spanish', 'Spanish (Peru)'),
(102, 'es_PR', 'es', 'spanish', 'Spanish (Puerto Rico)'),
(103, 'es_PY', 'es', 'spanish', 'Spanish (Paraguay)'),
(104, 'es_SV', 'es', 'spanish', 'Spanish (El Salvador)'),
(105, 'es_US', 'es', 'spanish', 'Spanish (United States)'),
(106, 'es_UY', 'es', 'spanish', 'Spanish (Uruguay)'),
(107, 'es_VE', 'es', 'spanish', 'Spanish (Venezuela)'),
(108, 'et_EE', 'et', 'estonian', 'Estonian (Estonia)'),
(109, 'eu_ES', 'eu', 'basque', 'Basque (Spain)'),
(110, 'eu_FR', 'eu', 'basque', 'Basque (France)'),
(111, 'fa_AF', 'fa', 'persian', 'Persian (Afghanistan)'),
(112, 'fa_IR', 'fa', 'persian', 'Persian (Iran)'),
(113, 'ff_SN', 'ff', 'fulah', 'Fulah (Senegal)'),
(114, 'fil_PH', 'fil', 'filipino', 'Filipino (Philippines)'),
(115, 'fi_FI', 'fi', 'finnish', 'Finnish (Finland)'),
(116, 'fo_FO', 'fo', 'faroese', 'Faroese (Faroe Islands)'),
(117, 'fr_BE', 'fr', 'french', 'French (Belgium)'),
(118, 'fr_BF', 'fr', 'french', 'French (Burkina Faso)'),
(119, 'fr_BI', 'fr', 'french', 'French (Burundi)'),
(120, 'fr_BJ', 'fr', 'french', 'French (Benin)'),
(121, 'fr_CA', 'fr', 'french', 'French (Canada)'),
(122, 'fr_CF', 'fr', 'french', 'French (Central African Republic)'),
(123, 'fr_CG', 'fr', 'french', 'French (Congo)'),
(124, 'fr_CH', 'fr', 'french', 'French (Switzerland)'),
(125, 'fr_CM', 'fr', 'french', 'French (Cameroon)'),
(126, 'fr_FR', 'fr', 'french', 'French (France)'),
(127, 'fr_GA', 'fr', 'french', 'French (Gabon)'),
(128, 'fr_GN', 'fr', 'french', 'French (Guinea)'),
(129, 'fr_GP', 'fr', 'french', 'French (Guadeloupe)'),
(130, 'fr_GQ', 'fr', 'french', 'French (Equatorial Guinea)'),
(131, 'fr_KM', 'fr', 'french', 'French (Comoros)'),
(132, 'fr_LU', 'fr', 'french', 'French (Luxembourg)'),
(133, 'fr_MC', 'fr', 'french', 'French (Monaco)'),
(134, 'fr_MG', 'fr', 'french', 'French (Madagascar)'),
(135, 'fr_ML', 'fr', 'french', 'French (Mali)'),
(136, 'fr_MQ', 'fr', 'french', 'French (Martinique)'),
(137, 'fr_NE', 'fr', 'french', 'French (Niger)'),
(138, 'fr_SN', 'fr', 'french', 'French (Senegal)'),
(139, 'fr_TD', 'fr', 'french', 'French (Chad)'),
(140, 'fr_TG', 'fr', 'french', 'French (Togo)'),
(141, 'fur_IT', 'fur', 'friulian', 'Friulian (Italy)'),
(142, 'fy_DE', 'fy', 'western frisian', 'Western Frisian (Germany)'),
(143, 'fy_NL', 'fy', 'western frisian', 'Western Frisian (Netherlands)'),
(144, 'ga_IE', 'ga', 'irish', 'Irish (Ireland)'),
(145, 'gd_GB', 'gd', 'scottish gaelic', 'Scottish Gaelic (United Kingdom)'),
(146, 'gez_ER', 'gez', 'geez', 'Geez (Eritrea)'),
(147, 'gez_ET', 'gez', 'geez', 'Geez (Ethiopia)'),
(148, 'gl_ES', 'gl', 'galician', 'Galician (Spain)'),
(149, 'gu_IN', 'gu', 'gujarati', 'Gujarati (India)'),
(150, 'gv_GB', 'gv', 'manx', 'Manx (United Kingdom)'),
(151, 'ha_NG', 'ha', 'hausa', 'Hausa (Nigeria)'),
(152, 'he_IL', 'he', 'hebrew', 'Hebrew (Israel)'),
(153, 'hi_IN', 'hi', 'hindi', 'Hindi (India)'),
(154, 'hr_HR', 'hr', 'croatian', 'Croatian (Croatia)'),
(155, 'hsb_DE', 'hsb', 'upper sorbian', 'Upper Sorbian (Germany)'),
(156, 'ht_HT', 'ht', 'haitian', 'Haitian (Haiti)'),
(157, 'hu_HU', 'hu', 'hungarian', 'Hungarian (Hungary)'),
(158, 'hy_AM', 'hy', 'armenian', 'Armenian (Armenia)'),
(159, 'ia', 'ia', 'interlingua', 'Interlingua'),
(160, 'id_ID', 'id', 'indonesian', 'Indonesian (Indonesia)'),
(161, 'ig_NG', 'ig', 'igbo', 'Igbo (Nigeria)'),
(162, 'ik_CA', 'ik', 'inupiaq', 'Inupiaq (Canada)'),
(163, 'is_IS', 'is', 'icelandic', 'Icelandic (Iceland)'),
(164, 'it_CH', 'it', 'italian', 'Italian (Switzerland)'),
(165, 'it_IT', 'it', 'italian', 'Italian (Italy)'),
(166, 'iu_CA', 'iu', 'inuktitut', 'Inuktitut (Canada)'),
(167, 'ja_JP', 'ja', 'japanese', 'Japanese (Japan)'),
(168, 'ka_GE', 'ka', 'georgian', 'Georgian (Georgia)'),
(169, 'kk_KZ', 'kk', 'kazakh', 'Kazakh (Kazakhstan)'),
(170, 'kl_GL', 'kl', 'kalaallisut', 'Kalaallisut (Greenland)'),
(171, 'km_KH', 'km', 'khmer', 'Khmer (Cambodia)'),
(172, 'kn_IN', 'kn', 'kannada', 'Kannada (India)'),
(173, 'kok_IN', 'kok', 'konkani', 'Konkani (India)'),
(174, 'ko_KR', 'ko', 'korean', 'Korean (South Korea)'),
(175, 'ks_IN', 'ks', 'kashmiri', 'Kashmiri (India)'),
(176, 'ku_TR', 'ku', 'kurdish', 'Kurdish (Turkey)'),
(177, 'kw_GB', 'kw', 'cornish', 'Cornish (United Kingdom)'),
(178, 'ky_KG', 'ky', 'kirghiz', 'Kirghiz (Kyrgyzstan)'),
(179, 'lg_UG', 'lg', 'ganda', 'Ganda (Uganda)'),
(180, 'li_BE', 'li', 'limburgish', 'Limburgish (Belgium)'),
(181, 'li_NL', 'li', 'limburgish', 'Limburgish (Netherlands)'),
(182, 'lo_LA', 'lo', 'lao', 'Lao (Laos)'),
(183, 'lt_LT', 'lt', 'lithuanian', 'Lithuanian (Lithuania)'),
(184, 'lv_LV', 'lv', 'latvian', 'Latvian (Latvia)'),
(185, 'mai_IN', 'mai', 'maithili', 'Maithili (India)'),
(186, 'mg_MG', 'mg', 'malagasy', 'Malagasy (Madagascar)'),
(187, 'mi_NZ', 'mi', 'maori', 'Maori (New Zealand)'),
(188, 'mk_MK', 'mk', 'macedonian', 'Macedonian (Macedonia)'),
(189, 'ml_IN', 'ml', 'malayalam', 'Malayalam (India)'),
(190, 'mn_MN', 'mn', 'mongolian', 'Mongolian (Mongolia)'),
(191, 'mr_IN', 'mr', 'marathi', 'Marathi (India)'),
(192, 'ms_BN', 'ms', 'malay', 'Malay (Brunei)'),
(193, 'ms_MY', 'ms', 'malay', 'Malay (Malaysia)'),
(194, 'mt_MT', 'mt', 'maltese', 'Maltese (Malta)'),
(195, 'my_MM', 'my', 'burmese', 'Burmese (Myanmar)'),
(196, 'naq_NA', 'naq', 'namibia', 'Namibia'),
(197, 'nb_NO', 'nb', 'norwegian bokmål', 'Norwegian Bokmål (Norway)'),
(198, 'nds_DE', 'nds', 'low german', 'Low German (Germany)'),
(199, 'nds_NL', 'nds', 'low german', 'Low German (Netherlands)'),
(200, 'ne_NP', 'ne', 'nepali', 'Nepali (Nepal)'),
(201, 'nl_AW', 'nl', 'dutch', 'Dutch (Aruba)'),
(202, 'nl_BE', 'nl', 'dutch', 'Dutch (Belgium)'),
(203, 'nl_NL', 'nl', 'dutch', 'Dutch (Netherlands)'),
(204, 'nn_NO', 'nn', 'norwegian nynorsk', 'Norwegian Nynorsk (Norway)'),
(205, 'no_NO', 'no', 'norwegian', 'Norwegian (Norway)'),
(206, 'nr_ZA', 'nr', 'south ndebele', 'South Ndebele (South Africa)'),
(207, 'nso_ZA', 'nso', 'northern sotho', 'Northern Sotho (South Africa)'),
(208, 'oc_FR', 'oc', 'occitan', 'Occitan (France)'),
(209, 'om_ET', 'om', 'oromo', 'Oromo (Ethiopia)'),
(210, 'om_KE', 'om', 'oromo', 'Oromo (Kenya)'),
(211, 'or_IN', 'or', 'oriya', 'Oriya (India)'),
(212, 'os_RU', 'os', 'ossetic', 'Ossetic (Russia)'),
(213, 'pap_AN', 'pap', 'papiamento', 'Papiamento (Netherlands Antilles)'),
(214, 'pa_IN', 'pa', 'punjabi', 'Punjabi (India)'),
(215, 'pa_PK', 'pa', 'punjabi', 'Punjabi (Pakistan)'),
(216, 'pl_PL', 'pl', 'polish', 'Polish (Poland)'),
(217, 'ps_AF', 'ps', 'pashto', 'Pashto (Afghanistan)'),
(218, 'pt_BR', 'pt', 'portuguese', 'Portuguese (Brazil)'),
(219, 'pt_GW', 'pt', 'portuguese', 'Portuguese (Guinea-Bissau)'),
(220, 'pt_PT', 'pt', 'portuguese', 'Portuguese (Portugal)'),
(221, 'ro_MD', 'ro', 'romanian', 'Romanian (Moldova)'),
(222, 'ro_RO', 'ro', 'romanian', 'Romanian (Romania)'),
(223, 'ru_RU', 'ru', 'russian', 'Russian (Russia)'),
(224, 'ru_UA', 'ru', 'russian', 'Russian (Ukraine)'),
(225, 'rw_RW', 'rw', 'kinyarwanda', 'Kinyarwanda (Rwanda)'),
(226, 'sa_IN', 'sa', 'sanskrit', 'Sanskrit (India)'),
(227, 'sc_IT', 'sc', 'sardinian', 'Sardinian (Italy)'),
(228, 'sd_IN', 'sd', 'sindhi', 'Sindhi (India)'),
(229, 'seh_MZ', 'seh', 'sena', 'Sena (Mozambique)'),
(230, 'se_NO', 'se', 'northern sami', 'Northern Sami (Norway)'),
(231, 'sid_ET', 'sid', 'sidamo', 'Sidamo (Ethiopia)'),
(232, 'si_LK', 'si', 'sinhala', 'Sinhala (Sri Lanka)'),
(233, 'sk_SK', 'sk', 'slovak', 'Slovak (Slovakia)'),
(234, 'sl_SI', 'sl', 'slovenian', 'Slovenian (Slovenia)'),
(235, 'sn_ZW', 'sn', 'shona', 'Shona (Zimbabwe)'),
(236, 'so_DJ', 'so', 'somali', 'Somali (Djibouti)'),
(237, 'so_ET', 'so', 'somali', 'Somali (Ethiopia)'),
(238, 'so_KE', 'so', 'somali', 'Somali (Kenya)'),
(239, 'so_SO', 'so', 'somali', 'Somali (Somalia)'),
(240, 'sq_AL', 'sq', 'albanian', 'Albanian (Albania)'),
(241, 'sq_MK', 'sq', 'albanian', 'Albanian (Macedonia)'),
(242, 'sr_BA', 'sr', 'serbian', 'Serbian (Bosnia and Herzegovina)'),
(243, 'sr_ME', 'sr', 'serbian', 'Serbian (Montenegro)'),
(244, 'sr_RS', 'sr', 'serbian', 'Serbian (Serbia)'),
(245, 'ss_ZA', 'ss', 'swati', 'Swati (South Africa)'),
(246, 'st_ZA', 'st', 'southern sotho', 'Southern Sotho (South Africa)'),
(247, 'sv_FI', 'sv', 'swedish', 'Swedish (Finland)'),
(248, 'sv_SE', 'sv', 'swedish', 'Swedish (Sweden)'),
(249, 'sw_KE', 'sw', 'swahili', 'Swahili (Kenya)'),
(250, 'sw_TZ', 'sw', 'swahili', 'Swahili (Tanzania)'),
(251, 'ta_IN', 'ta', 'tamil', 'Tamil (India)'),
(252, 'teo_UG', 'teo', 'teso', 'Teso (Uganda)'),
(253, 'te_IN', 'te', 'telugu', 'Telugu (India)'),
(254, 'tg_TJ', 'tg', 'tajik', 'Tajik (Tajikistan)'),
(255, 'th_TH', 'th', 'thai', 'Thai (Thailand)'),
(256, 'tig_ER', 'tig', 'tigre', 'Tigre (Eritrea)'),
(257, 'ti_ER', 'ti', 'tigrinya', 'Tigrinya (Eritrea)'),
(258, 'ti_ET', 'ti', 'tigrinya', 'Tigrinya (Ethiopia)'),
(259, 'tk_TM', 'tk', 'turkmen', 'Turkmen (Turkmenistan)'),
(260, 'tl_PH', 'tl', 'tagalog', 'Tagalog (Philippines)'),
(261, 'tn_ZA', 'tn', 'tswana', 'Tswana (South Africa)'),
(262, 'to_TO', 'to', 'tongan', 'Tongan (Tonga)'),
(263, 'tr_CY', 'tr', 'turkish', 'Turkish (Cyprus)'),
(264, 'tr_TR', 'tr', 'turkish', 'Turkish (Turkey)'),
(265, 'ts_ZA', 'ts', 'tsonga', 'Tsonga (South Africa)'),
(266, 'tt_RU', 'tt', 'tatar', 'Tatar (Russia)'),
(267, 'ug_CN', 'ug', 'uighur', 'Uighur (China)'),
(268, 'uk_UA', 'uk', 'ukrainian', 'Ukrainian (Ukraine)'),
(269, 'ur_PK', 'ur', 'urdu', 'Urdu (Pakistan)'),
(270, 'uz_UZ', 'uz', 'uzbek', 'Uzbek (Uzbekistan)'),
(271, 've_ZA', 've', 'venda', 'Venda (South Africa)'),
(272, 'vi_VN', 'vi', 'vietnamese', 'Vietnamese (Vietnam)'),
(273, 'wa_BE', 'wa', 'walloon', 'Walloon (Belgium)'),
(274, 'wo_SN', 'wo', 'wolof', 'Wolof (Senegal)'),
(275, 'xh_ZA', 'xh', 'xhosa', 'Xhosa (South Africa)'),
(276, 'yi_US', 'yi', 'yiddish', 'Yiddish (United States)'),
(277, 'yo_NG', 'yo', 'yoruba', 'Yoruba (Nigeria)'),
(278, 'zh_CN', 'zh', 'chinese', 'Chinese (China)'),
(279, 'zh_HK', 'zh', 'chinese', 'Chinese (Hong Kong SAR China)'),
(280, 'zh_SG', 'zh', 'chinese', 'Chinese (Singapore)'),
(281, 'zh_TW', 'zh', 'chinese', 'Chinese (Taiwan)'),
(282, 'zu_ZA', 'zu', 'zulu', 'Zulu (South Africa)');

-- --------------------------------------------------------

--
-- Table structure for table `mail_provider`
--

CREATE TABLE `mail_provider` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credentials` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mail_provider`
--

INSERT INTO `mail_provider` (`id`, `name`, `provider`, `credentials`) VALUES
(1, 'Amazon API', 'ses', '["key","secret","region"]'),
(2, 'Amazon SMTP', 'smtp', '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
(3, 'Mailgun API', 'mailgun', '["domain","secret"]'),
(4, 'Mailgun SMTP', 'smtp', '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
(5, 'Sparkpost', 'sparkpost', '["secret"]'),
(6, 'Sparkpost SMTP', 'smtp', '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
(7, 'SMTP', 'smtp', '["smtp_host","smtp_port","smtp_username","smtp_password","encryption"]'),
(8, 'Elastic Email API', 'elastic', '["key"]'),
(9, 'SendGrid API', 'sendgrid', '["key"]'),
(10, 'PHP Mail', 'mail', '');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_02_04_141804_create_activities_table', 1),
(4, '2016_02_04_142653_create_oauth_table', 1),
(5, '2016_02_04_142901_create_email_table', 1),
(6, '2016_02_04_143623_create_config_table', 1),
(7, '2016_02_09_113358_create_languages_table', 1),
(8, '2016_02_09_114729_create_locales_table', 1),
(9, '2016_06_15_071638_create_template_table', 1),
(10, '2016_06_16_105849_create_group_table', 1),
(11, '2016_06_16_105948_create_email_list_table', 1),
(12, '2016_06_19_101214_create_table_media', 1),
(13, '2016_06_23_073119_create_email-history_table', 1),
(14, '2016_06_27_110657_create_email_later_table', 1),
(15, '2016_07_12_131153_create_api_table', 1),
(16, '2016_08_14_123054_create_newsletter_table', 1),
(17, '2016_08_20_110121_create_email_sent_list_table', 1),
(18, '2016_09_03_122008_create_package_table', 1),
(19, '2016_09_05_114651_create_purchase_history_table', 1),
(20, '2016_09_28_065833_create_customer_groups_table', 1),
(21, '2016_11_07_113009_create_blacklists_table', 1),
(22, '2016_11_26_075255_create_attachments_table', 1),
(23, '2016_11_29_120013_create_coupons_table', 1),
(24, '2016_11_30_124237_ticket_status', 1),
(25, '2016_12_04_115648_create_departments_table', 1),
(26, '2016_12_15_085929_create_tickets_table', 1),
(27, '2017_04_04_093618_create_ticket_comments_table', 1),
(28, '2017_04_05_093712_create_mail_provider_table', 1),
(29, '2017_04_09_102355_create_jobs_table', 1),
(30, '2017_04_09_102704_create_failed_jobs_table', 1),
(31, '2017_04_29_171638_create_faq_table', 1),
(32, '2017_05_03_113901_create_front_table', 1),
(33, '2017_12_23_135131_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth`
--

CREATE TABLE `oauth` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validity` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `user_limit` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `name`, `validity`, `limit`, `user_limit`, `price`, `created_at`, `updated_at`) VALUES
(1, 'free', '30', 1000, 1, 0, '2016-09-03 08:48:31', '2016-09-03 08:48:31'),
(2, 'silver', '60', 5000, 5, 5, '2016-09-03 08:48:48', '2016-09-03 08:48:48'),
(3, 'gold', '90', 50000, 10, 10, '2016-09-03 08:49:04', '2016-09-03 08:49:04'),
(4, 'platinum', '120', 100000, 15, 15, '2016-09-03 08:49:04', '2016-09-03 08:49:04');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_history`
--

CREATE TABLE `purchase_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `package_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_validity` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_price` int(10) UNSIGNED DEFAULT NULL,
  `package_limit` int(10) UNSIGNED DEFAULT NULL,
  `package_user_limit` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_template` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `user_id`, `name`, `html_template`, `template`, `created_at`, `updated_at`) VALUES
(1, 1, 'Happy Birthday', NULL, '<p style=""><br></p><h1 class="size-30" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #b59859;font-size: 26px;line-height: 34px;text-align: center;" lang="x-size-30">— Happy Birthday&nbsp;—</h1><p><br></p><h2 class="size-24" style="Margin-top: 20px;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #555;font-size: 20px;line-height: 28px;font-family: " pt="" sans","trebuchet="" ms",sans-serif;text-align:="" center;"="" lang="x-size-24" align="center">How are you doing?</h2><p><br></p><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16"><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p class="size-16" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16"><span style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #cca95e;font-family: \'PT Serif\', Georgia, serif;">Love you!</span> <br></p><div style="Margin-left: 20px;Margin-right: 20px;">\n      <div style="line-height:5px;font-size:1px">&nbsp;</div></div>', '2016-06-23 10:40:03', '2016-06-28 08:08:20'),
(2, 1, 'Hotel Booking', NULL, '<div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: Merriweather,Georgia,serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Where do you want to go in this holidays.<br> <br><div style="margin-left: 45px; margin-right: 20px; margin-bottom: 24px;">\n      <div class="btn btn--flat btn--large" style="text-align: center; margin-left: 300px;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #70717d;font-family: Merriweather, Georgia, serif;" href="http://xcoder.io">Book now</a>\n      </div>\n    </div>\n\n          </div><p>\n\n\n\n\n      <br></p><div style="line-height:20px;font-size:20px;">&nbsp;</div><p>\n\n      <br></p><div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: Merriweather,Georgia,serif;Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i1.createsend1.com/ei/d/B2/145/2C5/204455/csfinal/greece-304-888x550.jpg" alt="" width="300">\n        </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">\n      <h3 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #353638;font-size: 16px;line-height: 24px;"><strong><em>Greece</em></strong></h3><p style="Margin-top: 12px;Margin-bottom: 20px;">Located in the heart of the Mediterranean, our resort and spa is guaranteed to be a great home base for your next trip to Greece in any season.&nbsp;</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #70717d;font-family: Merriweather, Georgia, serif;" href="http://xcoder.io">View Hotel</a>\n      </div>\n    </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: Merriweather,Georgia,serif;Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i2.createsend1.com/ei/d/B2/145/2C5/204455/csfinal/viet.jpg" alt="" width="300">\n        </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">\n      <h3 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #353638;font-size: 16px;line-height: 24px;"><em><strong>Vietnam</strong></em></h3><p style="Margin-top: 12px;Margin-bottom: 20px;">Our hotel in central Hanoi has the perfect blend of culture and luxury. This hotel features truly authentic dining with a 5-star restaurant onsite.&nbsp;</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #70717d;font-family: Merriweather, Georgia, serif;" href="http://xcoder.io">View Hotel</a>\n      </div>\n    </div>\n\n          </div>\n\n        </div>\n      </div><p>\n\n      <br></p><div style="line-height:20px;font-size:20px;">&nbsp;</div><p>\n\n      <br></p><div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: Merriweather,Georgia,serif;Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i3.createsend1.com/ei/d/B2/145/2C5/204455/csfinal/sf.jpg" alt="" width="300">\n        </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">\n      <h3 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #353638;font-size: 16px;line-height: 24px;"><em><strong>San Fransisco</strong></em>&nbsp;</h3><p style="Margin-top: 12px;Margin-bottom: 20px;">This city has so much to offer. Our hotel here is located right on the water with amazing views and a row of bars and restaurants just next door.</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #70717d;font-family: Merriweather, Georgia, serif;" href="http://xcoder.io">View Hotel</a>\n      </div>\n    </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: Merriweather,Georgia,serif;Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i4.createsend1.com/ei/d/B2/145/2C5/204455/csfinal/beach1.jpg" alt="" width="300">\n        </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">\n      <h3 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #353638;font-size: 16px;line-height: 24px;"><em><strong>Maldives</strong></em></h3><p style="Margin-top: 12px;Margin-bottom: 20px;">Whether you\'re on a romantic getaway or a family vacation, our Maldives location has something for everyone from spa treatments to outdoor fun.</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #70717d;font-family: Merriweather, Georgia, serif;" href="http://xcoder.io">View Hotel</a>\n      </div>\n    </div>\n\n          </div>\n\n        </div>\n      </div><p>\n\n      <br></p><div style="line-height:20px;font-size:20px;">&nbsp;</div><p>\n\n      <br></p><div class="layout email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">\n\n          <div class="column wide" style="text-align: left;font-size: 12px;line-height: 19px;color: #a3a4ad;font-family: Merriweather,Georgia,serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);">\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">\n\n              <div>\n\n              </div>\n              <div style="Margin-top: 18px;">\n\n              </div>\n            </div>\n          </div>\n\n          <div class="column narrow" style="text-align: left;font-size: 12px;line-height: 19px;color: #a3a4ad;font-family: Merriweather,Georgia,serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);">\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">\n\n            </div>\n          </div>\n\n        </div>\n      </div><p>\n\n\n\n          <br></p><div class="column" style="text-align: left;font-size: 12px;line-height: 19px;color: #a3a4ad;font-family: Merriweather,Georgia,serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">\n\n          </div>', '2016-06-23 10:58:05', '2016-06-28 08:08:38'),
(3, 1, 'Product (mobile view)', NULL, '<p><br></p><div class="snippet" style="Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #bbb;font-family: Merriweather,Georgia,serif;">\n\n          </div><p>\n\n          <br></p><div class="webversion" style="Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 139px; width: 139px;width: calc(14100% - 78680px);padding: 10px 0 5px 0;text-align: right;color: #bbb;font-family: Merriweather,Georgia,serif;">\n\n          </div><p>\n\n\n\n\n      <br></p><div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;max-width:="" 600px;min-width:="" 320px;="" width:="" 320px;width:="" calc(28000%="" -="" 167400px);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;display: block;height: auto;width: 100%;max-width: 900px;" src="https://i1.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/fashion-hero1.jpg" alt="" width="600">\n        </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">\n      <div style="line-height:10px;font-size:1px">&nbsp;</div>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;">\n      <h1 class="size-34" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 30px;line-height: 38px;font-family: Cabin,Avenir,sans-serif;text-align: center;" lang="x-size-34">My favorite things...</h1><p class="size-16" style="Margin-top: 20px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16">Being\n a fashionista in the big city I pick up all kinds of tips on how to\nstay stylish on a budget and still keep my individuality. As the end of\nthe month comes to a close, I picked a few of my favorite posts to\nshare.&nbsp;</p><p class="size-16" style="Margin-top: 20px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16">❤<br>\nRiya Lee</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div style="line-height:8px;font-size:1px">&nbsp;</div>\n    </div>\n\n          </div>\n\n        </div>\n      </div><p>\n\n      <br></p><div style="line-height:20px;font-size:20px;">&nbsp;</div><p>\n\n      <br></p><div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top gnd-corner-image-bottom" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i2.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/05-newsletter02.png" alt="" width="300">\n        </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">\n      <h2 class="size-22" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 18px;line-height: 26px;font-family: Cabin,Avenir,sans-serif;" lang="x-size-22"><strong>Hats to the rescue</strong></h2><p class="size-17" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 17px;line-height: 26px;" lang="x-size-17">The right hat can be your best friend when you\'re in a late for work in the morning or having a bad hair day.</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #2ecc9e;font-family: \'PT Serif\', Georgia, serif;" href="http://xcoder.io">Read more</a>\n      </div>\n    </div>\n\n          </div>\n\n        </div>\n      </div><p>\n\n      <br></p><div style="line-height:30px;font-size:30px;">&nbsp;</div><p>\n\n      <br></p><div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top gnd-corner-image-bottom" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i3.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/05-newsletter03.png" alt="" width="300">\n        </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">\n      <h2 class="size-22" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 18px;line-height: 26px;font-family: Cabin,Avenir,sans-serif;" lang="x-size-22"><strong>Fancy tech accessories</strong></h2><p class="size-17" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 17px;line-height: 26px;" lang="x-size-17">I love my devices but they all look the same! Here\'s how I wrap my style on my phone and tablet.</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #2ecc9e;font-family: \'PT Serif\', Georgia, serif;" href="http://xcoder.io">Read more</a>\n      </div>\n    </div>\n\n          </div>\n\n        </div>\n      </div><p>\n\n      <br></p><div style="line-height:30px;font-size:30px;">&nbsp;</div><p>\n\n      <br></p><div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top gnd-corner-image-bottom" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i4.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/05-newsletter04.png" alt="" width="300">\n        </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">\n      <h2 class="size-22" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 18px;line-height: 26px;font-family: Cabin,Avenir,sans-serif;" lang="x-size-22"><strong>Boot for every season</strong></h2><p class="size-17" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 17px;line-height: 26px;" lang="x-size-17">Check out my top tips on how to pair your favorite boots on any outfit from winter to summer.&nbsp;</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #2ecc9e;font-family: \'PT Serif\', Georgia, serif;" href="http://xcoder.io">Read more</a>\n      </div>\n    </div>\n\n          </div>\n\n        </div>\n      </div><div class="layout email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">\n\n          <div class="column wide" style="text-align: left;font-size: 12px;line-height: 19px;color: #bbb;font-family: Merriweather,Georgia,serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);">\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">\n\n              <div>\n\n              </div>\n              <div style="Margin-top: 18px;">\n\n              </div>\n            </div>\n          </div>\n\n          <div class="column narrow" style="text-align: left;font-size: 12px;line-height: 19px;color: #bbb;font-family: Merriweather,Georgia,serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);">\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">\n\n            </div>\n          </div>\n\n        </div>\n      </div><div class="layout one-col email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">\n\n          <div class="column" style="text-align: left;font-size: 12px;line-height: 19px;color: #bbb;font-family: Merriweather,Georgia,serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">\n</div></div></div></div>', '2016-06-23 11:04:06', '2016-06-28 08:07:23'),
(4, 1, 'Product (desktop view)', NULL, '<p><br></p><div class="wrapper" style="min-width: 320px;background-color: #fefefe;" lang="x-wrapper">\n      <div class="preheader" style="Margin: 0 auto;max-width: 560px;min-width: 280px; width: 280px;width: calc(28000% - 173040px);">\n        <div style="border-collapse: collapse;display: table;width: 100%;">\n\n          <div class="snippet" style="Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #bbb;font-family: Merriweather,Georgia,serif;">\n\n          </div>\n\n          <div class="webversion" style="Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 139px; width: 139px;width: calc(14100% - 78680px);padding: 10px 0 5px 0;text-align: right;color: #bbb;font-family: Merriweather,Georgia,serif;">\n\n          </div>\n\n        </div>\n      </div>\n\n      <div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;max-width:="" 600px;min-width:="" 320px;="" width:="" 320px;width:="" calc(28000%="" -="" 167400px);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top" style="border: 0;display: block;height: auto;width: 100%;max-width: 900px;" src="https://i1.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/fashion-hero1.jpg" alt="" width="600">\n        </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;">\n      <div style="line-height:10px;font-size:1px">&nbsp;</div>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;">\n      <h1 class="size-34" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 30px;line-height: 38px;font-family: Cabin,Avenir,sans-serif;text-align: center;" lang="x-size-34">My favorite things...</h1><p class="size-16" style="Margin-top: 20px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16">Being\n a fashionista in the big city I pick up all kinds of tips on how to\nstay stylish on a budget and still keep my individuality. As the end of\nthe month comes to a close, I picked a few of my favorite posts to\nshare.&nbsp;</p><p class="size-16" style="Margin-top: 20px;Margin-bottom: 20px;font-size: 16px;line-height: 24px;text-align: center;" lang="x-size-16">❤<br>\nRiya Lee</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div style="line-height:8px;font-size:1px">&nbsp;</div>\n    </div>\n\n          </div>\n\n        </div>\n      </div>\n\n      <div style="line-height:20px;font-size:20px;">&nbsp;</div>\n\n      <div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top gnd-corner-image-bottom" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i2.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/05-newsletter02.png" alt="" width="300">\n        </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">\n      <h2 class="size-22" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 18px;line-height: 26px;font-family: Cabin,Avenir,sans-serif;" lang="x-size-22"><strong>Hats to the rescue</strong></h2><p class="size-17" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 17px;line-height: 26px;" lang="x-size-17">The right hat can be your best friend when you\'re in a late for work in the morning or having a bad hair day.</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #2ecc9e;font-family: \'PT Serif\', Georgia, serif;" href="http://xcoder.io">Read more</a>\n      </div>\n    </div>\n\n          </div>\n\n        </div>\n      </div>\n\n      <div style="line-height:30px;font-size:30px;">&nbsp;</div>\n\n      <div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top gnd-corner-image-bottom" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i3.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/05-newsletter03.png" alt="" width="300">\n        </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">\n      <h2 class="size-22" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 18px;line-height: 26px;font-family: Cabin,Avenir,sans-serif;" lang="x-size-22"><strong>Fancy tech accessories</strong></h2><p class="size-17" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 17px;line-height: 26px;" lang="x-size-17">I love my devices but they all look the same! Here\'s how I wrap my style on my phone and tablet.</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #2ecc9e;font-family: \'PT Serif\', Georgia, serif;" href="http://xcoder.io">Read more</a>\n      </div>\n    </div>\n\n          </div>\n\n        </div>\n      </div>\n\n      <div style="line-height:30px;font-size:30px;">&nbsp;</div>\n\n      <div class="layout two-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <br><div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #fefefe;">\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n        <div style="font-size: 12px;font-style: normal;font-weight: normal;" align="center">\n          <img class="gnd-corner-image gnd-corner-image-center gnd-corner-image-top gnd-corner-image-bottom" style="border: 0;display: block;height: auto;width: 100%;max-width: 480px;" src="https://i4.createsend1.com/ei/d/B2/145/2C5/210047/csfinal/05-newsletter04.png" alt="" width="300">\n        </div>\n\n          </div>\n\n          <div class="column" style="text-align: left;color: #353638;font-size: 14px;line-height: 21px;font-family: " pt="" serif",georgia,serif;float:="" left;max-width:="" 320px;min-width:="" 300px;="" width:="" 320px;width:="" calc(12300px="" -="" 2000%);"="">\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 12px;">\n      <h2 class="size-22" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #2ecc9e;font-size: 18px;line-height: 26px;font-family: Cabin,Avenir,sans-serif;" lang="x-size-22"><strong>Boot for every season</strong></h2><p class="size-17" style="Margin-top: 16px;Margin-bottom: 20px;font-size: 17px;line-height: 26px;" lang="x-size-17">Check out my top tips on how to pair your favorite boots on any outfit from winter to summer.&nbsp;</p>\n    </div>\n\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 12px;">\n      <div class="btn btn--flat btn--large" style="text-align:left;">\n        <a target="_blank" style="border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #2ecc9e;font-family: \'PT Serif\', Georgia, serif;" href="http://xcoder.io">Read more</a>\n      </div>\n    </div>\n\n          </div></div></div><div class="layout email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">\n\n          <div class="column wide" style="text-align: left;font-size: 12px;line-height: 19px;color: #bbb;font-family: Merriweather,Georgia,serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);">\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">\n\n              <div>\n\n              </div>\n              <div style="Margin-top: 18px;">\n\n              </div>\n            </div>\n          </div>\n\n          <div class="column narrow" style="text-align: left;font-size: 12px;line-height: 19px;color: #bbb;font-family: Merriweather,Georgia,serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);">\n            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">\n\n            </div>\n          </div>\n\n        </div>\n      </div>\n      <div class="layout one-col email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">\n        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">\n\n          </div></div></div>', '2016-06-23 11:04:36', '2016-06-28 08:08:04');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporter` int(10) UNSIGNED DEFAULT NULL,
  `priority` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `attachments` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_comments`
--

CREATE TABLE `ticket_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `attachments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_status`
--

CREATE TABLE `ticket_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ticket_status`
--

INSERT INTO `ticket_status` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Answered', '2016-09-03 08:49:04', '2016-09-03 08:49:04'),
(2, 'Closed', '2016-09-03 08:49:04', '2016-09-03 08:49:04'),
(3, 'Open', '2016-09-03 08:49:04', '2016-09-03 08:49:04'),
(4, 'In Progress', '2016-09-03 08:49:04', '2016-09-03 08:49:04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `department_id_list` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oauth_user_id` text COLLATE utf8mb4_unicode_ci,
  `photo` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_key` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_id` int(10) UNSIGNED DEFAULT NULL,
  `email_limit` int(10) UNSIGNED DEFAULT NULL,
  `user_limit` int(10) UNSIGNED DEFAULT NULL,
  `package_activated_at` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domain` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DKIM` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SPF` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_settings_id` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api`
--
ALTER TABLE `api`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blacklists`
--
ALTER TABLE `blacklists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_history`
--
ALTER TABLE `email_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_later`
--
ALTER TABLE `email_later`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_list`
--
ALTER TABLE `email_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_sent_list`
--
ALTER TABLE `email_sent_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_settings`
--
ALTER TABLE `email_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_end`
--
ALTER TABLE `front_end`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locales`
--
ALTER TABLE `locales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_provider`
--
ALTER TABLE `mail_provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth`
--
ALTER TABLE `oauth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `purchase_history`
--
ALTER TABLE `purchase_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_status`
--
ALTER TABLE `ticket_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blacklists`
--
ALTER TABLE `blacklists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_groups`
--
ALTER TABLE `customer_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email_history`
--
ALTER TABLE `email_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_later`
--
ALTER TABLE `email_later`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_list`
--
ALTER TABLE `email_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_sent_list`
--
ALTER TABLE `email_sent_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_settings`
--
ALTER TABLE `email_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `front_end`
--
ALTER TABLE `front_end`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `locales`
--
ALTER TABLE `locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;
--
-- AUTO_INCREMENT for table `mail_provider`
--
ALTER TABLE `mail_provider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth`
--
ALTER TABLE `oauth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `purchase_history`
--
ALTER TABLE `purchase_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ticket_status`
--
ALTER TABLE `ticket_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
