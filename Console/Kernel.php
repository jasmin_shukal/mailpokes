<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call('App\Http\Controllers\Cron\CronController@cronTest')->everyMinute();
        $schedule->call('App\Http\Controllers\Cron\CronController@processKill')->everyMinute();
        $schedule->call('App\Http\Controllers\HomeController@clearLog')->daily();
        $schedule->call('App\Http\Controllers\HomeController@clearDemoDatabaseDaily')->daily();
        $schedule->command('queue:listen --tries=1 --timeout=3600')->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
