<?php
/**
 * Created by PhpStorm.
 * Users: mithun
 * Date: 11/30/15
 * Time: 10:04 PM
 */

Route::group(['middleware' => ['web','install', 'auth', 'demo', 'package', 'superadmin']], function(){
    Route::get('customer-groups', 'CustomerGroupController@index');
    Route::post('customer-groups/group-save', 'CustomerGroupController@group_save');
    Route::get('customer-groups/group-destroy/{id}', 'CustomerGroupController@group_destroy');
    Route::get('customer-groups/group-edit/{id}', 'CustomerGroupController@group_edit');
    Route::post('customer-groups/group-update', 'CustomerGroupController@group_update');
});