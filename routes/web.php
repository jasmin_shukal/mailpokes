<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {
    if (!env('INSTALLED', false)) {
        // Route::get('/', 'Install\InstallController@index');
        Route::get('/', 'Install\InstallController@index');
        Route::post('install/store', 'Install\InstallController@store');
        //Route::get('/', 'HomeController@index');
    } else {
        // Route::get('/', 'HomeController@index');
        Route::get('/', 'HomeController@index');
    }
    Route::get('home', 'HomeController@index');

    Route::auth();
// term and condition
    Route::get('termsCondition', 'HomeController@termsCondition');
// privacy policy
    Route::get('privacyPolicy', 'HomeController@privacyPolicy');


// Social github routes...
    Route::get('github', 'Auth\AuthController@redirectToProvider');
    Route::get('github/callback', 'Auth\AuthController@handleProviderCallback');

// Social facebook routes...
    Route::get('facebook', 'Auth\AuthController@redirectToProvider');
    Route::get('facebook/callback', 'Auth\AuthController@handleProviderCallback');

// Social google routes...
    Route::get('google', 'Auth\AuthController@redirectToProvider');
    Route::get('google/callback', 'Auth\AuthController@handleProviderCallback');

// Social twitter routes...
    Route::get('twitter', 'Auth\AuthController@redirectToProvider');
    Route::get('twitter/callback', 'Auth\AuthController@handleProviderCallback');

// Social linkedin routes...
    Route::get('linkedin', 'Auth\AuthController@redirectToProvider');
    Route::get('linkedin/callback', 'Auth\AuthController@handleProviderCallback');


//access without login
    Route::get('unsubscribe/{id}', 'EmailList\EmailListController@unsubscribe');

});


// set and prevent all route after login routes...
Route::group(['middleware' => ['web','install', 'auth', 'demo', 'package']], function () {

    Route::resource('oauth', 'Oauth\OauthController');
    Route::resource('theme', 'Theme\ThemeController');
        
    Route::post('config', 'Oauth\OauthController@updateSocialLogin');

    Route::get('email_settings/default_change/{id}', 'Email\EmailController@default_change');
    Route::post('email_settings', 'Email\EmailController@email_settings');
    Route::get('email/load_mail_provider/{mail_provider}', 'Email\EmailController@load_mail_provider');
    Route::post('email-testing', 'Email\EmailController@email_testing');
    Route::resource('email', 'Email\EmailController');
    Route::resource('system_settings', 'Settings\SettingsController');
    Route::get('paypal-api', 'ExternalApi\ExternalApiController@paypal_api');
    Route::post('store-paypal-api', 'ExternalApi\ExternalApiController@store_paypal_api');
    Route::get('stripe-api', 'ExternalApi\ExternalApiController@stripe_api');
    Route::post('store-stripe-api', 'ExternalApi\ExternalApiController@store_stripe_api');
    Route::resource('api', 'ExternalApi\ExternalApiController');
    Route::resource('profile', 'Profile\ProfileController');
    Route::resource('translation', 'Translation\TranslationController');
    Route::get('coupons/changed_published/{coupon}', 'Coupons\CouponsController@change_published');
    Route::resource('coupons', 'Coupons\CouponsController');
    Route::resource('ticketstatus', 'Settings\ticketstatusController');

// translation

    Route::get('lang/{locale}', function ($locale) {
        if (getenv('locale')) {
            $preCont = \Illuminate\Support\Facades\File::get(base_path() . '/.env');

            $newCont = str_replace('locale' . '=' . getenv('locale'), 'locale' . '=' . $locale, $preCont);

            \Illuminate\Support\Facades\File::put(base_path() . '/.env', $newCont);

        } else {
            \Illuminate\Support\Facades\File::append(base_path() . '/.env', "\nlocale=$locale");
        }
        return redirect()->back();
    });

// insert translation
    Route::post('insertLang', 'Translation\TranslationController@insertLang');
// update translation
    Route::get('updateLanguage/{id}', 'Translation\TranslationController@updateLanguage');
    // privacy
    Route::resource('privacy', 'Terms\TermsController');
    Route::any('storeTerms', 'Terms\TermsController@storeTerms');
    Route::get('backupDownload', 'DatabaseBackupController@backupDownload');

    // cron
    Route::resource('cron', 'Cron\CronController');
});


Route::group(['middleware' => ['web', 'auth', 'package']], function(){
    Route::resource('departments', 'Department\DepartmentController');
    Route::resource('frontend', 'FrontEnd\FrontEndController');

    Route::any('save-email', 'EmailList\EmailListController@save_email');
    Route::any('loadMedia', 'Media\MediaController@loadMedia');
    Route::any('update_email_info', 'EmailList\EmailListController@update_email_info');
    Route::any('email-delete/{emailList_id}', 'EmailList\EmailListController@email_delete');

    Route::resource('history', 'EmailHistory\EmailHistoryController');
    Route::get('tickets/attchment-download/{file_name}', 'Tickets\TicketsController@attachment_download');
    Route::get('tickets/change_status/{ticket_id}/{status_id}', 'Tickets\TicketsController@change_status');
    Route::get('tickets/comment_delete/{ticket_comment}', 'Tickets\TicketsController@comment_delete');
    Route::post('tickets/comment_save', 'Tickets\TicketsController@comment_save');
    Route::resource('tickets', 'Tickets\TicketsController');
    Route::get('purchase-history', 'EmailHistory\EmailHistoryController@purchase_history');
    Route::any('sent-email-list/{email_history_id}', 'EmailHistory\EmailHistoryController@sent_email_list');
    Route::get('clearHistory', 'EmailHistory\EmailHistoryController@clearHistory');

    Route::resource('email-list', 'EmailList\EmailListController');
    Route::post('email-list/save-group', 'EmailList\EmailListController@save_group');
    Route::any('real-email/{checker}/{id}', 'EmailList\EmailListController@real_email');
    Route::any('delete-invalid-email/{checker}/{id}', 'EmailList\EmailListController@delete_invalid_email');
    Route::any('load-template', 'MessageList\MessageListController@load_template');
//    Route::resource('message-list', 'MessageList\MessageListController');
    Route::post('upload_template', 'Template\TemplateController@upload_template');
    Route::resource('faq-setting', 'Faq\FaqController');
    Route::resource('template', 'Template\TemplateController');
    Route::resource('htmlTemplate', 'Template\htmlTemplate');
    Route::get('build/{column}', 'Template\htmlTemplate@buildTemplate');
    Route::resource('newsletter', 'Newsletter\NewsletterController');
    Route::resource('send-mail', 'SendMail\SendMailController');
    Route::get('send-mail/group_detail/{id}', 'SendMail\SendMailController@group_detail')->name('get-group-detail');
    Route::resource('media', 'Media\MediaController');


    Route::get('download_sample', 'HomeController@download_sample');
    //physical address
    Route::resource('address', 'Address\AddressController');

    // blacklist
    Route::resource('email_blacklist', 'Blacklist\blacklistController');
    Route::resource('word_blacklist', 'Blacklist\blacklistController');
    Route::resource('domain_blacklist', 'Blacklist\blacklistController');
    Route::resource('ip', 'Blacklist\blacklistController');
    Route::resource('attachments', 'Attachment\AttachmentController');

});

Route::group(['middleware' => ['web']], function(){
    Route::any('subscribe/{api_key?}/{group_id?}', 'Api\ApiController@subscribe');
    Route::get('link-open/{email_sent_list_id}/{email}', 'EmailHistory\EmailHistoryController@link_open');
    Route::any('email-open/{email_sent_list_id}/{email}', 'EmailHistory\EmailHistoryController@email_open');
//    pricing
    Route::resource('pricing', 'Pricing\PricingController');
//package

    Route::get('pay', 'Paypal\PaypalController@pay');
    Route::post('payment', 'Package\PackageController@payment');
    Route::get('order/{package}', 'Package\PackageController@order');
    Route::resource('package', 'Package\PackageController');
    Route::get('faq/search', 'Faq\FaqController@search');
    Route::get('faq', 'Faq\FaqController@faq_list');


});

Route::get('env_value/{value}', function ($value) {
    return $value."=".env($value);
});