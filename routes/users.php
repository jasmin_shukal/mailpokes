<?php
/**
 * Created by PhpStorm.
 * Users: mithun
 * Date: 11/30/15
 * Time: 10:04 PM
 */

Route::group(['middleware' => ['web','install', 'auth', 'demo', 'package', 'superadminORcustomer']], function(){
    Route::any('users/update_password', 'UsersController@update_password');
    Route::get('users/active', 'UsersController@active_users');
    Route::get('users/banned', 'UsersController@banned_users');
    Route::get('users/change_status/{user_id}', 'UsersController@change_status');
    Route::resource('users', 'UsersController');
});