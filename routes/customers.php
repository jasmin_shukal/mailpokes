<?php
/**
 * Created by PhpStorm.
 * Users: mithun
 * Date: 11/30/15
 * Time: 10:04 PM
 */

Route::group(['middleware' => ['web','install', 'auth', 'demo', 'package', 'superadmin']], function(){
    Route::get('load_customer_users/{customer_id}', 'CustomersController@load_customer_users');
    Route::get('customers/change_status/{user_id}', 'CustomersController@change_status');
    Route::resource('customers', 'CustomersController');
});