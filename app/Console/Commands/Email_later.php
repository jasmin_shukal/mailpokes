<?php

namespace App\Console\Commands;

use App\Models\EmailSentList\EmailSentList;
use App\Models\EmailList\EmailList;
use App\Models\Email\Email;
use App\Models\Email\MailProvider;
use App\User;
use App\Models\EmailHistory\EmailHistory;
use App\Models\Template\Template;
use App\Models\EmailLater\EmailLater;
use App\Jobs\EmailQueueJob;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Console\Command;
use App\Models\Blacklist\Blacklist;
use Log;

class Email_later extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:later';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send schedule mail (Later)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       Log::error('start email later');
        $emailLater = EmailLater::all();

        foreach ($emailLater as $email) {
            if ($email->send_time == strtotime(date('d-m-Y h:i a', time()))) {
                
                $emailTemplate = Template::find($email->template_id)->template;
                $emailLists = json_decode($email->email_list);
                $subject = $email->subject;

                if(env('APP_ENV', false) == 'demo') {
                    $emailLists = [array_values((array)$emailLists)[0]];
                }

                $authUser = User::find($email->sender_id);

                if($authUser->role == 'user'){
                    $admin_or_customer_id = $authUser->customer_id;
                } else {
                    $admin_or_customer_id = $email->sender_id;
                }
                $user = User::find($admin_or_customer_id);

                if($user) {
                    if(! isset($user->email_settings_id) || (isset($user->email_settings_id) && ($user->email_settings_id == 0))) {
                        Log::error('Please set mail provider first. Go Settings > Email Settings.');
                        break;
                    }

                    $email_info = Email::find($user->email_settings_id);
                    
                    if(! $email_info) {
                        Log::error('Mail Provider Credential not found.');
                        break;
                    }

                    $mail_provider = MailProvider::find($email_info->mail_provider_id);

                    if(! $mail_provider ) {
                        Log::error('Mail Provider not found. Please set mail provider.');
                        break;
                    }

                } else {
                    Log::error('User not found. Please try again.');
                    break;
                }


                // $authUser = User::find($email->sender_id);

                // if($authUser->role == 'user'){
                //     $admin_or_customer_id = $authUser->customer_id;
                // } else {
                //     $admin_or_customer_id = $email->sender_id;
                // }
                // $mail_driver = User::find($admin_or_customer_id)->mail_driver;

                // if($mail_driver == 'smtp') {
                //     $email_info = Email::where('user_id', $admin_or_customer_id)->where('default_enable', 1)->first();
                //     if(! $email_info){
                //         $email_info = Email::where('user_id', $admin_or_customer_id)->first();
                //     }
                // } else if($mail_driver == 'mail') {
                //     $email_info = User::find($admin_or_customer_id);
                // }

//                $mail_driver = User::find($email->sender_id)->mail_driver;

//                $email_info = Email::where('user_id', $email->sender_id)->first();

                $emailHistoryData = [
                    'sender_id' => $email->sender_id,
                    'template_id' => $email->template_id,
                    'attachment_list' => $email->attachment_list,
                    'subject' => $email->subject
                ];

                $emailHistory = EmailHistory::create($emailHistoryData);

                foreach($emailLists as $email_add){
                    $emailSentList = [
                        'email_history_id' => $emailHistory->id,
                        'email' => $email_add,
                        'seen_status' => 0
                    ];
                    EmailSentList::create($emailSentList);
                }

                $attachment_list = json_decode($email->attachment_list);

                foreach ( $emailLists as $emailId => $emaillist ) {
                    $userEmail = EmailList::find($emailId);

                    $replacedWith = '';
                    if(isset($userEmail->name)){
                        $replacedWith = $userEmail->name;
                    }
                    $trackingWithTemplate = $emailTemplate .'<img src="'. $email->url.'/email-open/'. base64_encode($emailHistory->id).'/'.$emaillist .'" height="1" width="1" alt="">';

                    $word_blacklist = Blacklist::where('for', 'word_blacklist')->pluck('name')->toArray();
                    if(count($word_blacklist) > 0) {
                        foreach($word_blacklist as $block_word) {
                            $trackingWithTemplate = str_replace($block_word, '', $trackingWithTemplate);
                        }
                    }

                    $emailTemplateReplaced = str_replace('{USERNAME}', $replacedWith, $trackingWithTemplate);

                    $jobs = (new EmailQueueJob($email_info, $mail_provider, $emailTemplateReplaced, $emailId, $emaillist, $subject, $attachment_list))->delay(1);
                    $this->dispatch($jobs);
                }
            }
        }
//        Log::error('end');
    }
}
