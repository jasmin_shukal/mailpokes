<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'group_id', 'customer_id', 'department_id_list', 'password', 'status', 'platform', 'oauth_user_id', 'photo', 'role', 'address', 'api_key', 'package_id', 'email_limit', 'user_limit', 'package_activated_at', 'from_email', 'sender_name', 'domain', 'DKIM', 'SPF', 'remember_token', 'email_settings_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
