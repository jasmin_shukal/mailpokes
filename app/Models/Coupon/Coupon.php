<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';

    protected $fillable = ["name", "code", 'package_id', 'discount_amount', 'amount_type', 'published'];

}
