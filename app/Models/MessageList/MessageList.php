<?php

namespace App\Models\MessageList;

use Illuminate\Database\Eloquent\Model;

class MessageList extends Model
{
    protected $table = 'message_list';

    protected $fillable = ['user_id', 'name'];

    public function template(){
        return $this->hasMany('App\Models\Template\Template');
    }
}
