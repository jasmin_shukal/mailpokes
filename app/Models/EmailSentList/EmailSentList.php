<?php

namespace App\Models\EmailSentList;

use Illuminate\Database\Eloquent\Model;

class EmailSentList extends Model
{
    protected $table = 'email_sent_list';
    protected $fillable = ["email_history_id","email","seen_status", 'link_open'];
}
