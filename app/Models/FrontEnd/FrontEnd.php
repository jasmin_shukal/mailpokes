<?php

namespace App\Models\FrontEnd;

use Illuminate\Database\Eloquent\Model;

class FrontEnd extends Model
{
    protected $table = 'front_end';

    protected $fillable = ['menu', 'description', 'status'];

}
