<?php

namespace App\Models\PurchaseHistory;

use Illuminate\Database\Eloquent\Model;

class PurchaseHistory extends Model
{
    protected $table = 'purchase_history';

    protected $fillable = ['user_id','package_name', 'package_validity', 'package_price', 'package_limit', 'package_user_limit'];
}
