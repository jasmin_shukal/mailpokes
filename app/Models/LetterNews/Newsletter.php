<?php

namespace App\Models\LetterNews;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Newsletter extends Model
{
    protected $table = 'newsletter';
    protected $fillable = ['user_id', 'name', 'template'];

}
