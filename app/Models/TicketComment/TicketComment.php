<?php

namespace App\Models\TicketComment;

use Illuminate\Database\Eloquent\Model;

class TicketComment extends Model
{
    protected $table = 'ticket_comments';

    protected $fillable = ['ticket_id', 'user_id', 'comments', 'attachments'];

}
