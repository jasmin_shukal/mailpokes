<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    protected $fillable = ['code', 'subject', 'reporter', 'priority', 'department_id', 'reason', 'attachments', 'created_by', 'status'];

    public function department()
    {
        return $this->belongsTo('App\Models\Department\Department');
    }

    public function ticket_status()
    {
        return $this->belongsTo('App\Models\TicketStatus\TicketStatus', 'status');
    }
}
