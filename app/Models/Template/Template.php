<?php

namespace App\Models\Template;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Template extends Model
{
    protected $table = 'template';
    protected $fillable = ['user_id', 'name', 'html_template', 'template'];
    
    public function message_list(){
        return $this->belongsTo('App\User');
    }
}
