<?php

namespace App\Models\Blacklist;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    protected $table = 'blacklists';
    protected $fillable = ["name","reason","for", 'created_by', 'user_id'];
}
