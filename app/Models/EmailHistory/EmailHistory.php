<?php

namespace App\Models\EmailHistory;

use Illuminate\Database\Eloquent\Model;

class EmailHistory extends Model
{
    protected $table = 'email_history';
    protected $fillable = ["sender_id","template_id", "attachment_list", "subject"];

    public function emailsentlist(){
        return $this->hasMany('App\Models\EmailSentList\EmailSentList');
    }
    
   public function emailsettings()
   {
       return $this->belongsTo('App\Models\Email\Email','sender_id','user_id');
       
   }
}
