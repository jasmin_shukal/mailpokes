<?php

namespace App\Models\Faq;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Faq extends Model
{
    protected $table = 'faq';
    protected $fillable = ['user_id', 'name', 'html_faq', 'faq'];

    public function message_list(){
        return $this->belongsTo('App\User');
    }
}
