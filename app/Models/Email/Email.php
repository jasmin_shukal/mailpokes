<?php

namespace App\Models\Email;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'email_settings';
    protected $fillable = ['user_id','domain','key','secret','region', "mail_provider_id", "smtp_host","smtp_username","smtp_password","smtp_port","encryption","from_email","name", "reply_to"];

    public function mail_provider()
    {
        return $this->belongsTo('App\Models\Email\MailProvider');
    }
}
