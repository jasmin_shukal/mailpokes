<?php

namespace App\Models\Email;

use Illuminate\Database\Eloquent\Model;

class MailProvider extends Model
{
    protected $table = 'mail_provider';
    protected $fillable = ["name","provider","credentilas"];
}
