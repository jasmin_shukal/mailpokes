<?php

namespace App\Models\PageDesign;

use Illuminate\Database\Eloquent\Model;

class PageDesign extends Model
{
    protected $table = 'page_designs';  
    protected $fillable = ['user_id', 'name', 'html_template', 'template', 'show_hide'];
    
    public function users(){
        return $this->belongsTo('App\User');
    }
}
