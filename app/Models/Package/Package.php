<?php

namespace App\Models\Package;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'package';

    protected $fillable = ['name','validity', 'limit', 'user_limit', 'price'];
}
