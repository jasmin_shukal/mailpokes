<?php

namespace App\Http\Requests\Customers;

use App\Http\Requests\Request;
use App\User;
use Log;

class CustomersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->segment(2));
        if ($user) {
            $rules = [
                'name' => 'required',
                'email' => 'required|email|max:255|unique:users,email,'.$this->segment(2),
                'package_id' => 'required',
                'group_id' => 'required'
            ];

            if($this->has('password') || $this->has('confirm_password')){
                $rules['password'] = 'required|min:6';
                $rules['confirm_password'] = 'required|same:password';
            }
            return $rules;
        }
        return [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
            'package_id' => 'required',
            'group_id' => 'required'
        ];
    }
}
