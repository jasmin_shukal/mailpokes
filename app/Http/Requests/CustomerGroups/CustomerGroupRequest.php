<?php

namespace App\Http\Requests\CustomerGroups;

use Auth;
use App\Http\Requests\Request;
use App\Models\CustomerGroup\CustomerGroup;
use App\User;
use Log;

class CustomerGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->segment(2) == 'group-update') {
            return [
                'name' => "required|max:255|unique:customer_groups,id,$this->group_id",
            ];
        }
        return [
            'name' => 'required|max:255|unique:customer_groups',
        ];
    }
}
