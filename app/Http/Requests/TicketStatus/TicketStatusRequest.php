<?php

namespace App\Http\Requests\TicketStatus;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\CustomerGroup\CustomerGroup;

class TicketStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        if($this->segment(2) == 'group-update') {
            return [
                'name' => "required|max:255|unique:customer_groups,id,$this->group_id",
            ];
        }
        return [
            'name' => 'required|max:255|unique:customer_groups',
        ];
    }
}
