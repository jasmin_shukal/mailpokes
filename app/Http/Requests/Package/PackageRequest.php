<?php

namespace App\Http\Requests\Package;

use App\Http\Requests\Request;

class PackageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'validity' => 'required|numeric',
            'limit' => 'required|numeric',
            'user_limit' => 'required|numeric',
            'price' => 'required|numeric',
        ];
    }
}
