<?php

namespace App\Http\Requests\EmailSettings;

use App\Http\Requests\Request;

class EmailSettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'smtp_provider'		                => 'required',
            'smtp_host'                         => 'required',
            'smtp_username'                     => 'required',
            'smtp_password'                     => 'required',
            'smtp_port'                         => 'required|numeric',
            'from_email'                        => 'required|email',
            'name'                              => 'required',
        ];
    }
}
