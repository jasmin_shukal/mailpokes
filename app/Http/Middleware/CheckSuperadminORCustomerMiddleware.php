<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckSuperadminORCustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // super admin or customer check
        if (Auth::check() && (Auth::user()->role != 'superadmin') && (Auth::user()->role != 'customer')) {
            return redirect('/');
        }
        return $next($request);
    }
}
