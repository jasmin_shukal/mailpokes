<?php

namespace App\Http\Middleware;

use Log;
use Auth;
use Closure;
use Session;

class PackageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->package_id == 0 && Auth::user()->role == 'customer'){
                Session::flash('error_msg', 'You do not have any package. Please purchase a package first.');
                return redirect('pricing');
            }
        }
        return $next($request);
    }
}
