<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckCustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // customer check
        if ( Auth::user()->role != 'customer') {
            return redirect('/');
        }
        return $next($request);
    }
}
