<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Schema;
use App\Models\EmailList\EmailList;

class EmailValidationController extends Controller
{

    public function index(){
            $comparison = array(
                "fields"=> ['id', 'group_id'],
                "operators" => ['=','='],
                "compare_with" => ['*', '1']
            );
        
        $data['comparison'] = $comparison;
        dd($comparison);
        foreach($comparison as $com){
            echo $com['fields']."  ". $com['operators']."  ". $com['with'];
        }
        exit;
        dd($data);
    }
    public function allEmails(){
        
        $columns = Schema::getColumnListing('email_list');
        $totalData = EmailList::where('group_id', 89)->count();
        $totalFiltered = $totalData;

        $limit = 100000;
        $start = 10;
        $order = 'email';
        $dir = 20;

        $emails = EmailList::offset($start)
                            ->limit($limit)
                            ->orderBy($order)
                            ->get();
                           
        $data = [];
        
        if(!empty($emails)){
            $data = $emails;
            foreach($emails as $email){
                // $show = route('allemails.show', $email->id);
                // $edit = route('allemails.edit', $email->id);

                $nestedData['id'] =  $email->id;
                $nestedData['group_id'] =  $email->group_id;
                $nestedData['name'] = $email->name;
                $nestedData['email'] = $email->email;
                $nestedData['subscribed'] = $email->subscribed;
                $nestedData['free_email_check'] = $email->free_email_check;
                $nestedData['free_email_value'] = $email->free_email_value;
                $nestedData['bulk_check'] = $email->bulk_check;
                $nestedData['bulk_value'] = $email->bulk_value;
                $nestedData['email_list_value'] = $email->email_list_value;
                $nestedData['email_list_check'] = $email->email_list_check;
                $nestedData['created_at'] = $email->created_at;
                
                $this->data = $nestedData;
            }

            $json_data = array(
                'draw' => 20,
                'recordsTotal' => intval($totalData),
                'recordsFiltered' => intval($totalFiltered),
                'data' => $this->data
            );
        }
        echo json_encode($json_data);
    }

    public function getCsv(){
        $row = 0;
        if (($handle = fopen("/var/www/email/emaillist/sample_email_list.csv", "r")) !== FALSE) {
            // Log::info('before while');
            
            while (($data = fgetcsv($handle, 250, "\r")) !== FALSE) {
                // Log::info('after while');
                
                $num = count($data);
                
                for ($c = 0; $c < $num; $c++) {
                    if($row == 0){
                        
                    }else{
                        // echo $c . " ";
                        // print_r($data[$c]);
                        $result = explode(',', $data[$c]);
                        print_r($result);
                        // print("   ");
                        // echo $result[1] . "\n";
                        // $validEmails = validEmailForFile($result[1]);
                        
                        // Log::alert($validEmails);
                        // if($validEmails){
                        //     $data['name'] = $result[0];
                        //     $data['email'] = $result[1];
                        //     $data['subcribed'] = 1;
                        //     EmailList::create($data);
                        //     foreach($this->group_id_list as $group_id){
                        //         $data['group_id'] = $group_id;
                        //         EmailList::create($data);
                        //     }
                        // }
                    }
                    $row++;
                }
                
            }

            fclose($handle);
        }

        // $email = "1name@gmail.com";

        // // Remove all illegal characters from email
        // $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        // // Validate e-mail
        // if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        //     echo("$email is a valid email address");
        // } else {
        //     echo("$email is not a valid email address");
        // }
    
    }
}
