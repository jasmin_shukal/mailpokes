<?php

namespace App\Http\Controllers;

use App\Models\EmailHistory\EmailHistory;
use App\Models\EmailSentList\EmailSentList;
use App\Models\Group\Group;
use App\Models\Template\Template;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Response;
use DB;
use Illuminate\Support\Facades\File;
use Storage;
use Log;
use App\Models\Package\Package;
use App\Models\FrontEnd\FrontEnd;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        $packages = Package::all();
        if (Auth::check()) {
            // get all user limit 5
            $allUsers = User::get()->take(5);
            // get all banned user
            $bannedUsers = User::where('status', 'Banned')->get();
            //dashboard loaded with active and banned users
            $activeUsers = User::where('status', 'Active')->orderBy('created_at', 'desc')->get()->take(5);
            $bannedUsers = User::where('status', 'Banned')->orderBy('created_at', 'desc')->get()->take(5);

            $groups = Group::where('user_id', Auth::id())->orderBy('created_at', 'desc')->get()->take(5);
            $templates = Template::where('user_id', Auth::id())->orderBy('created_at', 'desc')->get()->take(5);
            $package = Package::find(Auth::user()->package_id);
            $history = EmailHistory::where('sender_id', Auth::id())->pluck('id');
            $email_sent = 0;
            if(count($history) > 0){
                $email_sent = EmailSentList::whereIn('email_history_id', $history)->count();
            }


            return view('home', get_defined_vars());
        } else {
            //if no user registration first.
            $user = User::first();
            if (!$user) {
                return redirect('register');
            }
            $frontEnds = FrontEnd::orderBy('created_at', 'asc')->get();
            return view('getting-start', get_defined_vars());
        }

    }

//privacy poilicy page
    public function privacyPolicy()
    {
        $privacy = configValue('privacy');
        return view('terms.view_privacy', get_defined_vars());
    }

// terms and codition
    public function termsCondition()
    {
        $termsCondition = configValue('termCondition');
        return view('terms.view_privacy', get_defined_vars());
    }

//delete data in demo version
    public function clearDemoDatabaseDaily()
    {
        if (env('APP_ENV') == 'demo') {

            // migrate all table
            Artisan::call('migrate:refresh', [
                '--force' => true,
            ]);

            //seed all table
            Artisan::call('db:seed', [
                '--force' => true,
            ]);
            Log::info('Demo data deleted at', [date('d-m-Y h:i:s:A')]);
        }
    }

    public function download_sample()
    {
        return Response::download(base_path('sample_email_list.csv'));
    }

    public function clearLog()
    {
       $f = @fopen(storage_path('logs/laravel.log'), "r+");
       if ($f !== false) {
           ftruncate($f, 0);
           fclose($f);
       }
        Log::error('Clear log');
    }
}
