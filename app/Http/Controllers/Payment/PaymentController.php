<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Package\Package;
use App\Http\Controllers\Controller;
use App\Models\PurchaseHistory\PurchaseHistory;
use Paystack, Auth, Session;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        $packagePrice = isset($paymentDetails['data']['amount']) ? ($paymentDetails['data']['amount']) / 100 : '';
        $packages = Package::where('price', $packagePrice)->get();
        $packageName = $packages[0]['name'];
        if($paymentDetails['status'] == 'true' && $packagePrice != '') {
        	$user = Auth::user();
            $user->package_id = $packages[0]['id'];
            $user->email_limit = $user->email_limit + $packages[0]['limit'];
            $user->user_limit = $user->user_limit + $packages[0]['user_limit'];
            $user->package_activated_at = time();
            $user->save();

            $purchaseData = [
                'user_id' => Auth::id(),
                'package_name' => $packages[0]['name'],
                'package_validity' => $packages[0]['validity'],
                'package_price' => $packagePrice,
                'package_limit' => $packages[0]['limit']
            ];

            PurchaseHistory::create($purchaseData);

            Session::flash('success_msg', "Successfully completed the Payment. Now you are in $packageName package.");
        	return redirect('/');
        } else {
        	Session::flash('error_msg', 'Something happened wrong');
        	return redirect()->back();
        }
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }
}