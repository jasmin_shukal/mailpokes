<?php

namespace App\Http\Controllers\Address;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;

class AddressController extends Controller
{
    public function index()
    {
        $pageName = trans('common.newsletter');
        $address = User::where('id', Auth::id())->pluck('address')->first();
        $pageName = trans('common.physical') . ' ' . trans('common.address'). ' ' . trans('common.settings');
        return view('address.home', get_defined_vars());
    }

    public function store(Request $request)
    {
            $data['address'] = $request->address;
            User::where('id', Auth::id())->update($data);
            Session::flash('success_msg', 'Your physical address Successfully.');
            return redirect('address');
    }
}
