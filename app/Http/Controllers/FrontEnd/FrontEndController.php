<?php

namespace App\Http\Controllers\FrontEnd;

use App\Models\FrontEnd\FrontEnd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session, Log;

class FrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageName = trans('common.frontend');
        $frontEnds = FrontEnd::all();
        return view('front_end.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, FrontEnd $frontEnd)
    {
        if(env('APP_ENV') != 'demo'){
            $frontEnd->create($request->all());
            Log::error($request->all());
            return response()->json(['error' => false, 'msg' => 'Successfully added']);
        
        }else{
            return response()->json(['error' => true, 'msg' => "Can't be created in demo version"]);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FrontEnd $frontend)
    {
        return response()->json(['error' => false, 'data' => $frontend]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FrontEnd $frontend)
    {
        if(env('APP_ENV') != 'demo'){
            $frontend->update($request->all());
            return response()->json(['error' => false, 'msg' => 'Successfully updated']);
        }else{
            return response()->json(['error' => true, 'msg' => "Can't be updated in demo version"]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FrontEnd $frontend)
    {
        $frontend->delete();
        Session::flash('success_msg', 'Successfully deleted');
        return redirect('frontend');
    }
}
