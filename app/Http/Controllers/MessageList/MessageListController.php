<?php

namespace App\Http\Controllers\MessageList;

use App\Models\Template\Template;
use Auth;
use App\Models\MessageList\MessageList;
use App\Http\Requests\MessageList\MessageListRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Template\TemplateController;
use Illuminate\Support\Facades\Session;

class MessageListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageName = trans('common.all_message_list');

        $messageLists = MessageList::whereIn('user_id', [0, Auth::id()])->get();

        $title = trans('common.create') . ' ' . trans('common.message_list');

        return view('message_list.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageListRequest $request)
    {
        $requestAll = $request->all();
        $requestAll['user_id'] = Auth::id();

        MessageList::create($requestAll);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(MessageList $messageList)
    {
        $teplateController = new TemplateController();
        return $teplateController->index($messageList->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MessageList $messageList)
    {
        $pageName = trans('common.all_message_list');

        $messageLists = MessageList::whereIn('user_id', [0, Auth::id()])->get();

        $title = trans('common.edit') . ' ' . trans('common.message_list');

        return view('message_list.home', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MessageList $messageList, MessageListRequest $request)
    {
        $messageList->update($request->all());
        Session::flash('success_msg', 'Successfully Updated');
        return redirect('message-list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MessageList $messageList)
    {

        Template::where('message_list_id', $messageList->id)->delete();

        $messageList->delete();
        Session::flash('success_msg', 'Successfully Deleted.');
        return redirect('message-list');
    }

    public function load_template(Request $request){
        if($request->isMethod('POST')){
            return response()->json(Template::where('message_list_id', $request->message_list_id)->get());
        }
    }
}
