<?php

namespace App\Http\Controllers\Tickets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Department\Department;
use App\Models\Ticket\Ticket;
use App\Models\TicketStatus\TicketStatus;
use App\Models\TicketComment\TicketComment;
use Illuminate\Http\JsonResponse;
use Validator;
use Session;
use Auth;
use Log;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return customer_user(superadmin('id'));
        $pageName = trans('tickets.tickets');
        $reporters = User::where('role', '!=', 'superadmin')->where('customer_id', 0)->orderBy('created_at', 'asc')->pluck('name', 'id');
        $priority = [
            'urgent' => trans('tickets.urgent'),
            'high' => trans('tickets.high'),
            'medium' => trans('tickets.medium'),
            'low' => trans('tickets.low'),
        ];
        $department = Department::pluck('name', 'id');

        if(superadmin('id') == Auth::id()) {
            $tickets = Ticket::all();
        } elseif(in_array(Auth::id(), customer_user(superadmin('id')))) {
            if(isset(Auth::user()->department_id_list)) {
                $tickets = Ticket::whereIn('department_id', json_decode(Auth::user()->department_id_list))->get();
            }
        } else {
            $tickets = Ticket::where('created_by', Auth::id())->get();
        }


        return view('Tickets.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Ticket $ticket)
    {
        $this->validate($request, [
                'subject' => 'required',
                'department_id' => 'required',
                'reason' => 'required',
            ]);

        if ($request->hasFile('upload_file')) {
            foreach($request->file('upload_file') as $file) {
                $ext = strtolower($file->getClientOriginalExtension());

                $allRequest = $request->all();
                $allRequest['ext'] = $ext;
                $attachment_types = [
                    'doc',
                    'docx',
                    'txt',
                    'xls',
                    'xlsx',
                    'pdf',
                    'zip',
                    'rar',
                    'jpeg',
                    'jpg',
                    'png',
                    'bmp',
                    'gif',
                    'svg'
                ];

                $config_attachment_type = configValue('attachment_type');

                if($config_attachment_type) {
                    $attachment_types = json_decode($config_attachment_type);
                }

                $attachment_types_txt = implode(',', $attachment_types);

                $messages = [
                    'in' => 'The Upload file must be a file of type: '. $attachment_types_txt .'.',
                ];

                $rules = [
                    'ext' => 'in:'.$attachment_types_txt
                ];

                $validator = Validator::make($allRequest, $rules, $messages);

                if ($validator->fails()) {
                    return new JsonResponse($validator->errors(), 302);
                    // return response()->json($validator->errors()->all());
                    // return redirect()->back()->withErrors($validator, 'error')->withInput();
                }
            }
        }

        $last_id = 1;
        $lastTicket = Ticket::orderBy('created_at', 'desc')->first();
        if($lastTicket) {
            $last_id++; 
        }

        $request['code'] = 'TIC'.$last_id;

        $upload_files = [];
        if ($request->hasFile('upload_file')) {
            foreach($request->file('upload_file') as $file) {
                $originalName = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $tempName = microtime(true) . '.' . $ext;
                
                $destinationPath = 'upload';
                $file->move($destinationPath, $tempName);
                $upload_files[$originalName] = $tempName;
            }
        }

        // if(count($upload_files) > 0) {
            $request['attachments'] = json_encode($upload_files);
        // }

        $request['reporter'] = 0;
        
        if(superadmin('id') != Auth::id() && user_or_customer_info(Auth::id(), 'customer_id') != superadmin('id')) {

            $request['reporter'] = Auth::id();
            if(Auth::user()->customer_id != 0) {
                $request['reporter'] = Auth::user()->customer_id;
            }

            $request['priority'] = 'high';
        }

        $ticketStatus = TicketStatus::where('status', 'Open')->first();

        if(! $ticketStatus) {
            return response()->json(['msg' => 'Ticket Status must be required.']);
        }

        $request['status'] = $ticketStatus->id;

        $ticket = $ticket->create($request->all());

        return response()->json(['msg' => 'Successfully created a ticket']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        if(($ticket->created_by == Auth::id()) || (superadmin('id') == Auth::id()) || in_array(Auth::id(), department_user($ticket->department_id))) {
            $pageName = trans('tickets.tickets');

            $reporters = User::where('role', '!=', 'superadmin')->where('customer_id', 0)->orderBy('created_at', 'asc')->pluck('name', 'id');
            $priority = [
                'urgent' => trans('tickets.urgent'),
                'high' => trans('tickets.high'),
                'medium' => trans('tickets.medium'),
                'low' => trans('tickets.low'),
            ];
            $department = Department::pluck('name', 'id');

            if(superadmin('id') == Auth::id()) {
                $tickets = Ticket::all();
            } elseif(in_array(Auth::id(), customer_user(superadmin('id')))) {
                if(isset(Auth::user()->department_id_list)) {
                    $tickets = Ticket::whereIn('department_id', json_decode(Auth::user()->department_id_list))->get();
                }
            } else {
                $tickets = Ticket::where('created_by', Auth::id())->get();
            }

            $ticket_status = TicketStatus::all();

            $ticket_comments = TicketComment::where('ticket_id', $ticket->id)->orderBy('created_at', 'asc')->get();

            return view('Tickets.detail', get_defined_vars());
        } else {
            Session::flash('error_msg', 'You have no access this ticket.');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        return response()->json(['error' => false, 'data' => $ticket]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        if ($request->hasFile('upload_file')) {
            foreach($request->file('upload_file') as $file) {
                $ext = strtolower($file->getClientOriginalExtension());

                $allRequest = $request->all();
                $allRequest['ext'] = $ext;
                $attachment_types = [
                    'doc',
                    'docx',
                    'txt',
                    'xls',
                    'xlsx',
                    'pdf',
                    'zip',
                    'rar',
                    'jpeg',
                    'jpg',
                    'png',
                    'bmp',
                    'gif',
                    'svg'
                ];

                $config_attachment_type = configValue('attachment_type');

                if($config_attachment_type) {
                    $attachment_types = json_decode($config_attachment_type);
                }

                $attachment_types_txt = implode(',', $attachment_types);

                $messages = [
                    'in' => 'The Upload file must be a file of type: '. $attachment_types_txt .'.',
                ];

                $rules = [
                    'ext' => 'in:'.$attachment_types_txt
                ];

                $validator = Validator::make($allRequest, $rules, $messages);

                if ($validator->fails()) {
                    return new JsonResponse($validator->errors(), 302);
                    // return response()->json($validator->errors()->all());
                    // return redirect()->back()->withErrors($validator, 'error')->withInput();
                }
            }
        }


        $upload_files = [];

        $remove_attach = [];

        if($request->has('previous_attach')) {
            $previous_attach = (array) json_decode($request->previous_attach);

            if((count($previous_attach) > 0) && (count(json_decode($ticket->attachments)) > 0)) {
                $remove_attach = array_diff((array) json_decode($ticket->attachments), $previous_attach);

                $upload_files = array_intersect((array) json_decode($ticket->attachments), $previous_attach);
            }
        }

        foreach ($remove_attach as $attachment) {
            unlink('upload/'.$attachment);
        }


        if ($request->hasFile('upload_file')) {
            foreach($request->file('upload_file') as $file) {
                $originalName = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $tempName = microtime(true) . '.' . $ext;
                
                $destinationPath = 'upload';
                $file->move($destinationPath, $tempName);
                $upload_files[$originalName] = $tempName;
            }
        }

        // if(count($upload_files) > 0) {
            $request['attachments'] = json_encode($upload_files);
        // }

        $ticket->update($request->all());

        return response()->json(['error' => false, 'msg' => 'Successfully updated token information.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        if($ticket->created_by == Auth::id() || (superadmin('id') == Auth::id())) {
            if(isset($ticket->attachments)){
                foreach(json_decode($ticket->attachments) as $attachment) {
                    unlink('upload/'.$attachment);
                }
            }

            $ticket_comments = TicketComment::where('ticket_id', $ticket->id)->get();

            foreach ($ticket_comments as $comment) {
                if(isset($comment->attachments)){
                    foreach(json_decode($comment->attachments) as $comment_attachment) {
                        unlink('upload/'.$comment_attachment);
                    }
                }

                $comment->delete();
            }

            $ticket->delete();
            Session::flash('success_msg', 'Successfully deleted ticket');
            return redirect('tickets');
        } else {
            Session::flash('error_msg', 'You are not able to delete this ticket.');
            return redirect()->back();
        }
    }

    public function attachment_download($file_name) {
        return response()->download(base_path('public/upload/'.$file_name));
    }

    public function change_status($ticket_id, $status_id) {
        $ticket = Ticket::find($ticket_id);
        if(!$ticket) {
            Session::flash('error_msg', 'Ticket not found. Please try again.');
            return redirect()->back();
        }

        $ticket_status = TicketStatus::find($status_id);
        if(! $ticket_status) {
            Session::flash('error_msg', 'Status not found. Please try again.');
            return redirect()->back();
        }

        if(($ticket->created_by == Auth::id()) || (superadmin('id') == Auth::id()) || in_array(Auth::id(), department_user($ticket->department_id))) {
            
            $ticket->status = $ticket_status->id;
            $ticket->save();

            Session::flash('success_msg', 'Successfully changed ticket status.');
            return redirect()->back();
        }


    }

    public function comment_save(Request $request) {
        if ($request->hasFile('comment_files')) {
            foreach($request->file('comment_files') as $file) {
                $ext = strtolower($file->getClientOriginalExtension());
                Log::error($ext);

                $allRequest = $request->all();
                $allRequest['ext'] = $ext;
                $attachment_types = [
                    'doc',
                    'docx',
                    'txt',
                    'xls',
                    'xlsx',
                    'pdf',
                    'zip',
                    'rar',
                    'jpeg',
                    'jpg',
                    'png',
                    'bmp',
                    'gif',
                    'svg'
                ];

                $config_attachment_type = configValue('attachment_type');

                if($config_attachment_type) {
                    $attachment_types = json_decode($config_attachment_type);
                }

                $attachment_types_txt = implode(',', $attachment_types);

                $messages = [
                    'in' => 'The Upload file must be a file of type: '. $attachment_types_txt .'.',
                ];

                $rules = [
                    'ext' => 'in:'.$attachment_types_txt
                ];
                Log::error($rules);

                $validator = Validator::make($allRequest, $rules, $messages);

                if ($validator->fails()) {
                    // return new JsonResponse($validator->errors(), 302);
                    // return response()->json($validator->errors()->all());
                    // return redirect()->back()->withErrors($validator, 'error')->withInput();
                    Session::flash('error_msg', $validator->errors()->all()[0]);
                    return redirect()->back();
                }
            }
        }

        if(! $request->has('ticket_id')) {
            Session::flash('error_msg', 'Something happened wrong. Try again.');
            return redirect()->back();
        }

        $ticket = Ticket::find($request->ticket_id);
        
        if(! $ticket) {
            Session::flash('error_msg', 'Ticket not found. Please try again.');
            return reditect()->back();
        }

        if(($ticket->created_by == Auth::id()) || (superadmin('id') == Auth::id()) || in_array(Auth::id(), department_user($ticket->department_id))) {

            $upload_files = [];
            if ($request->hasFile('comment_files')) {
                foreach($request->file('comment_files') as $file) {
                    $originalName = $file->getClientOriginalName();
                    $ext = $file->getClientOriginalExtension();
                    $tempName = microtime(true) . '.' . $ext;
                    
                    $destinationPath = 'upload';
                    $file->move($destinationPath, $tempName);
                    $upload_files[$originalName] = $tempName;
                }
            }

            if(! $request->has('comments') && (count($upload_files) == 0)) {
                Session::flash('error_msg', 'Empty comment not allowed.');
                return redirect()->back();
            }

            $request['attachments'] = json_encode($upload_files);

            $request['ticket_id'] = $ticket->id;
            $request['user_id'] = Auth::id();

            TicketComment::create($request->all());

            return redirect()->back();
        
        } else {
            Session::flash('error_msg', 'You are not able to comment for this ticket.');
            return redirect()->back();
        }
    }

    public function comment_delete(TicketComment $ticket_comment) {
        if(($ticket_comment->user_id == Auth::id()) || (superadmin('id') == Auth::id())) {
            if(isset($ticket_comment->attachments)){
                foreach(json_decode($ticket_comment->attachments) as $comment_attachment) {
                    unlink('upload/'.$comment_attachment);
                }
            }

            $ticket_comment->delete();

            return redirect()->back();
        } else {
            Session::flash('error_msg', 'You are not able to delete this comment.');
            return redirect()->back();
        }
    }
}
