<?php

namespace App\Http\Controllers\Newsletter;

use Illuminate\Http\Request;

use Auth;
use App\Models\LetterNews\Newsletter;
use App\Models\Media\Media;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class NewsletterController extends Controller
{
    public function index()
    {
        //loaded email list index page
        $pageName = trans('common.newsletter');

        $newsletters = Newsletter::where('user_id', Auth::id())->get();
        $medias = Media::get();
        $title = trans('common.create') . ' ' . trans('common.newsletter');

        return view('newsletter.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request['user_id'] = Auth::id();
        Newsletter::create($request->all());
        Session::flash('success_msg', 'Successfully added');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Newsletter $newsletter)
    {
        return view('newsletter.detail', get_defined_vars());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Newsletter $newsletter)
    {
        $newsletters = Newsletter::where('user_id', Auth::id())->get();
        $title = trans('common.edit') . ' ' . trans('common.newsletter');
//        $messageLists = MessageList::whereIn('user_id', [0, Auth::id()])->pluck('name', 'id');
        return view('newsletter.home', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Newsletter $newsletter, Request $request)
    {
        $newsletter->update($request->all());
        Session::flash('success_msg', 'Successfully Updated');
        return redirect('newsletter');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Newsletter $newsletter)
    {
        $newsletter->delete();
        Session::flash('success_msg', 'Successfully Deleted.');
        return redirect('newsletter');
    }
}
