<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Template\TemplateController;
use App\Models\Package\Package;
use App\Models\PurchaseHistory\PurchaseHistory;
use App\User;
use App\Models\Activity\Activity;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Users\UsersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\ApiController;
use App\Models\Department\Department;
use App\Models\Attachment\Attachment;
use App\Models\Blacklist\Blacklist;
use App\Models\EmailHistory\EmailHistory;
use App\Models\Group\Group;
use App\Models\EmailList\EmailList;
use App\Models\EmailSentList\EmailSentList;
use App\Models\Email\Email;
use App\Models\Media\Media;
use App\Models\Template\Template;
use App\Models\Ticket\Ticket;
use App\Models\TicketComment\TicketComment;

class UsersController extends Controller
{
    public function __construct()
    {
//        $this->middleware('superadmin')->except('update_password');
    }

    public function index()
    {
        $title = trans('common.add') . ' ' . trans('common.user');
        // get all users
        if(Auth::user()->role == 'superadmin') {
            $allUsers = User::where('role', 'user')->get();
        } else {
            $allUsers = User::where('customer_id', Auth::id())->where('role', 'user')->get();
        }
        $customers = User::where('role', 'customer')->orderBy('created_at', 'asc')->pluck('name', 'id');
        if(Auth::id() == superadmin('id')) {
            $departments = Department::pluck('name', 'id');
        }    
        $pageName = trans('common.all') . ' ' . trans('common.users');
        return view('users.home', get_defined_vars());
    }

    // add new user
    public function store(UsersRequest $request, User $user)
    {
        // return $request->all();
        $customer_id = 0;
        if ($request->has('customer_id')) {
            $customer_id = $request->customer_id;
        } else {
            $customer_id = Auth::id();
        }
        if($customer_id == 0) {
            Session::flash('error_msg', "Not found Customer or User");
            return redirect()->back();
        }

        if($customer_id == superadmin('id') && ! $request->has('department_id')) {
            Session::flash('error_msg', 'You must select department');
            return redirect()->back()->withInput();
        }


        if($customer_id != superadmin('id')) {
            $customer = User::find($customer_id);
            if($customer) {
                $package = Package::find($customer->package_id);
                if($package){
                    if(time() > strtotime(date('d-m-Y', $customer->package_activated_at)."+ $package->validity days")) {
                        Session::flash('error_msg', 'Your validity is expired. Please upgrade your package');
                        return redirect()->back();
                    }

                    if($customer->user_limit <= 0) {
                        Session::flash('error_msg', "User addition limit is not available. Please upgrade your package");
                        return redirect()->back();
                    }
                } else {
                    if($customer->role != 'superadmin') {
                        Session::flash('error_msg', "Please purchase a package first.");
                        return redirect()->back();
                    }
                }
            } else {
                Session::flash('error_msg', "Not found Customer or User");
                return redirect()->back();
            }
        }
        // get all request data from

        $allRequest = $request->all();
        $allRequest['password'] = Hash::make($request->password);
        $allRequest['status'] = 'Active';
        $allRequest['role'] = 'user';
        $allRequest['customer_id'] = $customer_id;
        $allRequest['package_id'] = 0;
        $allRequest['email_limit'] = 0;
        $allRequest['user_limit'] = 0;
        $allRequest['email_settings_id'] = 0;
        if($customer_id == superadmin('id')) {
            $allRequest['department_id_list'] = json_encode($request->department_id);
        }
        $user = $user->create($allRequest);

        // customer user addition limit be minus -1 for adding a user
        if($customer_id != superadmin('id')) {
            $customer->user_limit = $customer->user_limit - 1;
            $customer->save();
        }    

        $user_id = Auth::id();
        $message = 'Created user ' . $user->email;
        //save into activity
        Activity::saveActivity($user_id, $message);
        TemplateController::templateForAll($user->id);
        Session::flash('success_msg', 'Successfully created a User.');

        return redirect('users');
    }

    public function edit(User $user)
    {
        //get all user
        if(Auth::user()->role == 'superadmin') {
            $allUsers = User::where('role', 'user')->get();
        } else {
            $allUsers = User::where('customer_id', Auth::id())->where('role', 'user')->get();
        }
        $customers = User::where('customer_id', 0)->orderBy('created_at', 'asc')->pluck('name', 'id');
        if(Auth::id() == superadmin('id')) {
            $departments = Department::pluck('name', 'id');
        }
        $userInfo = $user;
        $title = trans('common.edit') . ' ' . trans('common.user');

        return view('users.home', get_defined_vars());

    }

    public function update(UsersRequest $request, User $user)
    {
        //updated user info
        $data['name'] = $request->name;
        if ($request->has('customer_id')) {
            $data['customer_id'] = $request->customer_id;
        } else {
            $data['customer_id'] = Auth::id();
        }

        if($data['customer_id'] == superadmin('id') && ! $request->has('department_id')) {
            Session::flash('error_msg', 'You must select department');
            return redirect()->back()->withInput();
        }

        if($request->has('password')) {
            $data['password'] = Hash::make($request->password);
        }
        
        if ($request->email != $user->email) {
            $data['email'] = $request->email;
        }

        if($data['customer_id'] == superadmin('id')) {
            $data['department_id_list'] = json_encode($request->department_id);
        }

        $user->department_id_list = '';
        $user->save();

        $user->update($data);

        $user_id = Auth::id();
        $message = 'Updated user ' . $user->email;
        Activity::saveActivity($user_id, $message);

        Session::flash('success_msg', 'Successfully Updated User.');
        return redirect('users');

    }

    public function destroy(User $user)
    {
        //deleted user
        $user_id = Auth::id();
        $message = 'Deleted user ' . $user->email;
        Activity::saveActivity($user_id, $message);

        // deleted all activity according to this user
        Activity::where('user_id', $user->id)->delete();

        //deleted all attachement according to this user
        Attachment::where('user_id', $user->id)->delete();

        //deleted all blacklist according to this user
        Blacklist::where('user_id', $user->id)->delete();

        $email_history = EmailHistory::where('sender_id', $user->id)->pluck('id');

        EmailSentList::whereIn('email_history_id', $email_history)->delete();

        //deleted all EmailHistory according to this user
        EmailHistory::where('sender_id', $user->id)->delete();

        $groups = Group::where('user_id', $user->id)->pluck('id');

        EmailList::whereIn('group_id', $groups)->delete();
        
        Group::where('user_id', $user->id)->delete();

        Email::where('user_id', $user->id)->delete();

        Media::where('user_id', $user->id)->delete();

        Template::where('user_id', $user->id)->delete();

        $tickets = Ticket::where('created_by', $user->id)->pluck('id');

        TicketComment::whereIn('ticket_id', $tickets)->delete();

        Ticket::where('created_by', $user->id)->delete();

        PurchaseHistory::where('user_id', $user->id)->delete();

        $user->delete();

        Session::flash('success_msg', 'Successfully Deleted User.');
        return redirect('users');
    }

    public function change_status($user_id)
    {
        // get user by user_id
        $user = User::find($user_id);

        if ($user->status == 'Active') {// check user status (Active, Banned) and set
            $user->status = 'Banned';
        } else if ($user->status == 'Banned') {
            $user->status = 'Active';
        }
        $user->save();
        $message = $user->status . ' user ' . $user->email;
        Activity::saveActivity(Auth::id(), $message);
        return redirect('users');
    }

    public function active_users() // get all active user
    {
        $title = trans('common.add') . ' ' . trans('common.user');
        $pageName = trans('common.all') . ' ' . trans('common.users');
        // only active users
        if(Auth::user()->role == 'superadmin') {
            $allUsers = User::where('role', 'user')->where('status', 'Active')->get();
        } else {
            $allUsers = User::where('customer_id', Auth::id())->where('role', 'user')->where('status', 'Active')->get();
        }
//        $allUsers = User::where('status', 'Active')->get();
        $customers = User::where('customer_id', 0)->orderBy('created_at', 'asc')->pluck('name', 'id');
        return view('users.home', get_defined_vars());
    }

    public function banned_users() // get all banned user
    {
        $title = trans('common.add') . ' ' . trans('common.user');
        $pageName = trans('common.all') . ' ' . trans('common.users');
        //only banned users
        if(Auth::user()->role == 'superadmin') {
            $allUsers = User::where('role', 'user')->where('status', 'Banned')->get();
        } else {
            $allUsers = User::where('customer_id', Auth::id())->where('role', 'user')->where('status', 'Banned')->get();
        }
        $allUsers = User::where('status', 'Banned')->get();
        $customers = User::where('customer_id', 0)->orderBy('created_at', 'asc')->pluck('name', 'id');
        return view('users.home', get_defined_vars());
    }

    // this method is for updating user password
    public function update_password(Request $request)
    {
        if ($request->method() == 'POST') {
            $rules = array(
                'previous_password' => 'required|min:6',
                'new_password' => 'required|min:6',
                'retype_password' => 'required|same:new_password'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return Redirect('users/update_password')
                    ->withErrors($validator);
            } else {
                if (Auth::attempt(array('email' => Auth::user()->email, 'password' => $request->input('previous_password')), true)) {
                    $user = User::find(Auth::id());
                    $user->password = Hash::make($request->input('new_password'));
                    $user->save();
                    Session::flash('success_msg', 'Your password successfully updated');
                    return Redirect('/');
                } else {
                    Session::flash('error_msg', 'Your password Missmatched');
                    return Redirect('users/update_password');
                }
            }
        }
        return view('users.update_password');
    }
}
