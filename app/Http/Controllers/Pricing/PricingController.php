<?php

namespace App\Http\Controllers\Pricing;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Package\Package;
use App\User;
use App\Models\PurchaseHistory\PurchaseHistory;
use App\Models\Activity\Activity;
use Auth;
use Session;

class PricingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superadminORcustomer');
    }

    public function index()
    {
    	$customers = User::where('customer_id', 0)->orderBy('created_at', 'asc')->pluck('name', 'id');
        $packages = Package::all();
        return view('pricing.home', get_defined_vars());
    }

    public function store(Request $request) {
    	$package = Package::find($request->package_id);

        $customer = User::find($request->customer_id);

        $data['package_id'] = $package->id;
        $data['email_limit'] = ($customer->email_limit + $package->limit);
        $data['user_limit'] = ($customer->user_limit + $package->user_limit);
        $data['package_activated_at'] = time();
        $customer->update($data);

        $purchaseData = [
            'user_id' => $customer->id,
            'package_name' => $package->name,
            'package_validity' => $package->validity,
            'package_price' => $package->price,
            'package_limit' => $package->limit,
            'package_user_limit' => $package->user_limit
        ];

        PurchaseHistory::create($purchaseData);

        $user_id = Auth::id();
        $message = 'Super admin assigned ' . $package->name. ' package to '. $customer->name;
        //save into activity
        Activity::saveActivity($user_id, $message);

        Session::flash('success_msg', 'Super admin assigned ' . $package->name. ' package to '. $customer->name);

        return redirect('pricing');
    }
}
