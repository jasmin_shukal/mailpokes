<?php

namespace App\Http\Controllers\Attachment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Attachment\Attachment;
use Validator;
use Session;
use Auth;
use Log;

class AttachmentController extends Controller
{
    
    public function index()
    {
        $attachment_types = [
            'doc',
            'docx',
            'txt',
            'xls',
            'xlsx',
            'pdf',
            'zip',
            'rar',
            'jpeg',
            'jpg',
            'png',
            'bmp',
            'gif',
            'svg'
        ];

        $config_attachment_type = configValue('attachment_type');

        if($config_attachment_type) {
            $attachment_types = json_decode($config_attachment_type);
        }

        $attachment_types_txt = implode(', ', $attachment_types);

    	$attachments = Attachment::where('user_id', Auth::id())->get();
    	$add_edit_title = trans('common.new_attachment');
        return view('attachment.home', get_defined_vars());
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request, Attachment $model)
    {
        // $type = 'mimes: jpeg, jpg, png, gif, doc, xls';
        // if(configValue('attachment_type')){
        //     $type = 'mimes: '.implode(', ', json_decode(configValue('attachment_type')));
        // }

        $this->validate($request, [
            'title' => 'required|unique:attachments',
            'attachment' => 'required'
        ]);

        $file = $request->file('attachment');
        $ext = strtolower($file->getClientOriginalExtension());

        $allRequest = $request->all();
        $allRequest['ext'] = $ext;

        $attachment_types = [
            'doc',
            'docx',
            'txt',
            'xls',
            'xlsx',
            'pdf',
            'zip',
            'rar',
            'jpeg',
            'jpg',
            'png',
            'bmp',
            'gif',
            'svg'
        ];

        $config_attachment_type = configValue('attachment_type');

        if($config_attachment_type) {
            $attachment_types = json_decode($config_attachment_type);
        }

        $attachment_types_txt = implode(',', $attachment_types);

        $messages = [
            'in' => 'The Upload file must be a file of type: '. $attachment_types_txt .'.',
        ];

        $rules = [
            'ext' => 'in:'.$attachment_types_txt
        ];

        $validator = Validator::make($allRequest, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $attachment = '';
        if ($request->file('attachment')) {
            $ext = $request->file('attachment')->getClientOriginalExtension();
            $attachment = microtime(true) . '.' . $ext;
            $destinationPath = 'attachment';
            $request->file('attachment')->move($destinationPath, $attachment);
        }

        $all = $request->all();
        $all['user_id'] = Auth::id();
        $all['path'] = $attachment;
        $model = $model->create($all);

        Session::flash('success_msg', 'Successfully saved attachment file.');
        return redirect('attachments');
    }

    
    public function show(Attachment $attachment)
    {
        return response()->download("attachment/$attachment->path");
    }

    
    public function edit(Attachment $attachment)
    {
    	$attachInfo = $attachment;
        $attachments = Attachment::where('user_id', Auth::id())->get();
        $add_edit_title = trans('common.edit') . ' ' . trans('common.attachment');
        return view('attachment.home', get_defined_vars());
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy(Attachment $attachment)
    {
        unlink("attachment/$attachment->path");
        $attachment->delete();

        Session::flash('success_msg', 'Successfully deleted attachment file.');
        return redirect('attachments');
    }
}
