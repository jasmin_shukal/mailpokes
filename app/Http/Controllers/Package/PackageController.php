<?php

namespace App\Http\Controllers\Package;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Package\Package;
use App\Http\Requests\Package\PackageRequest;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Paypal\PaypalController;
use App\Http\Controllers\Stripe\StripeController;
use App\Models\PurchaseHistory\PurchaseHistory;
use App\Models\Coupon\Coupon;
use Auth;

class PackageController extends Controller
{

    public function __construct()
    {
        $this->middleware('superadminORcustomer', ['except' => 'show']);
    }

    public function index()
    {
        $packages = Package::all();
        $title = trans('common.package');
        $pageName = trans('common.package');
        return view('package.home', get_defined_vars());
    }

    public function show(Request $request, Package $package) {

        if($request->has('code')) {
            $coupon = Coupon::where('code', $request->code)
                            ->where('published', 'yes')
                            ->where('package_id', 'like', '%"'. $package->id .'"%')
                            ->first();
            if(! $coupon) {
                Session::flash('error_msg', trans('common.code_does_not_match'));
                return redirect()->back();
            }
        }

        if(!Auth::check()){
            if(isset($coupon)) {
                return redirect('register')->with('package_id', $package->id)->with('code', $coupon->code);
            } else {
                return redirect('register')->with('package_id', $package->id);
            }
        }
        Session::flash('error_msg', 'Something happened wrong.');
        return redirect()->back();
    }

    public function store(PackageRequest $request)
    {
        Package::create($request->all());
        Session::flash('success_msg', 'Successfully Inserted');
        return redirect()->back();
    }

    public function edit(Package $package)
    {
        $packages = Package::get();
        $title = trans('common.edit') . ' ' . trans('common.package');
        $pageName = trans('common.edit') . ' ' . trans('common.package');
        return view('package.home', get_defined_vars());
    }


    public function update(Package $package, PackageRequest $request)
    {
        $package->update($request->all());
        Session::flash('success_msg', 'Successfully Updated');
        return redirect('package');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        Session::flash('success_msg', 'Successfully Deleted.');
        return redirect('package');
    }

    public function payment(Request $request) {
        
     
        if(! $request->has('package_id')) {
            Session::flash('error_msg', 'You must be select a package.');
            return redirect()->back();
        } else {
            $package = Package::find($request->package_id);
            if (! $package) {
                Session::flash('error_msg', 'It\'s not valid package.');
                return redirect()->back();
            }
        }

        if($request->has('code')) {
            $coupon = Coupon::where('code', $request->code)
                            ->where('published', 'yes')
                            ->where('package_id', 'like', '%"'. $package->id .'"%')
                            ->first();
            if(! $coupon) {
                Session::flash('error_msg', trans('common.code_does_not_match'));
                return redirect()->back();
            }
        }

        if($request->payment == 'paypal') {

            $clientId = configValue('client_id');
            $clientSecret = configValue('client_secret');
            if (!$clientId || !$clientSecret) {
                Session::flash('error_msg', 'Paypal service is not available right now.');
                return redirect()->back();
            }

            if(isset($coupon)) {
                return PaypalController::paypal($package->id, $coupon->id);
            } else {
                return PaypalController::paypal($package->id);
            }
        } else if($request->payment == 'card') {
            $this->validate($request, [
                    'number' => 'required',
                    'exp_month' => 'required',
                    'exp_year' => 'required',
                    'cvc' => 'required'
                ]);

            $stripeSecret = configValue('stripe_secret');
            if (!$stripeSecret) {
                Session::flash('error_msg', 'Stripe service is not available right now.');
                return redirect()->back();
            }
        
            if(isset($coupon)) {
                return StripeController::stripe($package->id, $request->except(['_token', 'payment', 'package_id', 'code']), $coupon->id);
            } else {
                return StripeController::stripe($package->id, $request->except(['_token', 'payment', 'package_id', 'code']));
            }

        } else {
            Session::flash('error_msg', 'Please select a payment method');
            return redirect()->back();
        }
    }

    public function order(Request $request, Package $package) {
        
       
        
        if(!isset($package)) {
            Session::flash('error_msg', 'Package not found. Please try again.');
            return redirect('pricing');
        }

        if($request->has('code')) {
            $coupon = Coupon::where('code', $request->code)
                            ->where('published', 'yes')
                            ->where('package_id', 'like', '%"'. $package->id .'"%')
                            ->first();
            if(! $coupon) {
                Session::flash('error_msg', trans('common.code_does_not_match'));
                return redirect('pricing');
            }
        }

        $price = $package->price;
        
        if($price == 0) {
            $user = Auth::user();
            $user->package_id = $package->id;
            $user->email_limit = $user->email_limit + $package->limit;
            $user->user_limit = $user->user_limit + $package->user_limit;
            $user->package_activated_at = time();
             $user->save();

            $purchaseData = [
                'user_id' => Auth::id(),
                'package_name' => $package->name,
                'package_validity' => $package->validity,
                'package_price' => $price,
                'package_limit' => $package->limit
            ];

             PurchaseHistory::create($purchaseData);

            Session::flash('success_msg', "Successfully completed the Payment. Now you are in $package->name package.");
            Session::flash('free_package_flag','1');
            return redirect('/email');
        }

        if(Auth::check()){
            $pageName = trans('common.payment');
            return view('payment.home', get_defined_vars());
        } else {
            if(isset($coupon)) {
                return redirect('register')->with('package_id', $package->id)->with('coupon_id', $coupon->id);
            } else {
                return redirect('register')->with('package_id', $package->id);
            }
        }
    }
}
