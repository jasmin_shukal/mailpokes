<?php

namespace App\Http\Controllers\Paypal;

use App\Models\Package\Package;
use App\Models\PurchaseHistory\PurchaseHistory;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Session;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use App\Models\Coupon\Coupon;
use Log;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class PaypalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public static function paypal($package_id, $coupon_id = null)
    {

        $package = Package::find($package_id);
        if ($package) {
            if($package->price > 0) {
                if(isset($coupon_id)) {
                    $coupon = Coupon::find($coupon_id);
                }

                Session::put('package_id', $package_id);

                if(isset($coupon)) {
                    Session::put('coupon_id', $coupon->id);
                }

                $clientId = configValue('client_id');
                $clientSecret = configValue('client_secret');
                if (!$clientId || !$clientSecret) {
                    Session::flash('error_msg', 'Please setup your paypal api credentials');
                    return redirect()->back();
                }

                $paypal = new ApiContext(
                    new OAuthTokenCredential(
                        $clientId, $clientSecret
                    )
                );

                $product = $package->name;
                $price = $package->price;
                
                if(isset($coupon_id) && $coupon) {
                    if($coupon->amount_type == 'fixed_amount') {
                        if($coupon->discount_amount > 0) {
                            $price = $price - $coupon->discount_amount;
                        }    
                    } else if($coupon->amount_type == 'percentage_amount') {
                        if($coupon->discount_amount > 0) {
                            $price = $price - ($price * $coupon->discount_amount / 100);
                        }
                    }
                }    

                $shiping = 0;
                $id = rand(1, 1000);
                $total = $price + $shiping;

                $payer = new Payer();
                $payer->setPaymentMethod('paypal');

                $item = new Item();
                $item->setName($product)
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($price);
                $itemList = new ItemList();
                $itemList->setItems([$item]);

                $details = new Details();
                $details->setShipping($shiping)
                    ->setSubtotal($price);

                $amount = new Amount();
                $amount->setCurrency('USD')
                    ->setTotal($total)
                    ->setDetails($details);

                $transaction = new Transaction();
                $transaction->setAmount($amount)
                    ->setItemList($itemList)
                    ->setDescription('Item Description')
                    ->setInvoiceNumber($id);

                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(url('/') . '/pay?success=true')
                    ->setCancelUrl(url('/') . '/pay?success=false');


                $payment = new Payment();
                $payment->setIntent('sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirectUrls)
                    ->setTransactions([$transaction]);

                try {
                    $payment->create($paypal);
                } catch (\Exception $e) {
                    // echo $e->getCode();
                    die($e->getData());
                }

                $approvalUrl = $payment->getApprovalLink();
                return redirect($approvalUrl);
            } else {
                $user = Auth::user();
                $user->package_id = $package->id;
                $user->email_limit = $user->email_limit + $package->limit;
                $user->user_limit = $user->user_limit + $package->user_limit;
                $user->package_activated_at = time();
                $user->save();

                $purchaseData = [
                    'user_id' => Auth::id(),
                    'package_name' => $package->name,
                    'package_validity' => $package->validity,
                    'package_price' => $package->price,
                    'package_limit' => $package->limit,
                    'package_user_limit' => $package->user_limit
                ];

                PurchaseHistory::create($purchaseData);

                Session::flash('success_msg', "Now you are in $package->name package.");
                return redirect('/');
            }
        } else {
            Session::flash('error_msg', 'It\'s not valid package.');
            return redirect()->back();
        }


    }


    // Pay method

    public function pay(Request $request)
    {

        if ((bool)$request->success === false) {
            Session::flash('error_msg', 'Payment cancel');
            return redirect('pricing');
        }

        $clientId = configValue('client_id');
        $clientSecret = configValue('client_secret');

        if (!$clientId || !$clientSecret) {
            Session::flash('error_msg', 'Please setup your paypal api credentials');
            return redirect()->back();
        }

        $paypal = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $clientId, $clientSecret
            )
        );

        $paymentId = $request->paymentId;
        $PayerID = $request->PayerID;

        $payment = Payment::get($paymentId, $paypal);

        $execute = new PaymentExecution();
        $execute->setPayerId($PayerID);

        try {
            $result = $payment->execute($execute, $paypal);
        } catch (Exception $e) {
            $data = json_decode($e->getData());

            Session::flash('error_msg', $data->message);
            return redirect('pricing');
        }

        if(Session::has('package_id')) {
            $package = Package::find(Session::get('package_id'));
        }

        if(isset($package) && $package) {
            $user = Auth::user();
            $user->package_id = $package->id;
            $user->email_limit = $user->email_limit + $package->limit;
            $user->user_limit = $user->user_limit + $package->user_limit;
            $user->package_activated_at = time();
            $user->save();

            $price = $package->price;
            if(Session::has('coupon_id')) {
                $coupon = Coupon::find(Session::get('coupon_id'));
                if($coupon) {
                    if($coupon->amount_type == 'fixed_amount') {
                        if($coupon->discount_amount > 0) {
                            $price = $price - $coupon->discount_amount;
                        }    
                    } else if($coupon->amount_type == 'percentage_amount') {
                        if($coupon->discount_amount > 0) {
                            $price = $price - ($price * $coupon->discount_amount / 100);
                        }
                    }
                }
            }

            $purchaseData = [
                'user_id' => Auth::id(),
                'package_name' => $package->name,
                'package_validity' => $package->validity,
                'package_price' => $price,
                'package_limit' => $package->limit
            ];

            PurchaseHistory::create($purchaseData);
        }

        Session::flash('success_msg', "Successfully completed the Payment. Now you are in $package->name package.");
        return redirect('/');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
