<?php

namespace App\Http\Controllers\blacklist;

use App\Models\Blacklist\Blacklist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\User;
use Auth;
use Log;

class blacklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageName = trans('blacklist.' . request()->segment(1));
        
        if(Auth::id() == superadmin('id')) {
            $blacklist = Blacklist::where('created_by', Auth::id())->where('for', request()->segment(1))->get();

            $customers1[-1] = trans('all');
            $superadmin[superadmin('id')] = trans('common.own'); 
            $all_customers = User::where('role', 'customer')->pluck('name', 'id')->toArray();
            
            $customers = [];
            if(count($all_customers) > 0){
                $customers = $customers1 + $superadmin + $all_customers;
            } else {
                $customers = $superadmin;
            }
        } else {
            $blacklist = Blacklist::where('for', request()->segment(1))
                        ->where( function($query) {
                            $query->where('created_by', Auth::id())
                                ->orWhere('user_id', 'like', '%-1%') //-1 for all user/customers
                                ->orWhere('user_id', 'like', '%"'. Auth::id() .'"%');
                        })->get();
        }

        return view('blacklist.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::id() == superadmin('id')) {
            if(in_array('-1', $request->select_customer)){ // -1 for all user/customer
                $request['user_id'] = json_encode([-1]);
            } else {
                $request['user_id'] = json_encode($request->select_customer); 
            }
        }

        Blacklist::create($request->all());
        return response()->json(['error' => false, 'msg' => trans('blacklist.' . $request['for']) . ' ' . 'Successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blacklist $email_blacklist)
    {
        if(($email_blacklist->created_by == superadmin('id')) && (Auth::id() != superadmin('id'))) {
            return response()->json(['error' => true, 'msg' => trans('blacklist.only_super_admin_edit')]);    
        }

        return response()->json(['error' => false, 'data' => $email_blacklist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blacklist $email_blacklist)
    {
        if(($email_blacklist->created_by == superadmin('id')) && (Auth::id() != superadmin('id'))) {
            return response()->json(['error' => true, 'msg' => trans('blacklist.only_super_admin_edit')]);    
        }

        if(Auth::id() == superadmin('id')) {
            if(in_array('-1', $request->select_customer)){
                $request['user_id'] = json_encode([-1]);
            } else {
                $request['user_id'] = json_encode($request->select_customer); 
            }
        }

        $email_blacklist->update($request->all());
        return response()->json(['error' => false, 'msg' => trans('blacklist.' . $request['for']) . ' ' . 'Successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blacklist $email_blacklist)
    {
        if(($email_blacklist->created_by == superadmin('id')) && (Auth::id() != superadmin('id'))) {
            Session::flash('error_msg', trans('blacklist.only_super_admin_delete'));
            return redirect(request()->segment(1));
        }

        $email_blacklist->delete();
        Session::flash('success_msg', trans('blacklist.' . $email_blacklist['for']) . ' ' . 'Successfully deleted.');
        return redirect(request()->segment(1));
    }
}
