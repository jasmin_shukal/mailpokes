<?php

namespace App\Http\Controllers\ExternalApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;

class ElasticMail extends Controller
{
	public $emailTemplate;
    public $emailId;
    public $email;
    public $subject;
    public $email_info;
    public $mail_provider;
    public $attachment_list;
    public $url;
    private $ajaxRequest;

	public function __construct($email_info, $mail_provider, $emailTemplate = null, $emailId = null, $email, $subject, $attachment_list, $url = null, $ajaxRequest = false)
    {
        $this->email_info = $email_info;
        $this->mail_provider = $mail_provider;
        $this->emailTemplate = $emailTemplate;
        $this->emailId = $emailId;
        $this->email = $email;
        $this->subject = $subject;
        $this->attachment_list = $attachment_list;
        $this->url = $url;
        $this->ajaxRequest = $ajaxRequest;

    }

	public function shoot(){
		Log::error('Elastic Shoot...');
		$email_info = $this->email_info;
        $mail_provider = $this->mail_provider;
        $emailTemplate = $this->emailTemplate;
        $emailId = $this->emailId;
        $email = $this->email;
        $subject = $this->subject;
        $attachment_list = $this->attachment_list;
        $url = $this->url;
        $ajaxRequest = $this->ajaxRequest;

		$elastic_url = 'https://api.elasticemail.com/v2/email/send';

		try{
	        $post = array('from' => $email_info->from_email,
	                      'fromName' => $email_info->name,
	                      'apikey' => $email_info->key,
	                      'subject' => $subject,
	                      'to' => $email,
	                      'bodyHtml' => $emailTemplate,
	                      'isTransactional' => false
	                   );

	        Log::error($post);
	        
	        $ch = curl_init();
	        curl_setopt_array($ch, array(
	            CURLOPT_URL => $elastic_url,
	            CURLOPT_POST => true,
	            CURLOPT_POSTFIELDS => $post,
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_HEADER => false,
	            CURLOPT_SSL_VERIFYPEER => false
	        ));
	        
	        $result=curl_exec($ch);
	        curl_close($ch);
	        
	        Log::error($result);

	        if($ajaxRequest) {
	        	$result = json_decode($result);
	        	
	        	if(! $result->success) {
	        		return response()->json(['error' => true, 'msg' => 'Invalid Credentials. ' . $result->error]);
	        	}
	        	return response()->json(['error' => false, 'msg' => "Mail provider credentials is vaild"]);
	        }

	        return $result;    
		}
		catch(Exception $ex){
			if($ajaxRequest) {
				Log::error($ex->getMessage());
                return response()->json(['error' => true, 'msg' => 'Invalid Credentials. ' . $ex->getMessage()]);
			}
		    return $ex->getMessage();
		}
	}
    
}
