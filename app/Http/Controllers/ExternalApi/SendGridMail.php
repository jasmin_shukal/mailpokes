<?php

namespace App\Http\Controllers\ExternalApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Log;

class SendGridMail extends Controller
{
    public $emailTemplate;
    public $emailId;
    public $email;
    public $subject;
    public $email_info;
    public $mail_provider;
    public $attachment_list;
    public $url;
    private $ajaxRequest;

    public function __construct($email_info, $mail_provider, $emailTemplate = null, $emailId = null, $email, $subject, $attachment_list, $url = null, $ajaxRequest = false)
    {
        $this->email_info = $email_info;
        $this->mail_provider = $mail_provider;
        $this->emailTemplate = $emailTemplate;
        $this->emailId = $emailId;
        $this->email = $email;
        $this->subject = $subject;
        $this->attachment_list = $attachment_list;
        $this->url = $url;
        $this->ajaxRequest = $ajaxRequest;

    }

    public function shoot(){
        Log::error('SendGrid Shoot...');
        $email_info = $this->email_info;
        $mail_provider = $this->mail_provider;
        $emailTemplate = $this->emailTemplate;
        $emailId = $this->emailId;
        $email = $this->email;
        $subject = $this->subject;
        $attachment_list = $this->attachment_list;
        $url = $this->url;
        $ajaxRequest = $this->ajaxRequest;


		$from = new \SendGrid\Email($email_info->name, $email_info->from_email);
		$subject = $subject;
		$to = new \SendGrid\Email(null, $email);
		$content = new \SendGrid\Content("text/html", $emailTemplate);
		$mail = new \SendGrid\Mail($from, $subject, $to, $content);
        foreach($attachment_list as $attachment) {
            if (File::exists(base_path('../attachment/'.$attachment))) {
                $att = new \SendGrid\Attachment();
                $att->setContent(base64_encode(file_get_contents(base_path('../attachment/'.$attachment))));
                $att->setType(mime_content_type(base_path('../attachment/'.$attachment)));
                $att->setFilename($attachment);
                $att->setDisposition("attachment");
                $att->setContentId("Attachment");
                $mail->addAttachment($att);
            }
        }

		$apiKey = $email_info->key;
		$sg = new \SendGrid($apiKey);
        Log::error($apiKey);

		$response = $sg->client->mail()->send()->post($mail);
        Log::error($response->statusCode());
        Log::error($response->headers());
        Log::error($response->body());
        
        if($ajaxRequest) {
            if($response->statusCode() == 201 || $response->statusCode() == 202) {
                return response()->json(['error' => false, 'msg' => "Mail provider credentials is vaild"]);
            }
            $response_body = json_decode($response->body());

            return response()->json(['error' => true, 'msg' => 'Invalid Credentials. '.$response_body->errors[0]->message]);
        }

        return $response;

	}
}
