<?php

namespace App\Http\Controllers\ExternalApi;

use App\Models\Config\Config;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Api\EmailCheckerApi;
use Session;
use Auth;
use App\Http\Controllers\Checker\EmailChecker;

class ExternalApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadminORcustomer');
    }

    public function index()
    {
        //loaded bulk email checker api index page
        $pageName = trans('common.api');

        $bulkEmailChecker = EmailCheckerApi::where('provider', 'bulkemailchecker')->first();
        $emailListVerify = EmailCheckerApi::where('provider', 'emaillistverify')->first();

        return view('api.home', get_defined_vars());
    }

    public function store(Request $request)
    {
        if (env('APP_ENV', false) == 'demo') {
            Session::flash('error_msg', 'Not submitted in demo version.');
            return redirect()->back();
        }
        
        if ($request->has('bulkemailchecker') && $request->input('bulkemailchecker') != ''){

            if(! $request->has('bulkemailchecker_url')) {
                return redirect('api')->withErrors('Bulk Email Checker URL is required.');
            }

            $api = new EmailChecker('bulkemailchecker', $request->bulkemailchecker, $request->bulkemailchecker_url);
            if($api->checkApiKey()){
                Session::flash('error_msg', 'Your Bulk Email Checker API Key or URL is invalid.');
                return redirect('api')->withErrors('Bulk Email Checker API Key or URL is invalid.');
            }
        } 

        if ($request->has('emaillistverify') && $request->input('emaillistverify') !=''){

            $api = new EmailChecker('emaillistverify', $request->emaillistverify);
            
            if($api->checkApiKey()){
                Session::flash('error_msg', 'Your Email List Verify API Key is invalid.');
                return redirect('api')->withErrors('Email List Verify API Key is invalid.');
            }

        }

        if ($request->has('bulkemailchecker') || $request->has('emaillistverify')) {
            $bulkemailchecker = EmailCheckerApi::where('provider', 'bulkemailchecker')->first();


            if ($bulkemailchecker) {
                $bulkemailchecker->update(['key' => $request->input('bulkemailchecker'), 'url' => $request->bulkemailchecker_url]);
            } else {
                EmailCheckerApi::create(
                    [
                        'provider' => 'bulkemailchecker',
                        'key' => $request->input('bulkemailchecker'),
                        'url' => $request->input('bulkemailchecker_url')
                    ]);
            }

            $emailListVerify = EmailCheckerApi::where('provider', 'emaillistverify')->first();
            if ($emailListVerify) {
                $emailListVerify->update(['key' => $request->input('emaillistverify')]);
            } else {
                EmailCheckerApi::create(
                    [
                        'provider' => 'emaillistverify',
                        'key' => $request->input('emaillistverify')
                    ]);
            }

            Session::flash('success_msg', 'Successfully Added.');
        } else {
            Session::flash('error_msg', 'Please enter key.');
        }
        return redirect('api');
    }

    public function paypal_api()
    {
        if (Auth::user()->role == 'superadmin') {
            //loaded bulk email checker api index page
            $pageName = trans('common.paypal_api');

            $client_id = configValue('client_id');
            $client_secret = configValue('client_secret');

            return view('api.paypal', get_defined_vars());
        } else {
            return redirect('/');
        }
    }

    public function store_paypal_api(Request $request)
    {
        if (Auth::user()->role == 'superadmin') {
            if (env('APP_ENV', false) == 'demo') {
                Session::flash('error_msg', 'Not submitted in demo version.');
                return redirect()->back();
            }

            $this->validate($request, [
                'client_id' => 'required',
                'client_secret' => 'required'
            ]);

            $client_id = Config::where('key', 'client_id')->first();
            $client_secret = Config::where('key', 'client_secret')->first();

            if ($client_id) {
                $client_id->update(['value' => $request->client_id]);
            } else {
                Config::create(['key' => 'client_id', 'value' => $request->client_id]);
            }

            if ($client_secret) {
                $client_secret->update(['value' => $request->client_secret]);
            } else {
                Config::create(['key' => 'client_secret', 'value' => $request->client_secret]);
            }

            Session::flash('success_msg', 'Successfully added paypal information.');
            return redirect('paypal-api');
        } else {
            return redirect('/');
        }
    }

    public function stripe_api()
    {
        if (Auth::user()->role == 'superadmin') {
            //loaded bulk email checker api index page
            $pageName = trans('common.stripe_api');

            $stripe_secret = configValue('stripe_secret');
            $stripe_public=configValue('stripe_public');
            
            return view('api.stripe', get_defined_vars());
        } else {
            return redirect('/');
        }
    }

    public function store_stripe_api(Request $request)
    {
        if (Auth::user()->role == 'superadmin') {
            if (env('APP_ENV', false) == 'demo') {
                Session::flash('error_msg', 'Not submitted in demo version.');
                return redirect()->back();
            }

            $this->validate($request, [
                'stripe_secret' => 'required',
                'stripe_public' => 'required'
            ]);

            $stripe_secret = Config::where('key', 'stripe_secret')->first();
            if ($stripe_secret) {
                $stripe_secret->update(['value' => $request->stripe_secret]);
            } else {
                Config::create(['key' => 'stripe_secret', 'value' => $request->stripe_secret]);
            }
            
            $stripe_public = Config::where('key', 'stripe_public')->first();
            if ($stripe_public) {
                $stripe_public->update(['value' => $request->stripe_public]);
            } else {
                Config::create(['key' => 'stripe_public', 'value' => $request->stripe_public]);
            }

            Session::flash('success_msg', 'Successfully added Stripe Secret Key and Public Key');
            return redirect('stripe-api');
        } else {
            return redirect('/');
        }
    }
}
