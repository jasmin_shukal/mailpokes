<?php

namespace App\Http\Controllers\Api;

use App\Models\EmailList\EmailList;
use Illuminate\Http\Request;

use App\User;
use App\Models\Group\Group;
use App\Models\EmailLater\EmailLater;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Log;

class ApiController extends Controller
{
    public function subscribe(Request $request, $api_key = null, $group_id = null)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'api_key' => 'required',
                'group_id' => 'required',
                'email' => 'required|email|max:255|unique:email_list',
            ]);

            $user = User::where('api_key', $request->api_key)->first();
            if ($user) {
                $decode_group_id = base64_decode($request->group_id);
                $group = Group::where('id', $decode_group_id)->where('user_id', $user->id)->first();
                if ($group) {
                    $allRequest = $request->all();
                    $allRequest['group_id'] = $decode_group_id;
                    $allRequest['subscribed'] = 1;
                    $allRequest['free_email_check'] = 0;
                    $allRequest['bulk_check'] = 0;
                    $allRequest['email_list_check'] = 0;
                    EmailList::create($allRequest);
                } else {
                    Session::flash('subscribe_error_msg', 'Please enter valid Group ID.');
                }
            } else {
                Session::flash('subscribe_error_msg', 'Please enter valid Api key.');
            }

            Session::flash('subscribe_success_msg', 'You are subscribed successfully now.');
            return redirect()->back();
        }

        if (!isset($api_key) || !isset($group_id)) {
            return redirect('/');
        }
        return view('subscribe.home', get_defined_vars());

    }

    public function link_open($url){

        return redirect($url);
    }

    public static function generate_api_key()
    {
        return sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',

            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
