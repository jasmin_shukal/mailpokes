<?php

namespace App\Http\Controllers\CustomerGroup;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CustomerGroup\CustomerGroup;
use Auth;
use Session;
use App\Models\Activity\Activity;
use App\Http\Requests\CustomerGroups\CustomerGroupRequest;

class CustomerGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerGroups = CustomerGroup::all();
        return view('customer_group.home', get_defined_vars());
    }

    public function group_save(CustomerGroupRequest $request, CustomerGroup $customerGroup)
    {
//        if (!$request->has('name')) {
//            Session::flash('error_msg', 'Group Name must be required.');
//            return redirect('customer-groups');
//        }
//
//        $existingGroup = CustomerGroup::where('user_id', Auth::id())->where('name', $request->name)->first();
//        if ($existingGroup) {
//            Session::flash('error_msg', 'Group Name must be unique.');
//            return redirect('customer-groups');
//        }

        $allRequest = $request->all();

        $customerGroup->create($allRequest);

        $message = '"'.$customerGroup->name . '" customer group created.';
        Activity::saveActivity(Auth::id(), $message);

        Session::flash('success_msg', 'Successfully added a new group');
        return redirect('customer-groups');
    }

    public function group_edit($group_id)
    {

        $groupInfo = CustomerGroup::find($group_id);
        if($groupInfo){
            $customerGroups = CustomerGroup::all();
            return view('customer_group.home', get_defined_vars());
        }
        return redirect('customer-groups');
    }

    public function group_update(CustomerGroupRequest $request) {
//        if (!$request->has('name')) {
//            Session::flash('error_msg', 'Group Name must be required.');
//            return redirect('customer-groups');
//            return response()->json(['title' => 'Warning', 'msg' => 'Group Name must be required.']);
//        }
//
//        $existingGroup = CustomerGroup::where('user_id', Auth::id())->where('name', $request->value)->first();
//        if ($existingGroup) {
//            return response()->json(['title' => 'Warning', 'msg' => 'Group Name must be unique.']);
//        }

        $customerGroup = CustomerGroup::find($request->group_id);
        $customerGroup->update(['name' => $request->name]);

        $message = '"'.$customerGroup->name . '" customer group updated.';
        Activity::saveActivity(Auth::id(), $message);

        Session::flash('success_msg', 'Successfully updated group name.');
        return redirect('customer-groups');
    }

    public function group_destroy($group_id)
    {
        $customerGroup = CustomerGroup::find($group_id);

        $message = '"'.$customerGroup->name . '" customer group deleted.';
        Activity::saveActivity(Auth::id(), $message);

        $customerGroup->delete();

        Session::flash('success_msg', 'Successfully deleted a group.');
        return redirect('customer-groups');
    }
}
