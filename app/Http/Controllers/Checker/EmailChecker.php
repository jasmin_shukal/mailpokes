<?php

namespace App\Http\Controllers\Checker;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Log;

class EmailChecker extends Controller
{
    protected $key;
    protected $email;
    protected $checker;
    protected $url;

    public function __construct($checker, $key, $url = null)
    {
        //set checker
        $this->checker = $checker;
        // set the api key and email to be validated
        $this->key = $key;
        $this->url = $url;
    }

    public function check($email)
    {
        if ($this->checker == 'bulkemailchecker') {
            $this->email = urlencode($email);
// use curl to make the request

            // $url = 'http://api-v4.bulkemailchecker.com/?key=' . $this->key . '&email=' . $this->email;
            $url = $this->url.'/?key=' . $this->key . '&email=' . $this->email;
            Log::error($url);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            $response = curl_exec($ch);
            curl_close($ch);           
            $json = json_decode($response, true);
            return $json;

        } else if ($this->checker == 'emaillistverify') {

            $url = "https://apps.emaillistverify.com/api/verifyEmail?secret=" . $this->key . "&email=" . $email;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
    }

    public function checkApiKey()
    {
        $email = User::first()->email;
        if ($this->checker == 'bulkemailchecker') {
          
// use curl to make the request
            $url = $this->url.'/?key=' . $this->key . '&email=' . $email;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            $response = curl_exec($ch);
            curl_close($ch);
            $json = json_decode($response, true);            
            if(isset($json['error']) && $json['error'] == 'Invalid or missing api key provided in request.'){
                return true;
            }else{
                return false;
            }

        } else if ($this->checker == 'emaillistverify') {
            $key = $this->key;
            $url = 'https://apps.emaillistverify.com/api/getCredit?secret='.$key;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);
            if($response == 'key not valid'){
                return true;
            }else{
                return false;
            }
        }
    }
}
