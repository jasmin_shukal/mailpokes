<?php

namespace App\Http\Controllers\Template;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class htmlTemplate extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //loaded email list index page
        $pageName = trans('common.template');
        return view('template.htmlTemplate', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function buildTemplate($column)
    {
        $pageName = trans('common.template');
        return view('template.build', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request['user_id'] = Auth::id();
        Template::create($request->all());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {
        $templates = Template::where('user_id', Auth::id())->get();
        $pageName = trans('common.edit') . ' ' . trans('common.template');
        $title = trans('common.edit') . ' ' . trans('common.template');
//        $messageLists = MessageList::whereIn('user_id', [0, Auth::id()])->lists('name', 'id');
        return view('template.home', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Template $template, Request $request)
    {
        $template->update($request->all());
        Session::flash('success_msg', 'Successfully Updated');
        return redirect('template');
    }

}
