<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;

use App\Http\Controllers\Template\TemplateController;
use App\Models\Package\Package;
use App\Models\PurchaseHistory\PurchaseHistory;
use App\User;
use App\Models\Activity\Activity;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sesion;
use App\Models\CustomerGroup\CustomerGroup;
use Session;
use Auth;
use App\Http\Requests\Customers\CustomersRequest;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Users\UsersRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Department\Department;
use App\Models\Attachment\Attachment;
use App\Models\Blacklist\Blacklist;
use App\Models\EmailHistory\EmailHistory;
use App\Models\Group\Group;
use App\Models\EmailList\EmailList;
use App\Models\EmailSentList\EmailSentList;
use App\Models\Email\Email;
use App\Models\Media\Media;
use App\Models\Template\Template;
use App\Models\Ticket\Ticket;
use App\Models\TicketComment\TicketComment;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session('tab')) {
            $tab = session('tab');
        }
        $allUsers = User::where('role', 'customer')->get();
        $groups = CustomerGroup::pluck('name', 'id');
        $packages = Package::pluck('name', 'id');
        $customers = User::where('role', 'customer')->get();
        $add_edit_title = trans('common.add') . ' ' . trans('common.customer');
        return view('customers.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function show(User $customer)
    {
        $total_email = PurchaseHistory::where('user_id', $customer->id)->sum('package_limit');
        $sending_email = $total_email - $customer->email_limit;

        $sending_per = 0;
        $remaining_per = 0;
        if($total_email > 0) {
            $sending_per = $sending_email * 100 / $total_email;
            $remaining_per = $customer->email_limit * 100 / $total_email;
        }

        $users = User::where('customer_id', $customer->id)->get();
        $purchaseHistories = PurchaseHistory::where('user_id', $customer->id)->get();

        return view('customers.details', get_defined_vars());
    }

    public function store(CustomersRequest $request, User $customer)
    {
        // get all request data from

        $package = Package::find($request->package_id);
        $allRequest = $request->all();
        $allRequest['password'] = Hash::make($request->password);
        $allRequest['status'] = 'Active';
        $allRequest['api_key'] = ApiController::generate_api_key();
        $allRequest['package_id'] = $package->id;
        $allRequest['role'] = 'customer';
        $allRequest['email_limit'] = $package->limit;
        $allRequest['user_limit'] = $package->user_limit;
        $allRequest['package_activated_at'] = time();
        // $allRequest['group_id'] = 0;
        $allRequest['customer_id'] = 0;
        $allRequest['email_settings_id'] = 0;
        $customer = $customer->create($allRequest);

        $purchaseData = [
            'user_id' => $customer->id,
            'package_name' => $package->name,
            'package_validity' => $package->validity,
            'package_price' => $package->price,
            'package_limit' => $package->limit,
            'package_user_limit' => $package->user_limit
        ];

        PurchaseHistory::create($purchaseData);

        $user_id = Auth::id();
        $message = 'Created customer ' . $customer->email;
        //save into activity
        Activity::saveActivity($user_id, $message);
        TemplateController::templateForAll($customer->id);
        Session::flash('success_msg', 'Successfully created a Customer.');

        return redirect('customers');
    }

    public function edit(User $customer)
    {

        $groups = CustomerGroup::pluck('name', 'id');
        $packages = Package::pluck('name', 'id');
        $customers = User::where('role', 'customer')->get();
        $add_edit_title = trans('common.edit') . ' ' . trans('common.customer');
        //get all user
        $allUsers = User::where('role', 'customer')->get();
        $userInfo = $customer;

        return view('customers.home', get_defined_vars());
    }

    public function update(CustomersRequest $request, User $customer)
    {
        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        
        //updated user info
        // $data['name'] = $request->name;
        // if ($request->email != $customer->email) {
        //     $data['email'] = $request->email;
        // }

        if ($request->package_id != $customer->package_id) {
            $package = Package::find($request->package_id);
            $data['package_id'] = $package->id;
            $data['email_limit'] = $customer->email_limit + $package->limit;
            $data['user_limit'] = $customer->user_limit + $package->user_limit;
            $data['package_activated_at'] = time();

            $purchaseData = [
                'user_id' => $customer->id,
                'package_name' => $package->name,
                'package_validity' => $package->validity,
                'package_price' => $package->price,
                'package_limit' => $package->limit,
                'package_user_limit' => $package->user_limit
            ];

            PurchaseHistory::create($purchaseData);
        }


        $customer->update($data);

        $user_id = Auth::id();
        $message = 'Updated customer ' . $customer->email;
        Activity::saveActivity($user_id, $message);

        Session::flash('success_msg', 'Successfully Updated Customer Information.');
        return redirect('customers');
    }

    public function destroy(User $customer)
    {
        //deleted user
        $user_id = Auth::id();
        $message = 'Deleted customer ' . $customer->email;
        Activity::saveActivity($customer->id, $message);

        // deleted all activity according to this user
        Activity::where('user_id', $customer->id)->delete();

        //deleted all attachement according to this user
        Attachment::where('user_id', $customer->id)->delete();

        //deleted all blacklist according to this user
        Blacklist::where('user_id', $customer->id)->delete();

        $email_history = EmailHistory::where('sender_id', $customer->id)->pluck('id');

        EmailSentList::whereIn('email_history_id', $email_history)->delete();

        //deleted all EmailHistory according to this user
        EmailHistory::where('sender_id', $customer->id)->delete();

        $groups = Group::where('user_id', $customer->id)->pluck('id');

        EmailList::whereIn('group_id', $groups)->delete();

        Group::where('user_id', $customer->id)->delete();

        Email::where('user_id', $customer->id)->delete();

        Media::where('user_id', $customer->id)->delete();

        Template::where('user_id', $customer->id)->delete();

        $tickets = Ticket::where('created_by', $customer->id)->pluck('id');

        TicketComment::whereIn('ticket_id', $tickets)->delete();

        Ticket::where('created_by', $customer->id)->delete();

        PurchaseHistory::where('user_id', $customer->id)->delete();

        $customer->delete();

        Session::flash('success_msg', 'Successfully Deleted Customer.');
        return redirect('customers');
    }

    public function change_status($user_id)
    {
        // get user by user_id
        $user = User::find($user_id);

        if ($user->status == 'Active') {// check user status (Active, Banned) and set
            $user->status = 'Banned';
        } else if ($user->status == 'Banned') {
            $user->status = 'Active';
        }
        $user->save();
        $message = $user->status . ' customer ' . $user->email;
        Activity::saveActivity(Auth::id(), $message);
        return redirect('customers');
    }

    public function load_customer_users($customer_id)
    {
        $users = User::where('customer_id', $customer_id)->get();
        return response()->json($users);
    }

}
