<?php

namespace App\Http\Controllers\Stripe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PurchaseHistory\PurchaseHistory;
use App\Models\Package\Package;
use App\Models\Coupon\Coupon;
use Session;
use Auth;
use Log;

class StripeController extends Controller
{
    static public function stripe($package_id, $card_info, $coupon_id = null)
    {
        
        $package = Package::find($package_id);
        $price = $package->price;

        if(isset($coupon_id)) {
        	$coupon = Coupon::find($coupon_id);
        	if($coupon->amount_type == 'fixed_amount') {
		        $price = $price - $coupon->discount_amount;
        	} else if($coupon->amount_type == 'percentage_amount') {
		    	$price = $price - ($price * $coupon->discount_amount / 100); 
		    }
        }

        $stripeSecret = configValue('stripe_secret');
        if (!$stripeSecret) {
            Session::flash('error_msg', 'Stripe service is not available right now.');
            return redirect()->back();
        }
        
        
        try {
        	\Stripe\Stripe::setApiKey($stripeSecret);
        	// $card_info = array('number' => '4242424242424242', 'exp_month' => 8, 'exp_year' => 2018);
        	$charge = \Stripe\Charge::create(array('source' => $card_info['stripeToken'], 'amount' => ($price * 100), 'currency' => 'inr','description'=>'test','shipping' => [
             'name' => Auth::user()->name,
             'address' => [
              'line1' => 'test',
              'postal_code' => '',
              'city' => '',
              'state' => '',
              'country' => '',
                        ],
            ]));
           
		} catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  	$body = $e->getJsonBody();
		  	$err  = $body['error'];
		  	Session::flash('error_msg', $err['message']);
        	return redirect()->back();
		}
        
        if($charge->status == 'succeeded' && $charge->amount == $price * 100) {
        	$user = Auth::user();
            $user->package_id = $package->id;
            $user->email_limit = $user->email_limit + $package->limit;
            $user->user_limit = $user->user_limit + $package->user_limit;
            $user->package_activated_at = time();
            $user->save();

            $purchaseData = [
                'user_id' => Auth::id(),
                'package_name' => $package->name,
                'package_validity' => $package->validity,
                'package_price' => $price,
                'package_limit' => $package->limit
            ];

            PurchaseHistory::create($purchaseData);

            Session::flash('success_msg', "Successfully completed the Payment. Now you are in $package->name package.");
            Session::flash('free_package_flag','1');
        	return redirect('/email');
        } else {
        	Session::flash('error_msg', 'Something happened wrong');
        	return redirect()->back();
        }

    }
}
