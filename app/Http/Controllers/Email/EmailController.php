<?php

namespace App\Http\Controllers\Email;

use Illuminate\Http\Request;

use App\User;
use Auth;
use File;
use Session;
use App\Http\Requests;
use App\Models\Email\Email;
use App\Models\Email\MailProvider;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmailSettings\EmailSettingsRequest;
use App\Http\Controllers\ExternalApi\ElasticMail;
use App\Http\Controllers\ExternalApi\SendGridMail;
use Log;
use Mail;

class EmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadminORcustomer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //loaded email settings index page
        $user = Auth::user();
        if(isset($user->email_settings_id)) {
            $default_mail_provider = $user->email_settings_id;
        }

        $mail_providers = MailProvider::pluck('name', 'id');
        $pageName = trans('common.email') . ' ' . trans('common.settings');
        $smtpLists = Email::where('user_id', Auth::id())->get();
        $title = trans('common.new').' '.trans('common.mail_provider');
        return view('email_settings.home', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //saved and updated email settings info
    public function store(Request $request)
    {
        $this->validate($request, [
                            'mail_provider' => 'required|numeric'
                        ]);

        $mail_provider = MailProvider::find($request->mail_provider);

        if(! $mail_provider) {
            return response()->json(['error' => true, 'msg' => 'Something happened wrong.']);
        }

        $rules = [
            'from_email' => 'required|email',
            'name'       => 'required',
            'reply_to'       => 'nullable|email',
        ];

        if(isset($mail_provider->credentials) && ($mail_provider->credentials != '')) {
            foreach (json_decode($mail_provider->credentials) as $credential) {
                if($credential != 'encryption') {
                    $rules[$credential] = 'required';
                }
            }
        }

        $this->validate($request, $rules);

        $allRequest = $request->all();
        $allRequest['user_id'] = Auth::id();
        $allRequest['mail_provider_id'] = $mail_provider->id;

        
        $testing_mail_response = $this->test_mail_send((object) $allRequest, Auth::user()->email, $request);

        Log::error($testing_mail_response);
        
        $response_data = $testing_mail_response->getData();

        if($response_data->error) {
            return response()->json(['error' => true, 'msg' => $response_data->msg]);
        }
        
        $email_settings = Email::create($allRequest);

        $email_settings_count = Email::where('user_id', Auth::id())->count();

        if($email_settings_count == 1) {
            $user = Auth::user();
            $user->email_settings_id = $email_settings->id;
            $user->save();
        }

        return response()->json(['error' => false, 'msg' => 'Successfully added SMTP credentials.']);

    }

    public function edit(Email $email){
        $mail_provider = MailProvider::find($email->mail_provider_id);
        $credentials = [];
        if(isset($mail_provider->credentials) && ($mail_provider->credentials != '')) {
            $credentials = json_decode($mail_provider->credentials);
        }

        return response()->json(['email_settings' => $email, 'mail_provider' => $credentials]);
    }

    public function update(Request $request, Email $email) {
        $this->validate($request, [
                            'mail_provider' => 'required|numeric'
                        ]);

        $mail_provider = MailProvider::find($request->mail_provider);

        if(! $mail_provider) {
            return response()->json(['error' => true, 'msg' => 'Something happened wrong.']);
        }

        $rules = [
            'from_email' => 'required|email',
            'name'       => 'required',
            'reply_to'       => 'nullable|email',
        ];

        if(isset($mail_provider->credentials) && ($mail_provider->credentials != '')) {
            foreach (json_decode($mail_provider->credentials) as $credential) {
                if($credential != 'encryption') {
                    $rules[$credential] = 'required';
                }
            }
        }

        $this->validate($request, $rules);

        $allRequest = $request->all();
        $allRequest['user_id'] = Auth::id();
        $allRequest['mail_provider_id'] = $mail_provider->id;

        $testing_mail_response = $this->test_mail_send((object) $allRequest, Auth::user()->email, $request);

        Log::error($testing_mail_response);

        $response_data = $testing_mail_response->getData();

        if($response_data->error) {
            return response()->json(['error' => true, 'msg' => $response_data->msg]);
            
        }

        $email->update($allRequest);
        return response()->json(['error' => false, 'msg' => 'Successfully updated mail provider credentials.']);

    }

    public function destroy(Email $email){
        
        if($email->user_id == Auth::id()){
            if(isset(Auth::user()->email_settings_id) && (Auth::user()->email_settings_id == $email->id)) {
                $newEmail = Email::where('id', '!=', $email->id)->orderBy('created_at', 'asc')->first();
                
                $user = Auth::user();
                if($newEmail) {
                    $user->email_settings_id = $newEmail->id;
                } else {
                    $user->email_settings_id = 0;
                }
                $user->save();
            }
        }
        $email->delete();

        Session::flash('success_msg', 'Successfully deleted SMTP credentials.');
        return redirect('email');
    }

    public function email_settings(Request $request) {
        $this->validate($request, [
            'sender_email' => 'email',
        ]);

        $allRequest = $request->all();
        $allRequest['from_email'] = $request->sender_email;

        $user = User::find(Auth::id());
        $user->update($allRequest);

        Session::flash('success_msg', 'Successfully saved php mail information.');
        return redirect('email');
    }

    public function default_change($email_id) {
        $email = Email::find($email_id);
        if($email){
            if($email->user_id == Auth::id()) {
                $user = Auth::user();
                $user->email_settings_id = $email->id;
                $user->save();
                Session::flash('success_msg', 'Successfully changed default enable Mail Provider.');
            } else {
                Session::flash('error_msg', 'You have no access for this mail provider.');
            }
            
        } else {
            Session::flash('error_msg', 'Not found Mail Provider.');
        }
        return redirect('email');
    }

    public function load_mail_provider(MailProvider $mail_provider) {
        if($mail_provider) {
            if(isset($mail_provider->credentials) && ($mail_provider->credentials != '')) {
                return response()->json(json_decode($mail_provider->credentials));
            }
        }

        return response()->json([]);
    }

    public function email_testing(Request $request) {
        $this->validate($request, [
                'email_address' => 'required|email'
            ]);

        $email_info = Email::find($request->email_settings_id);
        
        $email = $request->email_address;

        return $this->test_mail_send($email_info, $email, $request);        
        
    }

    private function test_mail_send($email_info, $email, $request) {

        $mail_provider = MailProvider::find($email_info->mail_provider_id);
        
        $subject = 'Test mail';
        
        config(['queue.default' => 'sync']);

        $template = "Mail Successfully sent.";

        // config(['mail.driver' => 'log']);

        if($mail_provider->provider == 'elastic') {
            Log::error("Elastic  Mail...");
            $elasticMail = new ElasticMail($email_info, $mail_provider, $template, null, $email, $subject, [], url(), $request->ajax());
            return $elasticMail->shoot();
        } else if($mail_provider->provider == 'sendgrid') {
            Log::error("SendGride  Mail...");
            $sendGridMail = new SendGridMail($email_info, $mail_provider, $template, null, $email, $subject, [], url(), $request->ajax());
            return $sendGridMail->shoot();
        } else {

            config(['mail.driver' => $mail_provider->provider]); // detect mail driver
            config(['mail.from.address' => $email_info->from_email]);
            config(['mail.from.name' => $email_info->name]);

            $name = $email_info->name;
            $replyToAddress = '';
            if(isset($email_info->reply_to) && ($email_info->reply_to != '')) {
                $replyToAddress = $email_info->reply_to;
            }

            if ($mail_provider->provider == 'smtp') {
                config(['mail.host' => $email_info->smtp_host]);
                config(['mail.username' =>  $email_info->smtp_username]);
                config(['mail.password' => $email_info->smtp_password]);
                config(['mail.port' => $email_info->smtp_port]);
                
                if(isset($email_info->encryption) && ($email_info->encryption != '')) {
                    config(['mail.encryption' => $email_info->encryption]);
                }

            } else {
                if(isset($mail_provider->credentials) && ($mail_provider->credentials != '')) {
                    foreach (json_decode($mail_provider->credentials) as $credential) {
                        config(['services.'.$mail_provider->provider.'.'.$credential => $email_info->{$credential}]);
                    }
                    Log::error(config('services.'.$mail_provider->provider));
                }
            }
            Log::error(config('mail'));
            Log::error(config('queue'));

            try {
                Mail::send('emails.mail_provider_testing', ['template' => $template], function ($message) use ($email, $subject, $name, $replyToAddress) {

                    $message->to($email)->subject($subject);
                    if($replyToAddress) {
                        $message->replyTo($replyToAddress, $name);
                    }

                });
                return response()->json(['error' => false, 'msg' => "Mail provider credentials is vaild"]);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                return response()->json(['error' => true, 'msg' => 'Invalid credentials. ' .$e->getMessage()]);
            }
            
        }
    }

}
