<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Schema, Log, DB;

class DynamicDatatableController extends Controller
{
    public static function datatables(Request $request){
        // Log::info($request->all());
        $model_name = $request->input('model_name');
        $table_name = $request->input('table_name');
        $group_id = $request->input('group_id');

        //normal data pulling parameters
        $compare_field_name = $request->input('compare_field_name');
        $compare_with = $request->input('compare_with');

        //search field parameters
        $search_fields = $request->input('search_field_name');

        //taking columns from given table
        $columns = Schema::getColumnListing($table_name);
        
        $comparison = $request->input('compare_info');

        $globalTables = DB::table('email_list')->select('email_list.*');
        
        if(!is_null($comparison) ){
            $begin = 0;
            foreach($comparison as $comp){
                if($begin == 0){
                   
                    $totalData = $globalTables->where($table_name.'.'.$comp['field'], $comp['operators'], $comp['compare_with']);
                    $begin++;
                }else{
                    $totalData = $totalData->orWhere($table_name.'.'.$comp['field'], $comp['operators'], $comp['compare_with']);
                    $begin++;
                }
            }
            
            $data_tables = $totalData;
            $totalData = $totalData->count();
            $totalFiltered = $totalData;
        } else{
            $totalData = $model_name::count();
            $totalFiltered = $totalData;
        }
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir   = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $data_tables = $data_tables->offset($start)
                                        ->limit($limit)
                                        ->orderBy($order, $dir)
                                        ->get();
        } else {
            $search = $request->input('search.value');
            
            if(empty($search_fields)){
                $totalData = $globalTables->where('name', 'LIKE', "%{$search}%")
                                        ->orWhere('email', 'LIKE', "%{$search}%")
                                        ->where('group_id', $group_id)
                                        ->offset($start)
                                        ->limit($limit)
                                        ->get();
                $totalFiltered = $globalTables->where('name', 'LIKE', "%{$search}%")
                                        ->orWhere('email', 'LIKE', "%{$search}%")
                                        ->where('group_id', $group_id)
                                        ->count();
            } else{
                $counter = 0;
                foreach($search_fields as $search_field){
                    if($counter == 0){
                        $data_tables = $globalTables->where($table_name.'.'.$search_field, 'LIKE', "%{$search}%")->where('group_id', $group_id);
                    }else{
                        $data_tables = $data_tables->orWhere($table_name.'.'.$search_field, 'LIKE', "%{$search}%")->where('group_id', $group_id);
                    }
                    $counter++;
                }
                $data_tables = $data_tables->offset($start)
                                           ->limit($limit)
                                           ->get();
                $totalFiltered = count($data_tables);
            }
            
        }

        $i = $start ;
        foreach($data_tables as $data_table){
          
            if($data_table->subscribed == 1){
                $subs = 'trans("common.subscribed")';
            }else{
                $subs = 'trans("common.unsubscribed")';
            }
           $data_table->serial_no = $i +1;
           $data_table->name = '<a class="field_editable" href="#" data-name="name" rel="'. csrf_token() .'" data-type="text" data-pk="'. $data_table->id .'" data-url="'. url('/update_email_info') .'" data-title="Enter Name">'.$data_table->name.'</a>';
           $data_table->email = '
                                <a class="field_editable" href="#" data-name="email" rel="'. csrf_token() .'"
                                data-type="text" data-pk="'.$data_table->id.'"
                                data-url="'. url('/update_email_info') .'"
                                data-title="Enter Email">'.$data_table->email .'
                                </a>
                                <span class="badge badge-info" data-toggle="tooltip" data-placement="top"
                                title="'.$subs.'">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </span>
            ';
           if($data_table->free_email_check == 1){
               $data_table->free_email_check = '<a href="#" data-toggle="modal" class="label label-success" data-target="#free_email_detail_'. $data_table->id .'" data-toggle='.'tooltip'.' data-placement='.'top'.' title='.'@lang('.'common.detail'.')'.'> <i class="fa fa-check"></i> </a>';
           } else if($data_table->free_email_check == 2){
               $data_table->free_email_check = ' <a href="#" data-toggle="modal" class="label label-danger" data-target="#free_email_detail_'. $data_table->id .'" data-toggle='.'tooltip'.' data-placement='.'top'.' title='.'@lang('.'common.detail'.')'.'> <i class="fa fa-times"></i></a>';
           } else {
               $data_table->free_email_check = '';
           }
           if($data_table->bulk_check == 1){
            $data_table->bulk_check = '<a href="#" data-toggle="modal" class="label label-success" data-target="#bulk_detail_'. $data_table->id .'" data-toggle='.'tooltip'.' data-placement='.'top'.' title='.'@lang('.'common.detail'.')'.'> <i class="fa fa-check"></i> </a>';
            } else if($data_table->bulk_check == 2){
                $data_table->bulk_check = ' <a href="#" data-toggle="modal" class="label label-danger" data-target="#bulk_detail_'. $data_table->id .'" data-toggle='.'tooltip'.' data-placement='.'top'.' title='.'@lang('.'common.detail'.')'.'> <i class="fa fa-times"></i></a>';
            } else {
                $data_table->bulk_check = '';
            }
           if($data_table->email_list_check == 1){
            $data_table->email_list_check = '<a href="#" data-toggle="modal" class="label label-success" data-target="#email_list_detail_'. $data_table->id .'" data-toggle='.'tooltip'.' data-placement='.'top'.' title='.'@lang('.'common.detail'.')'.'> <i class="fa fa-check"></i> </a>';
            } else if($data_table->email_list_check == 2){
                $data_table->email_list_check = ' <a href="#" data-toggle="modal" class="label label-danger" data-target="#email_list_detail_'. $data_table->id .'" data-toggle='.'tooltip'.' data-placement='.'top'.' title='.'@lang('.'common.detail'.')'.'> <i class="fa fa-times"></i></a>';
            } else {
                $data_table->email_list_check = '';
            }
            $data_table->action = '<a href="'. url('email-delete')."/".$data_table->id .'"'.' class="btn btn-danger btn-xs delete-swl" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i></a>';
            $i++;
        }
       
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data_tables
        );
        echo json_encode($json_data);
    }

    public static function datatablesHistory(Request $request){
        // Log::info($request->all());
        $model_name = $request->input('model_name');
        $table_name = $request->input('table_name');

        //normal data pulling parameters
        $compare_field_name = $request->input('compare_field_name');
        $compare_with = $request->input('compare_with');

        //search field parameters
        $search_fields = $request->input('search_field_name');

        //taking columns from given table
        $columns = Schema::getColumnListing($table_name);
        
        $comparison = $request->input('compare_info');

        $globalTables = DB::table($table_name)->select($table_name.'.*');
        
        if(!is_null($comparison) ){
            $begin = 0;
            foreach($comparison as $comp){
                if($begin == 0){
                   
                    $totalData = $globalTables->where($table_name.'.'.$comp['field'], $comp['operators'], $comp['compare_with']);
                    $begin++;
                }else{
                    $totalData = $totalData->orWhere($table_name.'.'.$comp['field'], $comp['operators'], $comp['compare_with']);
                    $begin++;
                }
            }

            $data_tables = $totalData;
            $totalData = $totalData->count();
            $totalFiltered = $totalData;
        } else{
            $totalData = $model_name::count();
            $totalFiltered = $totalData;
        }
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir   = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $data_tables = $data_tables->offset($start)
                                        ->limit($limit)
                                        ->orderBy($order, $dir)
                                        ->get();
        } else {
            $search = $request->input('search.value');
            $mainString = $search;
            if($search == 'seen' || $search == 'Seen' || $search == 'Clicked' || $search == 'clicked'){
                $search = 1;
            }else if($search == 'unseen' || $search == 'Unseen' || $search == 'not clicked' || $search == 'Not Clicked'){
                $search = 0;
            }
            
            if(empty($search_fields)){
                $totalData = $globalTables->where('name', 'LIKE', "%{$search}%")
                                        ->orWhere('email', 'LIKE', "%{$search}%")
                                        // ->where('email_history_id', $group_id)
                                        ->offset($start)
                                        ->limit($limit)
                                        ->get();
                $totalFiltered = $globalTables->where('name', 'LIKE', "%{$search}%")
                                        ->orWhere('email', 'LIKE', "%{$search}%")
                                        // ->where('email_history_id', $group_id)
                                        ->count();
            } else{
                if(is_numeric($search)){
                    if($mainString == 'seen' || $mainString == 'Seen' || $mainString == 'unseen' || $mainString == 'Unseen'){
                        $data_tables = $globalTables->where($table_name.'.'.'seen_status', $search);
                    }else if($mainString == 'clicked' || $mainString == 'Clicked' || $mainString == 'not clicked' || $mainString == 'Not Clicked'){
                        $data_tables = $globalTables->where($table_name.'.'.'link_open', $search);
                    }
                }else{
                    $data_tables = $globalTables->where($table_name.'.'.'email', 'LIKE', "%{$search}%");
                }
                $data_tables = $data_tables->offset($start)
                                           ->limit($limit)
                                           ->get();
                $totalFiltered = count($data_tables);
            }
            
        }

        foreach($data_tables as $data_table){
            if($data_table->seen_status == 0){
                $data_table->seen_status = '<span class="label label-danger">Unseen</span>';
            }else{
                $data_table->seen_status = '<span class="label label-success">Seen</span>';
            }

            if( $data_table->link_open == 0){
                $data_table->link_open = '<span class="label label-danger">Not Clicked</span>';
            }else{
                $data_table->link_open = '<span class="label label-primary">Clicked</span>';
            }
        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data_tables
        );
        echo json_encode($json_data);
    }
}
