<?php

namespace App\Http\Controllers\Coupons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package\Package;
use App\Models\Coupon\Coupon;
use Session;

class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $packages = Package::pluck('name', 'id');
        $coupons = Coupon::all();
        $amount_types = [
            'percentage_amount' => trans('common.percentage_amount'),
            'fixed_amount' => trans('common.fixed_amount'),
        ];
        $add_edit_coupon = trans('common.new'). ' '. trans('common.coupon');
        return view('Settings.coupons.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Coupon $coupon)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required|unique:coupons',
            'package_id' => 'required',
            'discount_amount' => 'required'
        ]);

        $request['package_id'] = json_encode($request->package_id);
        $coupon->create($request->all());
        Session::flash('success_msg', trans('common.successfully_added_new_coupon'));
        return redirect('coupons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        $couponInfo = $coupon;
        $packages = Package::pluck('name', 'id');
        $coupons = Coupon::all();
        $amount_types = [
            'percentage_amount' => trans('common.percentage_amount'),
            'fixed_amount' => trans('common.fixed_amount'),
        ];
        $add_edit_coupon = trans('common.edit'). ' '. trans('common.coupon');
        return view('Settings.coupons.home', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required|unique:coupons',
            'package_id' => 'required',
            'discount_amount' => 'required'
        ]);

        $request['package_id'] = json_encode($request->package_id);
        $coupon->update($request->all());
        Session::flash('success_msg', trans('common.successfully_updated_coupon'));
        return redirect('coupons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        Session::flash('success_msg', trans('common.successfully_deleted_coupon'));
        return redirect('coupons');
    }

    public function change_published(Coupon $coupon ) {
        if($coupon->published == 'yes') {
            $coupon->update(['published' => 'no']);
        } else {
            $coupon->update(['published' => 'yes']);
        } 

        Session::flash('success_msg', trans('common.successfully_changed_published_status'));
        return redirect('coupons');

    }
}
