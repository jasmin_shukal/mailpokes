<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Config\Config;
use Log;
use File;
use Session;

class CronController extends Controller
{
    public function __construct()
    {
        // $this->middleware('superadmin');
    }

    public function cronTest()
    {
        $data['value'] = 1;
        Config::where('key', 'cron')->update($data);
        Log::error('cron called');
    }

    public function index()
    {
        // load corn
        $cron = Config::where('key', 'cron')->first();
        $queueDriver = env('QUEUE_DRIVER');
        return view('cron.home', get_defined_vars());
    }

    public function store(Request $request)
    {
        if( !is_writable(base_path('.env') ) ){
            Session::flash('error_msg', base_path('.env') . ' this file is not writable. Please give write permission(0777).' );
           return \Redirect::back();
        }
        
        $queueDriver = Config::where('key', 'QUEUE_DRIVER')->first();
        if ($queueDriver) {
            $data['key'] = 'QUEUE_DRIVER';
            $data['value'] = $request->QUEUE_DRIVER;
            Config::where('key', 'QUEUE_DRIVER')->update($data);
        }
        $previousQueueDriver = env('QUEUE_DRIVER');
        $env =  File::get(base_path('.env'));

        file_put_contents(base_path('.env'), str_replace('QUEUE_DRIVER=' . $previousQueueDriver, 'QUEUE_DRIVER=' . $request->input('QUEUE_DRIVER'), $env));
        return \Redirect::back();
    }

}
