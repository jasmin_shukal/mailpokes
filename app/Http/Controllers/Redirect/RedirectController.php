<?php

namespace App\Http\Controllers\Redirect;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Models\Config\Config;
use Illuminate\Support\Facades\Session;
use Validator;
use Auth;

class RedirectController extends Controller
{
    // redirect url
    public function index()
    {
        $redirect_url = configValue('redirect_url');
        return view('redirect.home', get_defined_vars());
    }

     // set redirect url
    public function store(Request $request )
    {
    	$validator = Validator::make($request->all(), [
        'redirect_url' => 'active-url',
	    ]);
  		
  		if ($validator->fails()) {
            Session::flash('error_msg', 'This has be to an active URL.');
            return redirect('redirect')
                ->withErrors($validator)
                ->withInput();
        }

        $data['value'] = $request->redirect_url;
        Config::where('key', 'redirect_url')->update($data);
        return redirect('redirect');
    }
}
