<?php

namespace App\Http\Controllers\EmailList;

use App\Jobs\CheckValidEmail;
use App\Models\Api\EmailCheckerApi;
use App\Http\Controllers\Checker\EmailChecker;
use App\Http\Controllers\Checker\VerifyEmail;
use Illuminate\Http\Request;
use Auth;
use App\Models\Group\Group;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EmailList\EmailList;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\EmailListRequest;
use Excel;
use Log, Mail;
use App\Jobs\ImportQueueJob;

class EmailListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //loaded email list index page
        $pageName = trans('common.email-list');

        $groupNames = Group::where('user_id', Auth::id())->pluck('name', 'id');
        $groups = Group::where('user_id', Auth::id())->get();

        $title = trans('common.create') . ' ' . trans('common.email-list');

        return view('emailList.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(EmailListRequest $request, EmailList $model)
    {
        set_time_limit(0);
        ini_set('memory_limit', '4028M');

        if (!$request->has('emails') && !$request->hasFile('excel_file')) {
            Session::flash('error_msg', 'There is no any email. Please enter email address.');
            return redirect('email-list');
        }

        if ($request->hasFile('excel_file')) {
            $file = $request->file('excel_file');
            $ext = strtolower($file->getClientOriginalExtension());

            $allRequest = $request->all();
            $allRequest['ext'] = $ext;

            list($messages, $rules) = csvValidationMsgRules();

            // $messages = [
            //     'in' => 'The Upload file must be a file of type: csv.',
            // ];

            // $rules = [
            //     'ext' => 'in:csv'
            // ];

            $validator = Validator::make($allRequest, $rules, $messages);

            if ($validator->fails()) {
                return redirect('email-list')->withErrors($validator)->withInput();
            }
        }

        $queue = '';

//        $queue = env('QUEUE_DRIVER', 'sync');
        //create group name

        $group_id_list = [];

        if ($request->has('select_group_name')) {
            $group_id_list = $request->input('select_group_name');
            foreach ($group_id_list as &$group_id_or_new) {
                $group = Group::find($group_id_or_new);
                if (!$group) {
                    $group = Group::create(['name' => $group_id_or_new, 'status' => 1, 'user_id' => Auth::id(), 'free_email_check' => 0, 'bulk_check' => 0, 'email_list_check' => 0]);
                    $group_id_or_new = $group->id;
                }
            }
        }

        if ($request->has('emails')) {
            if ($queue != 'sync') {
                $jobs = (new ImportQueueJob('emails', $request->emails, $group_id_list))->delay(1);
                $this->dispatch($jobs);
            } else {
                $commaAndNew = str_replace("\r\n", ',', $request->emails);

                $string = array_unique(explode(",", $commaAndNew));
                if (array_search("", $string)) {
                    $position = array_search("", $string);
                    unset($string[$position]);
                }

                $validEmails = array_diff(validEmail($string), array(NULL));

                if (count($validEmails) > 0) {
                    foreach ($validEmails as $email) {
                        $data['email'] = $email;
                        $data['subscribed'] = 1; // email address treat as a subscriber
                        $data['free_email_check'] = 0; // email address treat as a subscriber
                        $data['bulk_check'] = 0; // email address treat as a subscriber
                        $data['email_list_check'] = 0; // email address treat as a subscriber
                        foreach ($group_id_list as $group_id) {
                            $data['group_id'] = $group_id;
                            EmailList::create($data);
                        }
                    }
                } else {
                    Session::flash('success_msg', 'There is no any valid email.');
                    return redirect('email-list');
                }
            }

        }


        if ($request->hasFile('excel_file')) {
            $extension = $request->file('excel_file')->getClientOriginalExtension();
            $fileName = microtime(true) . '.' . $extension;
            $destinationPath = storage_path('imports');
            $request->file('excel_file')->move($destinationPath, $fileName);

            if ($queue != 'sync') {
                $jobs = (new ImportQueueJob('excel_file', $fileName, $group_id_list))->delay(1);
                $this->dispatch($jobs);
            } else {
                // $results = Excel::load(storage_path('imports/' . $fileName), function ($reader) {

                // })->get(array('name', 'email'))->toArray();

                $results = csvFileRead(storage_path('imports/' . $fileName));

                $validEmails = validEmailForFile($results);

                if (count($validEmails) > 0) {
                    foreach ($validEmails as $emailInfo) {
                        $data['name'] = $emailInfo['name'];
                        $data['email'] = $emailInfo['email'];
                        $data['subscribed'] = 1; // email address treat as a subscriber
                        $data['free_email_check'] = 0; // email address treat as a subscriber
                        $data['bulk_check'] = 0; // email address treat as a subscriber
                        $data['email_list_check'] = 0; // email address treat as a subscriber
                        foreach ($group_id_list as $group_id) {
                            $data['group_id'] = $group_id;
                            EmailList::create($data);
                        }
                    }
                } else {
                    Session::flash('success_msg', 'There is no any valid email.');
                    return redirect('email-list');
                }
            }
        }

        if ($queue) {
            Session::flash('success_msg', 'Successfully added email list into queue');
        } else {
            Session::flash('success_msg', 'Successfully Added to queue. It might add after a minute');
        }
        return redirect('email-list');
    }

    public function show(Group $email_list)
    {
        $group = $email_list;
        if ($group->user_id != Auth::id()) {
            return redirect()->back();
        }
        return view('emailList.emaillist', get_defined_vars());
    }

    public function edit(Group $email_list)
    {
        $groupList = $email_list;
        $groupNames = Group::where('user_id', Auth::id())->pluck('name', 'id');
        $pageName = trans('common.email-list');
        $groups = Group::where('user_id', Auth::id())->get();
        $title = trans('common.edit') . ' ' . trans('common.email-list');

        return view('emailList.home', get_defined_vars());
    }


    public function update(EmailListRequest $request, Group $email_list)
    {
        $email_list->update($request->all());

        Session::flash('success_msg', 'Successfully Updated Group Name!');
        return redirect('email-list');
    }


    public function destroy(Group $email_list)
    {
        EmailList::where('group_id', $email_list->id)->delete();
        $email_list->delete();


        Session::flash('success_msg', 'Successfully Deleted');
        return redirect('email-list');
    }

    public function update_email_info(Request $request)
    {
        $emailList_id = $request->input('pk');
        $value = $request->input('value');
        $field = $request->input('name');
        $emailList = EmailList::find($emailList_id);

        if ($field == 'email') {
            $a = trim($value);
            if (!filter_var($a, FILTER_VALIDATE_EMAIL)) {
                return response()->json(['type' => 'Warning', 'msg' => 'Invalid email, Please enter valid email.']);
            }

            $rules = [
                'email' => 'required|max:255|unique:email_list,email,$emailList->id,group_id',
            ];

            $validator = Validator::make(['email' => $value], $rules);
            if ($validator->fails()) {
                return response()->json(['type' => 'Warning', 'msg' => implode(', ', $validator->messages()->all())]);
            }
        }

        $emailList->update([$field => $value]);


        return response()->json(['type' => 'Congrats', 'msg' => 'Successfully updated email information.']);
    }

    public function email_delete($emailList_id)
    {
        EmailList::find($emailList_id)->delete();
        Session::flash('success_msg', 'Successfully Deleted');
        return redirect()->back();
    }

    public function save_email(Request $request)
    {
        if ($request->ajax()) {
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['subscribed'] = 1;
            $data['group_id'] = $request->group_id;
            $data['free_email_check'] = 0; // email address treat as a subscriber
            $data['bulk_check'] = 0; // email address treat as a subscriber
            $data['email_list_check'] = 0; // email address treat as a subscriber

            if ($data['email'] == '') {
                return response()->json(['type' => false, 'msg' => 'Email field is required.']);
            }

            $validEmails = array_diff(validEmail([$data['email']]), array(NULL));
            if (count($validEmails) > 0) {
                EmailList::create($data);
                return response()->json(['type' => true, 'msg' => 'Successfully saved email information.']);
            } else {
                return response()->json(['type' => false, 'msg' => 'Please enter valid email.']);

            }

        }
    }

    public function real_email($checker, $group_id)
    {
        if ($checker == 'bulkemailchecker') {
            $bulkEmailChecker = EmailCheckerApi::where('provider', 'bulkemailchecker')->first();
            if ($bulkEmailChecker && $bulkEmailChecker->key != '') {
                $para = $bulkEmailChecker->key;
            } else {
                Session::flash('error_msg', 'Please enter Bulk email chcker information');
                return redirect('api');
            }
        } elseif ($checker == 'emaillistverify') {
            $emailListVerify = EmailCheckerApi::where('provider', 'emaillistverify')->first();
            if ($emailListVerify && $emailListVerify->key != '') {
                $para = $emailListVerify->key;
            } else {
                Session::flash('error_msg', 'Please enter Email list verify information.');
                return redirect('api');
            }
        } elseif ($checker == 'freeemailverify') {
            $para = Auth::user()->email;
        } else {
            Session::flash('error_msg', 'Please select a email checker.');
            return redirect()->back();
        }

        $emails = EmailList::where('group_id', $group_id)->get();
        $url = url('email-list');
        
        $jobs = (new CheckValidEmail($checker, $para, $emails, $group_id, Auth::user(), $url))->delay(1);
        $this->dispatch($jobs);
        Session::flash('success_msg', 'Successfully added to Queue. Once finished you will get an email');
        return redirect('email-list');
    }

    public function delete_invalid_email($checker, $group_id)
    {
        $count_deleted_email = EmailList::where('group_id', $group_id)->where($checker, 2)->count();
        EmailList::where('group_id', $group_id)->where($checker, 2)->delete();

        Session::flash('success_msg', 'Successfully deleted ' . $count_deleted_email . ' email address(es).');
        return redirect()->back();
    }

    public function unsubscribe($email_id)
    {
        $email_id = Crypt::decrypt($email_id);
        $email = EmailList::find($email_id);
        if ($email) {

            $email->subscribed = 0;
            $email->save();

        }
        return view('emailList.unsubscribe', get_defined_vars());
    }

    public function save_group(Request $request, Group $group)
    {
        $this->validate($request, [
            'group_name' => 'required|required_without:existing_group_name|max:255|unique:group,name'
        ]);
        $group->create(['name' => $request->group_name, 'status' => 1, 'user_id' => Auth::id(), 'free_email_check' => 0, 'bulk_check' => 0, 'email_list_check' => 0]);

        Session::flash('success_msg', 'Successfully added a group');
        return redirect()->back();
    }

}