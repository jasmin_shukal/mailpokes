<?php

namespace App\Http\Controllers\EmailHistory;

use App\Models\PurchaseHistory\PurchaseHistory;
use Auth;
use Illuminate\Http\Request;

use App\Models\EmailSentList\EmailSentList;
use App\Models\EmailHistory\EmailHistory;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class EmailHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'superadmin'){
            $emailHistories = EmailHistory::with('emailsettings')->orderBy('id', 'DESC')->get();
        } else {
            $emailHistories = EmailHistory::orderBy('id', 'DESC')->where('sender_id', Auth::id())->get();
        }
       
        
        $title = trans('common.history');
        return view('emailHistory.home', get_defined_vars());
    }

    public function sent_email_list($email_history_id){
        $email_sent_lists = EmailSentList::where('email_history_id', $email_history_id)->get();
        $title = trans('common.sent_email_list');
        return view('emailHistory.home', get_defined_vars());
    }

    public function email_open($email_sent_list_id, $email){
        $id = base64_decode($email_sent_list_id);
        EmailSentList::where('email_history_id', $id)->where('email', $email)->update(['seen_status' => 1]);
        return ;
    }

    public function link_open($email_sent_list_id, $email, Request $request ){
        $id = base64_decode($email_sent_list_id);
        EmailSentList::where('email_history_id', $id)->where('email', $email)->update(['link_open' => 1]);
        $url = $request->input('url');
        return redirect($url);
    }

    public function purchase_history(){
        if((Auth::user()->role == 'superadmin') || (Auth::user()->role == 'customer')) {
            if (Auth::user()->role == 'superadmin') {
                $purchaseHistories = PurchaseHistory::all();
            } else {
                $purchaseHistories = PurchaseHistory::where('user_id', Auth::id())->get();
            }

            return view('purchaseHistory.home', get_defined_vars());
        } else {
            return redirect('/');
        }
    }

    public function clearHistory(){
        if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'customer') {
            EmailHistory::where('sender_id',Auth::id())->delete();
            Session::flash('success_msg', 'Deleted all History successfully');
            return redirect('history');
        } else {
            Session::flash('success_msg', 'Only Admin can clear history.');
            return redirect('history');
        }
    }
}
