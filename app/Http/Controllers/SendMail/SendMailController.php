<?php

namespace App\Http\Controllers\SendMail;

use App\Models\EmailSentList\EmailSentList;
use App\Models\Email\Email;
use App\Models\Email\MailProvider;
use App\Models\EmailLater\EmailLater;
use App\Models\EmailHistory\EmailHistory;
use App\Models\Group\Group;
use App\Models\EmailList\EmailList;
use App\Models\MessageList\MessageList;
use App\Models\Package\Package;
use App\Models\Template\Template;
use App\Models\Attachment\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use App\Models\Blacklist\Blacklist;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs\EmailQueueJob;
use Session;
use Auth;
use App\User;
use DateTime;
use DateTimeZone;
use Validator;
use Excel;
use App\Models\Config\Config;

class SendMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        //loaded send mail list index page
        $pageName = trans('common.send-mail');
        $groups1[] = 'All Groups';
        $groups2 = Group::where('user_id', Auth::id())->pluck('name', 'id')->toArray();
        // dd(Group::with('emaillist')->find(1));
        $groups = [];
        if(count($groups2) > 0){
            $groups = $groups1 + $groups2;
        }
        $templates = Template::where('user_id', Auth::id())->pluck('name', 'id');
        $template_lists = Template::where('user_id', Auth::id())->get();
        $attachments = Attachment::where('user_id', Auth::id())->pluck('title', 'id');

        $title = trans('common.send-mail');

        if(Auth::user()->role == 'user'){
            $admin_or_customer_id = Auth::user()->customer_id;
        } else {
            $admin_or_customer_id = Auth::id();
        }
        $user = User::find($admin_or_customer_id);
        // dd(isset($user->email_settings_id));
        if($user) {
            if(! isset($user->email_settings_id) || (isset($user->email_settings_id) && ($user->email_settings_id == 0))) {
                $email_settings_info = 'Please set mail provider first. Go Settings > Email Settings or Click here';
            }
        } else {
            $email_settings_info = 'Something happened wrong.';
        }
        
        $cron = Config::where('key', 'cron')->first();
        if($cron->value == 1 && env('QUEUE_DRIVER') != 'sync'){
            $send_later = true ; 
        }
        return view('sendMail.home', get_defined_vars());
    }

    public function group_detail($id)
    {
        if($id=="0")
        {
            $group=EmailList::all()->pluck('email', 'id');
        }
        else
        {
            $group=EmailList::where('group_id',$id)->pluck('email', 'id');
        }
        return $group;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->role == 'user'){
            $admin_or_customer_id = Auth::user()->customer_id;
        } else {
            $admin_or_customer_id = Auth::id();
        }
        $user = User::find($admin_or_customer_id);

        if($user) {
            if(! isset($user->email_settings_id) || (isset($user->email_settings_id) && ($user->email_settings_id == 0))) {
                Session::flash('error_msg', 'Please set mail provider first. Go Settings > Email Settings.');
                return redirect()->back();
            }

            $email_info = Email::find($user->email_settings_id);
            
            if(! $email_info) {
                Session::flash('error_msg', 'Mail Provider Credential not found.');
                return redirect()->back();
            }

            $mail_provider = MailProvider::find($email_info->mail_provider_id);

            if(! $mail_provider ) {
                Session::flash('error_msg', 'Mail Provider not found. Please set mail provider.');
                return redirect()->back();

            }

        } else {
            Session::flash('error_msg', 'User not found. Please try again.');
            return redirect()->back();
        }

        set_time_limit(36000);
        $time_start = time();

        $this->validate($request, [
            'select_group_name' => 'required',
            'template' => 'required',
            'subject' => 'required'
        ]);

        if ($request->hasFile('excel_file')) {
            $file = $request->file('excel_file');
            $ext = strtolower($file->getClientOriginalExtension());

            $allRequest = $request->all();
            $allRequest['ext'] = $ext;

            list($messages, $rules) = csvValidationMsgRules();

            // $messages = [
            //     'in' => 'The Upload file must be a file of type: xls, xlsx.',
            // ];

            // $rules = [
            //     'ext' => 'in:xls,xlsx'
            // ];

            $validator = Validator::make($allRequest, $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }

        $emailLists = [];
        $send_later = 0;

        $queue = env('QUEUE_DRIVER', 'sync');

        $group_id_list = $request->input('select_group_name');
        foreach ($group_id_list as &$group_id_or_new) {
            $group = Group::find($group_id_or_new);
            if(! $group && ($group_id_or_new != '0')){
                $group = Group::create(['name' => $group_id_or_new, 'status' => 1, 'user_id' => Auth::id()]);
                $group_id_or_new = $group->id;
                $commaAndNew = str_replace("\r\n", ',', $request->emails);

                $string = array_unique(explode(",", $commaAndNew));
                if (array_search("", $string)) {
                    $position = array_search("", $string);
                    unset($string[$position]);
                }

                $validEmails = array_diff(validEmail($string), array(NULL));
                if(count($validEmails) > 0){
                    foreach ($validEmails as $email) {
                        $data['group_id'] = $group->id;
                        $data['email'] = $email;
                        $data['subscribed'] = 1; // email address treat as a subscriber
                        $data['free_email_check'] = 0;
                        $data['bulk_check'] = 0;
                        $data['email_list_check'] = 0;
                        EmailList::create($data);
                    }
                }

                if ($request->hasFile('excel_file')) {
                    $extension = $request->file('excel_file')->getClientOriginalExtension();
                    $fileName = microtime(true) . '.' . $extension;
                    $destinationPath = storage_path('imports');
                    $request->file('excel_file')->move($destinationPath, $fileName);

//                    if ($queue == 'database') {
//                        $jobs = (new ImportQueueJob('excel_file', $fileName, $group_id_list))->delay(1);
//                        $this->dispatch($jobs);
//                    } else {
                        // $results = Excel::load(storage_path('imports/' . $fileName), function ($reader) {

                        // })->get(array('name', 'email'))->toArray();

                        $results = csvFileRead(storage_path('imports/' . $fileName));

                        $validEmails = validEmailForFile($results);

                        if(count($validEmails) > 0) {
                            foreach ($validEmails as $emailInfo) {
                                $data['name'] = $emailInfo['name'];
                                $data['email'] = $emailInfo['email'];
                                $data['subscribed'] = 1; // email address treat as a subscriber
                                $data['free_email_check'] = 0;
                                $data['bulk_check'] = 0;
                                $data['email_list_check'] = 0;
//                                foreach ($group_id_list as $group_id) {
                                    $data['group_id'] = $group->id;
                                    EmailList::create($data);
//                                }
                            }
                        }
//                    }
                }

            }
        }

        if(in_array('0', $group_id_list)){
            $group_id_list = Group::where('user_id', Auth::id())->pluck('id')->toArray(); //get user all group name
        }

        $emailLists = EmailList::whereIn('group_id', $group_id_list)->where('subscribed', 1)->pluck('email', 'id')->toArray();

        if(count($emailLists) == 0) {
            Session::flash('error_msg', 'There is no any email. Please enter email address.');
            return redirect()->back();
        }


        if(Auth::user()->role == 'user'){
            $admin_or_customer_id = Auth::user()->customer_id;
        } else {
            $admin_or_customer_id = Auth::id();
        }
        $user = User::find($admin_or_customer_id);

        $package = Package::find($user->package_id);
        if($package){
            if(time() > strtotime(date('d-m-Y', $user->package_activated_at)."+ $package->validity days")) {
                Session::flash('error_msg', 'Your validity is expired. Please upgrade your package');
                return redirect()->back();
            }

            if($user->email_limit < count($emailLists)) {
                Session::flash('error_msg', "Your email sending limit is $user->email_limit. Please upgrade your package");
                return redirect()->back();
            }
        } else {
            if(Auth::user()->role != 'superadmin') {
                Session::flash('error_msg', "Please purchase a package first.");
                return redirect()->back();
            }
        }

        if(Auth::id() == superadmin('id')) {
            $black_email_list = Blacklist::where('created_by', Auth::id())->where('for', 'email_blacklist')->pluck('name')->toArray();
        } else {
            $black_email_list = Blacklist::where('for', 'email_blacklist')
                        ->where( function($query) {
                            $query->where('created_by', Auth::id())
                                ->orWhere('user_id', 'like', '%-1%')
                                ->orWhere('user_id', 'like', '%"'. Auth::id() .'"%');
                        })
                        ->pluck('name')
                        ->toArray();
        }  

        // $black_email_list = Blacklist::where('for', 'email_blacklist')->pluck('name')->toArray();

        if(count($black_email_list) > 0) {
            $block_emails = array_intersect($emailLists, $black_email_list);
            
            if(count($block_emails) > 0){
                Session::flash('block_errors', implode(', ', $block_emails). ' block email(s) found to your sending email list');
                return redirect()->back();
            }

        }

        $emailTemplate = Template::find($request->template)->template;

        //block word check with template
        if(Auth::id() == superadmin('id')) {
            $word_blacklist = Blacklist::where('created_by', Auth::id())->where('for', 'word_blacklist')->pluck('name')->toArray();
        } else {
            $word_blacklist = Blacklist::where('for', 'word_blacklist')
                        ->where( function($query) {
                            $query->where('created_by', Auth::id())
                                ->orWhere('user_id', 'like', '%-1%')
                                ->orWhere('user_id', 'like', '%"'. Auth::id() .'"%');
                        })
                        ->pluck('name')
                        ->toArray();
        }

        // $word_blacklist = Blacklist::where('for', 'word_blacklist')->pluck('name')->toArray();

        if(count($word_blacklist) > 0) {

            $block_matches = array_filter($word_blacklist, function($value) use($emailTemplate) {
                if(strpos($emailTemplate, $value) !== false){
                    return $value;
                }
            });    

            if(count($block_matches) > 0){
                Session::flash('block_errors', implode(', ', $block_matches). ' block word(s) found to your template');
                return redirect()->back();
            }
            // foreach($word_blacklist as $block_word) {
                // $trackingWithTemplate = str_replace($block_word, '', $trackingWithTemplate);
            // }


        }
        //end block word check with template


        $send_later = $request->send_later_email;

        $subject = $request->subject;

        $count_email = count($emailLists);

        if (env('APP_ENV', false) == 'demo') {
            $emailLists = [array_values($emailLists->toArray())[0]];
        }

        $attachment_list = [];

        if($request->has('select_attachment')) {
            foreach($request->select_attachment as $attachment_id) {
                $attachment = Attachment::find($attachment_id);
                if($attachment) {
                    $attachment_list[] = $attachment->path;
                }
            }
        }


        if ($send_later == '1') {
            if($request->has('datetime')){
                $datetime = new DateTime($request->datetime);
                if($request->timezone > 0) {
                    $datetime->modify('-' . $request->timezone . ' hours');
                }
                $emailLater = [
                    'sender_id' => Auth::id(),
                    'email_list' => json_encode($emailLists),
                    'template_id' => $request->template,
                    'subject' => $subject,
                    'send_time' => $datetime->getTimestamp(),
                    'attachment_list' => json_encode($attachment_list),
                    'url' => url('')
                ];

                EmailLater::create($emailLater);
                if($package) {
                    $user->email_limit = $user->email_limit - count($emailLists);
                    $user->save();
                }

                Session::flash('success_msg', 'Your email will be sent on ' . $request->datetime);
            } else {
                Session::flash('error_msg', 'Please select valid date and time for sending later.');
                return redirect()->back()->withInput($request->only(['email_identifier']));
            }
        } else {

//            $mail_driver = 'log';
            // if(Auth::user()->role == 'user'){
            //     $admin_or_customer_id = Auth::user()->customer_id;
            // } else {
            //     $admin_or_customer_id = Auth::id();
            // }
            // $mail_driver = User::find($admin_or_customer_id)->mail_driver;

            // $email_info = Email::find($mail_provider->id);

            // if(! $email_info) {
            //     Session::flash('error_msg', 'Mail Credential not found.');
            //     return redirect()->back();
            // }

            // if($mail_driver == 'smtp') {
            //     $email_info = Email::where('user_id', $admin_or_customer_id)->where('default_enable', 1)->first();
            //     if(! $email_info){
            //         $email_info = Email::where('user_id', $admin_or_customer_id)->first();
            //     }
            // } else if($mail_driver == 'mail') {
            //     $email_info = User::find($admin_or_customer_id);
            // }

            $url = url('unsubscribe');

            $emailHistoryData = [
                'sender_id' => Auth::id(),
                'template_id' => $request->template,
                'attachment_list' => json_encode($attachment_list),
                'subject' => $subject
            ];

            $emailHistory = EmailHistory::create($emailHistoryData);

            foreach($emailLists as $email_add){
                $emailSentList = [
                    'email_history_id' => $emailHistory->id,
                    'email' => $email_add,
                    'seen_status' => 0,
                    'link_open' => 0
                ];
                EmailSentList::create($emailSentList);
            }

            if(Auth::user()->address) {
                $emailTemplate .= Auth::user()->address;
            }
            foreach ($emailLists as $emailId => $email) {
                $userEmail = EmailList::find($emailId);

                $replacedWith = '';
                if(isset($userEmail->name)){
                    $replacedWith = $userEmail->name;
                }

                $trackingWithTemplate = $emailTemplate .'<img src="'. url('/email-open/'. base64_encode($emailHistory->id).'/'.$email ) .'" height="1" width="1" alt="">';

                $trackingWithTemplate = str_replace('{USERNAME}', $replacedWith, $trackingWithTemplate);

                $trackingWithTemplate = str_replace('href="', 'href="'.url('/link-open/'. base64_encode($emailHistory->id).'/'.$email).'?&url=' , $trackingWithTemplate);
                $trackingWithTemplate = str_replace("href='", "href='".url('/link-open/'. base64_encode($emailHistory->id).'/'.$email ).'?&url=' , $trackingWithTemplate);

                $jobs = (new EmailQueueJob($email_info, $mail_provider, $trackingWithTemplate, $emailId, $email, $subject, $attachment_list, $url))->delay(1);
                $this->dispatch($jobs);
//                Log::info(['before'. date('h:m:s')]);
            }

            if($package) {
                $user->email_limit = $user->email_limit - count($emailLists);
                $user->save();
            }

//        return redirect('send-mail');

//        Mail::queue('emails.template', ['emailTemplate' => $emailTemplate], function ($message) use ($emailLists, $subject) {
//            $message->to($emailLists)->subject($subject);
//        });

            if (env('APP_ENV', false) == 'demo' && ($count_email > 1)) {
                Session::flash('success_msg', 'Message are successfully queued to deliver. Only 1 email is allowed in demo version.');
            } else {
                Session::flash('success_msg', 'Message are successfully queued to deliver.');
            }
        }
//        Log::error(date('i-s', time() - $time_start));
        return redirect('send-mail');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
