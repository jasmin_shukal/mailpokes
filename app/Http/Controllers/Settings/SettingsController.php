<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config\Config;
use Session;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attachment_types = [
            'doc' => 'doc',
            'docx' => 'docx',
            'txt' => 'txt',
            'xls' => 'xls',
            'xlsx' => 'xlsx',
            'pdf' => 'pdf',
            'zip' => 'zip',
            'rar' => 'rar',
            'jpeg' => 'jpeg',
            'jpg' => 'jpg',
            'png' => 'png',
            'bmp' => 'bmp',
            'gif' => 'gif',
            'svg' => 'svg'
        ];

        //loaded email settings index page
        $pageName = trans('common.system') . ' ' . trans('common.settings');
        return view('Settings.system_settings.home', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->has('attachment_type')) {
            $config = Config::where('key', 'attachment_type')->update(['value' => '']);
        }

        foreach ($request->except('_token') as $key => $value) {
            if ($key == 'attachment_type') {
                $value = json_encode($value);
            }

            $config = Config::where('key', $key)->first();
            if ($config) {
                $config->update(['value' => $value]);
            } else {
                Config::create([
                    'key' => $key,
                    'value' => $value
                ]);
            }
        }

        Session::flash('success_msg', trans('common.successfully_inserted_information'));
        return redirect('system_settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
