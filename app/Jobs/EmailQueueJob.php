<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\ExternalApi\ElasticMail;
use App\Http\Controllers\ExternalApi\SendGridMail;
use Illuminate\Support\Facades\Log;
use File;
use Mail;
use Illuminate\Support\Facades\Crypt;

class EmailQueueJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $emailTemplate;
    public $emailId;
    public $email;
    public $subject;
    public $email_info;
    public $mail_provider;
    public $attachment_list;
    public $url;
    /**
     * Create a new job instance.
     *
     * @return void
     *
     */

    public $tries = 3;

    public $timeout = 30;

    public function __construct($email_info, $mail_provider, $emailTemplate = null, $emailId = null, $email, $subject, $attachment_list, $url = null)
    {
        $this->email_info = $email_info;
        $this->mail_provider = $mail_provider;
        $this->emailTemplate = $emailTemplate;
        $this->emailId = $emailId;
        $this->email = $email;
        $this->subject = $subject;
        $this->attachment_list = $attachment_list;
        $this->url = $url;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email_info = $this->email_info;
        $mail_provider = $this->mail_provider;
        $emailTemplate = $this->emailTemplate;
        $emailId = $this->emailId;
        $email = $this->email;
        $subject = $this->subject;
        $attachment_list = $this->attachment_list;
        $url = $this->url;
        logger($mail_provider->provider);
        if($mail_provider->provider == 'elastic') {
            Log::error("Elastic  Mail...");
            $elasticMail = new ElasticMail($email_info, $mail_provider, $emailTemplate, $emailId, $email, $subject, $attachment_list, $url);
            $elasticMail->shoot();
        } else if($mail_provider->provider == 'sendgrid') {
            Log::error("SendGride  Mail...");
            $sendGridMail = new SendGridMail($email_info, $mail_provider, $emailTemplate, $emailId, $email, $subject, $attachment_list, $url);
            $sendGridMail->shoot();
        } else {

            config(['mail.driver' => $mail_provider->provider]); // detect mail driver
            config(['mail.from.address' => $email_info->from_email]);
            config(['mail.from.name' => $email_info->name]);

            $name = $email_info->name;
            $replyToAddress = '';
            if(isset($email_info->reply_to) && ($email_info->reply_to != '')) {
                $replyToAddress = $email_info->reply_to;
            }
            

            //config(['mail.driver' => 'log']); // detect mail driver

            if ($mail_provider->provider == 'smtp') {
                config(['mail.host' => $email_info->smtp_host]);
                config(['mail.username' =>  $email_info->smtp_username]);
                config(['mail.password' => $email_info->smtp_password]);
                config(['mail.port' => $email_info->smtp_port]);
                
                if(isset($email_info->encryption) && ($email_info->encryption != '')) {
                    config(['mail.encryption' => $email_info->encryption]);
                }

            } else {
                if(isset($mail_provider->credentials) && ($mail_provider->credentials != '')) {
                    foreach (json_decode($mail_provider->credentials) as $credential) {
                        config(['services.'.$mail_provider->provider.'.'.$credential => $email_info->{$credential}]);
                    }
                    Log::error(config('services.'.$mail_provider->provider));
                }
            }
            Log::error(config('mail'));

            if(isset($emailId)) {
                Mail::send('emails.template', ['emailTemplate' => $emailTemplate, 'emailId' => $emailId, 'url' => $url], function ($message) use ($email, $subject, $url, $emailId, $attachment_list, $name, $replyToAddress) {

                    $headers = $message->getHeaders();
                    $headers->addTextHeader('List-Unsubscribe', '<'.$url .'/' . Crypt::encrypt($emailId) . '>');
                    $message->to($email)->subject($subject);
                    if($replyToAddress) {
                        $message->replyTo($replyToAddress, $name);
                    }
                    foreach($attachment_list as $attachment) {
                        if (File::exists(base_path('public/attachment/'.$attachment))) {
                            $message->attach(base_path('public/attachment/'.$attachment));
                        }
                    }

                });
            } else {
                Mail::send('emails.remainder', ['url' => $url], function ($message) use ($email, $subject, $url, $emailId, $name, $replyToAddress) {

                    $headers = $message->getHeaders();
                    $headers->addTextHeader('List-Unsubscribe', '<'. $url .'/' . Crypt::encrypt($emailId). '>');
                    $message->to($email)->subject($subject);
                    if($replyToAddress) {
                        $message->replyTo($replyToAddress, $name = null);
                    }

                });
            }
        }
    }
}
