<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use Log, Mail, DB, File;
use App\Models\EmailList\EmailList;
use App\Models\Attachment\Attachment;
use App\Models\Template\Template;
use App\Models\EmailHistory\EmailHistory;
use App\Models\EmailSentList\EmailSentList;
use App\Models\Blacklist\Blacklist;
use App\Mail\EmailSendingProcess;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\ExternalApi\ElasticMail;
use App\Http\Controllers\ExternalApi\SendGridMail;
use App\Mail\SendGrid;
use App\Http\Controllers\Cron\CronController;



class EmailSendingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

 
    public $template;
    public $subject;
    public $group_id;
    public $total_email;
    public $select_attachment;
    public $user_id;
    public $user_address;
    public $email_info;
    public $mail_provider;
    public $domainName;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $tries = 1;

    public $timeout = 3600;
    
    public function __construct($request)
    {
        $this->template = $request['template'];
        $this->subject = $request['subject'];
        $this->group_id = $request['group_name'];
        $this->total_email = $request['total_email'];
        $this->select_attachment = $request['select_attachment'];
        $this->user_id = $request['user_id'];
        $this->user_address = $request['user_address'];
        $this->email_info = $request['email_info'];
        $this->mail_provider = $request['mail_provider'];
        $this->domainName = $request['domainName'];
  
    }

    /**
     * Execute the job.
     *;
     * @return void
     */
    public function handle()
    {
        $email_info = $this->email_info;
        $mail_provider = $this->mail_provider;
        $domainName = $this->domainName;
        
        $subject = $this->subject;
        $emails = EmailList::select('id','name','email')->where('group_id', $this->group_id)->get();
        $template = Template::where('id', $this->template)->pluck('template');
        
        if(! is_null($template)){
            foreach($template as $temp){
                $template  = $temp;
            }
        }else{
            $template = '';
        }
       
        
        $user_id = $this->user_id;
        $user_address = $this->user_address;
       

        if($user_id == superadmin('id')) {
            $black_email_list = Blacklist::where('created_by', $user_id)->where('for', 'email_blacklist')->pluck('name')->toArray();
            $word_blacklist = Blacklist::where('created_by', $user_id)->where('for', 'word_blacklist')->pluck('name')->toArray();
        } else {
            $black_email_list = Blacklist::where('for', 'email_blacklist')
                        ->where( function($query) use ($user_id){
                            $query->where('created_by', $user_id)
                                ->orWhere('user_id', 'like', '%-1%')
                                ->orWhere('user_id', 'like', '%"'. $user_id .'"%');
                        })
                        ->pluck('name')
                        ->toArray();
            $word_blacklist = Blacklist::where('for', 'word_blacklist')
                        ->where( function($query) use ($user_id){
                            $query->where('created_by', $user_id)
                                ->orWhere('user_id', 'like', '%-1%')
                                ->orWhere('user_id', 'like', '%"'. $user_id .'"%');
                        })
                        ->pluck('name')
                        ->toArray();
        } 
        
        if(count($black_email_list) > 0) {
            $block_emails = array_filter($black_email_list, function($value) use($emails) {
                if(strpos($emails, $value) !== false){
                    unset($emails[$value]);
                }
            });    
        }

        if(count($word_blacklist) > 0) {

            $block_matches = array_filter($word_blacklist, function($value) use($template) {
                if(strpos($template, $value) !== false){
                    unset($template[$value]);
                }
            });    

        }

        $attachment_list = [];

        if(isset($this->select_attachment)) {
            foreach($this->select_attachment as $attachment_id) {
                $attachment = Attachment::find($attachment_id);
                if($attachment) {
                    $attachment_list[] = $attachment->path;
                }else{
                    $attachment_list = '';
                }
            }
        }

        $url = $domainName . '/unsubscribe';

            $emailHistoryData = [
                'sender_id' => $user_id,
                'template_id' => $this->template,
                'attachment_list' => json_encode($attachment_list),
                'subject' => $this->subject
            ];

            $emailHistory = EmailHistory::create($emailHistoryData);
            
            if($user_address) {
                $template .= $user_address;
            }
            $searchValue = [];
            $emailSentList = [];

            if(strpos($template, '{USERNAME}') !== false){
                $searchValue[] = '{USERNAME}';
            }
            if(strpos($template, 'href="') !== false){
                $searchValue[] = 'href="';
            }
            $countSearchValue = count($searchValue);
            $emailSentList = [];

                //mail credentials
                $destinationPath = storage_path('imports');
                $fileName = 'emailSentList'. $user_id. $emailHistory->id. '.csv';
                $fileName = $destinationPath.'/'. $fileName;
                $fp = fopen($fileName, 'w');
//Mail Sending For Elastic Mail Credentials
            if($mail_provider->provider){
                if($mail_provider->provider == 'elastic') {
                    foreach($emails as $email){
                        $email_id = $email->id;
                        $name = $email->name;
                        $email = $email->email;
                        $emailSentList = [
                            'email_history_id' => $emailHistory->id,
                            'email' => $email,
                            'seen_status' => 0,
                            'link_open' => 0
                        ];
                        fputcsv($fp, $emailSentList);
                        $userEmail = $name;
                        $replacedWith = '';
                        if(isset($userEmail)){
                            $replacedWith = $userEmail;
                        }
                        $trackingWithTemplate = $template .'<img src="'. $domainName.'/email-open/'. base64_encode($emailHistory->id).'/'.$email  .'" height="1" width="1" alt="">';
        
                        $replacedWith = [];
                        if($countSearchValue > 0){
                            if(in_array('{USERNAME}', $searchValue)) {
                                
                                $userName = '';
                                if(isset($name) && $name != '') {
                                    $userName = $name;
                                }
        
                                $replacedWith[] = $userName;
                            }
        
                            if(in_array('href="', $searchValue)) {
                                
                                $replacedWith[] = 'href="'. $domainName.'/link-open/'. base64_encode($emailHistory->id).'/'.$email.'?&url=';
                            }
        
                            $trackingWithTemplate = str_replace($searchValue, $replacedWith, $trackingWithTemplate);
                        }
                        //mysql connection process kill method 
                        $processKill = new CronController();
                        $processKill->processKill();
    
                    Log::error("Elastic  Mail...");
                    $elasticMail = new ElasticMail($email_info, $mail_provider, $trackingWithTemplate, $email_id, $email, $subject, $attachment_list, $url);
                    $elasticMail->shoot();
                    }
                    
                    try{
                        $pdo = DB::connection()->getPdo();
        
                        $res = $pdo->exec("
                                            LOAD DATA LOCAL INFILE '".$fileName."' 
                                            INTO TABLE email_sent_list 
                                            FIELDS TERMINATED BY ',' 
                                            LINES TERMINATED BY '\\n' 
                                            (email_history_id, email, seen_status, link_open)
                                        ");
                        $res = null;
                        fclose($fp);
                        unset($res);
                        unlink($fileName);
                    }catch(PDOException $e){
                        Log::error("Error!: " . $e->getMessage() . "<br/>");
                    }

                } else if($mail_provider->provider == 'sendgrid') { //Mail Sending for SendGrid Mail Credentials
                    config(['mail.driver' => 'smtp']); // detect mail driver
                    config(['mail.from.address' => $email_info->from_email]);
                    config(['mail.from.name' => $email_info->name]);
                    config(['mail.host' => $email_info->smtp_host]);
                    config(['mail.username' =>  $email_info->smtp_username]);
                    config(['mail.password' => $email_info->smtp_password]);
                    config(['mail.port' => $email_info->smtp_port]);
                    if(isset($email_info->encryption) && ($email_info->encryption != '')) {
                        config(['mail.encryption' => $email_info->encryption]);
                    }
                    
                    foreach($emails as $email){
                        $email_id = $email->id;
                        $name = $email->name;
                        $email = $email->email;
                        $emailSentList = [
                            'email_history_id' => $emailHistory->id,
                            'email' => $email,
                            'seen_status' => 0,
                            'link_open' => 0
                        ];
                        fputcsv($fp, $emailSentList);
                        $userEmail = $name;
                        $replacedWith = '';
                        if(isset($userEmail)){
                            $replacedWith = $userEmail;
                        }
                        $trackingWithTemplate = $template .'<img src="'. $domainName.'/email-open/'. base64_encode($emailHistory->id).'/'.$email  .'" height="1" width="1" alt="">';
        
                        $replacedWith = [];
                        if($countSearchValue > 0){
                            if(in_array('{USERNAME}', $searchValue)) {
                                
                                $userName = '';
                                if(isset($name) && $name != '') {
                                    $userName = $name;
                                }
        
                                $replacedWith[] = $userName;
                            }
        
                            if(in_array('href="', $searchValue)) {
                                
                                $replacedWith[] = 'href="'. $domainName.'/link-open/'. base64_encode($emailHistory->id).'/'.$email.'?&url=';
                            }
        
                            $trackingWithTemplate = str_replace($searchValue, $replacedWith, $trackingWithTemplate);
                        }
                        Log::info('send grid....');
                       //mysql connection process kill method 
                       $processKill = new CronController();
                       $processKill->processKill();
                      
                        $response = Mail::to($email)->send(new SendGrid($email_info,$mail_provider, $trackingWithTemplate, $email_id, $subject,  $attachment_list, url(''), ''));
                       
                    }

                    try{
                        $pdo = DB::connection()->getPdo();
        
                        $res = $pdo->exec("
                                            LOAD DATA LOCAL INFILE '".$fileName."' 
                                            INTO TABLE email_sent_list 
                                            FIELDS TERMINATED BY ',' 
                                            LINES TERMINATED BY '\\n' 
                                            (email_history_id, email, seen_status, link_open)
                                        ");
                        $res = null;
                        fclose($fp);
                        unset($res);
                        unlink($fileName);
                    }catch(PDOException $e){
                        Log::error("Error!: " . $e->getMessage() . "<br/>");
                    }
                
                } else {
                    //Mail Sending for smtp mail credentials 
                    
                    config(['mail.driver' => $mail_provider->provider]); // detect mail driver
                    config(['mail.from.address' => $email_info->from_email]);
                    config(['mail.from.name' => $email_info->name]);
        
                    $name = $email_info->name;
                    $replyToAddress = '';
                    if(isset($email_info->reply_to) && ($email_info->reply_to != '')) {
                        $replyToAddress = $email_info->reply_to;
                    }
                    
        
                    // config(['mail.driver' => 'log']);
                    if(env('MAIL_DRIVER', false) == 'log'){

                        config(['mail.driver' => env('MAIL_DRIVER', false)]); // set mail driver

                    }
              
                    // Log::info(config('mail.driver'));
                    if ($mail_provider->provider == 'smtp') {
                        
                        config(['mail.host' => $email_info->smtp_host]);
                        config(['mail.username' =>  $email_info->smtp_username]);
                        config(['mail.password' => $email_info->smtp_password]);
                        config(['mail.port' => $email_info->smtp_port]);
                        
                        if(isset($email_info->encryption) && ($email_info->encryption != '')) {
                            config(['mail.encryption' => $email_info->encryption]);
                        }
        
                    } else {
                       
                        if(isset($mail_provider->credentials) && ($mail_provider->credentials != '')) {
                            foreach (json_decode($mail_provider->credentials) as $credential) {
                                config(['services.'.$mail_provider->provider.'.'.$credential => $email_info->{$credential}]);
                            }
                            Log::error(config('services.'.$mail_provider->provider));
                        }
                    }
                    
                    
                    foreach($emails as $email){
                        $email_id = $email->id;
                        $name = $email->name;
                        $email = $email->email;
                        $emailSentList = [
                            'email_history_id' => $emailHistory->id,
                            'email' => $email,
                            'seen_status' => 0,
                            'link_open' => 0
                        ];
                        fputcsv($fp, $emailSentList);
                        $userEmail = $name;
                        $replacedWith = '';
                        if(isset($userEmail)){
                            $replacedWith = $userEmail;
                        }
                        $trackingWithTemplate = $template .'<img src="'. $domainName.'/email-open/'. base64_encode($emailHistory->id).'/'.$email .'" height="1" width="1" alt="">';
        
                        $replacedWith = [];
                        if($countSearchValue > 0){
                            if(in_array('{USERNAME}', $searchValue)) {
                                
                                $userName = '';
                                if(isset($name) && $name != '') {
                                    $userName = $name;
                                }
        
                                $replacedWith[] = $userName;
                            }
        
                            if(in_array('href="', $searchValue)) {
                                
                                $replacedWith[] = 'href="'. $domainName.'/link-open/'. base64_encode($emailHistory->id).'/'.$email.'?&url=';
                            }
        
                            $trackingWithTemplate = str_replace($searchValue, $replacedWith, $trackingWithTemplate);
                        }

                        //smtp
                        if(isset($email_id)) {   
                            // //mysql connection process kill method 
                            $processKill = new CronController();
                            $processKill->processKill();

                            try {
                                Mail::send('emails.template', ['emailTemplate' => $trackingWithTemplate, 'emailId' => $email_id, 'url' => $url], function ($message) use ($email, $subject, $url, $email_id, $attachment_list, $name, $replyToAddress) {
            
                                    $headers = $message->getHeaders();
                                    $headers->addTextHeader('List-Unsubscribe', '<'.$url .'/' . Crypt::encrypt($email_id) . '>');
                                    $message->to($email)->subject($subject);
                                    if($replyToAddress) {
                                        $message->replyTo($replyToAddress, $name);
                                    }
                                    foreach($attachment_list as $attachment) {
                                        
                                        if (File::exists(public_path('attachment/'.$attachment))) {
                                        
                                            $message->attach(public_path('attachment/'.$attachment));
                                        }
                                    }
                                });
                            } catch (\Throwable $e) {
                                Log::error($e->getMessage() . "<br/>");
                            }

                        } else {
                            // //mysql connection process kill method 
                            $processKill = new CronController();
                            $processKill->processKill();
                            // Log::info(config('mail'));
                            Mail::send('emails.remainder', ['url' => $url], function ($message) use ($email, $subject, $url, $email_id, $name, $replyToAddress) {
            
                                $headers = $message->getHeaders();
                                $headers->addTextHeader('List-Unsubscribe', '<'. $url .'/' . Crypt::encrypt($email_id). '>');
                                $message->to($email)->subject($subject);
                                if($replyToAddress) {
                                    $message->replyTo($replyToAddress, $name = null);
                                }
            
                            });
                        }
                    }
                     
                    try{
                        // $storingHistory = DB::select(DB::raw(" LOAD DATA LOCAL INFILE '".$fileName."' 
                        //                      INTO TABLE email_sent_list 
                        //                      FIELDS TERMINATED BY ',' 
                        //                      LINES TERMINATED BY '\\n' 
                        //                      (email_history_id, email, seen_status, link_open)"));
                        // unset($storingHistory);
                        // $pdo = DB::connection()->getPdo();
        
                        // $res = $pdo->exec("
                        //                     LOAD DATA LOCAL INFILE '".$fileName."' 
                        //                     INTO TABLE email_sent_list 
                        //                     FIELDS TERMINATED BY ',' 
                        //                     LINES TERMINATED BY '\\n' 
                        //                     (email_history_id, email, seen_status, link_open)
                        //                 ");

                        $link=mysqli_connect(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_DATABASE'));

                        $load_data_to_temp_table_sql = "LOAD DATA LOCAL INFILE '".$fileName."' 
                                                        INTO TABLE email_sent_list 
                                                        FIELDS TERMINATED BY ',' 
                                                        LINES TERMINATED BY '\\n' 
                                                        (email_history_id, email, seen_status, link_open)";

                      /*  $loadDataToTempTableRes = mysqli_query($link,$load_data_to_temp_table_sql);*/

                        $loadedData = mysqli_affected_rows($link);
                        // DB::select($load_data_to_temp_table_sql);


                        // $res = null;
                        fclose($fp);
                        // unset($res);
                        unlink($fileName);
                    }catch(PDOException $e){
                        Log::error("Error!: " . $e->getMessage() . "<br/>");
                    }
                   
                   
                }
            }

                // Mail::to($email)->send(new EmailSendingProcess($this->subject, $trackingWithTemplate));
            
            $db = DB::connection()->getDatabaseName();
    
            if($db){
                DB::disconnect($db);
            };
    }
}
