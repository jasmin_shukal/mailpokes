<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;
use Log, DB;
use Illuminate\Support\Facades\Mail;
use App\Models\EmailList\EmailList;
use App\Mail\EmailSendingProcess;


class GoodJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $group_id;
    private $template;
    private $subject;
    public $al;
    public $temp;
    public $al_emails;
    public $numItems;
    public $template_name;
    public function __construct($allRequest)
    {
        $this->group_id = $allRequest['group_name'];
        $this->template = $allRequest['template'];
        $this->subject = $allRequest['subject'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $group = $this->group_id;
        $template = $this->template;
        $sub = $this->subject;
        $numItems = $this->numItems;
       
        $this->temp = DB::table('template')->select('template')->where('id', '=', '3')->get();
        
        $emails = EmailList::select('email')->where('group_id', $group)->get();
        $al = $this->al;
        $al = 0;
        $numItems = count($emails);
       
        foreach($emails as $key){
            ++$al;
            // if($al % 21000 == 0){
            //     sleep(2);
            // }
            Mail::to($key->email)->send(new EmailSendingProcess($al, $sub, $template));
        }

        
        // Log::info($this->al_emails);
        // Mail::to($this->al_emails)->send(new EmailSendingProcess($template, $sub));
 
    }
}
