<?php

namespace App\Jobs;

use Excel;
use App\Models\EmailList\EmailList;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class ImportQueueJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $type;
    protected $data_path;
    protected $group_id_list;
    /**
     * Create a new job instance.
     *
     * @return void
     */


    public $tries = 3;

    public $timeout = 30;

    
    public function __construct($type, $data_path, $group_id_list)
    {
        $this->type = $type;
        $this->data_path = $data_path;
        $this->group_id_list = $group_id_list;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        $data['group_id'] = $this->group_id_list;
        if($this->type == 'emails'){

            $commaAndNew = str_replace("\r\n", ',', $this->data_path);

            $string = array_unique(explode(",", $commaAndNew));
            if (array_search("", $string)) {
                $position = array_search("", $string);
                unset($string[$position]);
            }

            $validEmails = array_diff(validEmail($string), array(NULL));

            foreach ($validEmails as $email) {
                $data['email'] = $email;
                $data['subscribed'] = 1; // email address treat as a subscriber
                foreach ($this->group_id_list as $group_id) {
                    $data['group_id'] = $group_id;
                    EmailList::create($data);
                }
            }
        }

        if($this->type == 'excel_file'){
        //     $results = Excel::load(storage_path('imports/' . $this->data_path), function ($reader) {

        // })->get(array('name', 'email'))->toArray();

            $results = csvFileRead(storage_path('imports/' . $this->data_path));

            $validEmails = validEmailForFile($results);

            foreach ($validEmails as $emailInfo) {
                $data['name'] = $emailInfo['name'];
                $data['email'] = $emailInfo['email'];
                $data['subscribed'] = 1; // email address treat as a subscriber
                foreach ($this->group_id_list as $group_id) {
                    $data['group_id'] = $group_id;
                    EmailList::create($data);
                }
            }
        }
    }
}
