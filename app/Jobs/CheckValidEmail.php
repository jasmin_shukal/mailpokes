<?php

namespace App\Jobs;

use App\Jobs\EmailQueueJob;
use App\Models\Email\Email;
use Auth;
use App\Models\Group\Group;
use App\Http\Controllers\Checker\EmailChecker;
use App\Http\Controllers\Checker\VerifyEmail;
use App\Jobs\Job;
use ClassPreloader\Config;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Redirect;
use Log;
use Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CheckValidEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    protected $checker;
    protected $para;
    protected $emails;
    protected $group_id;
    protected $user;
    protected $url;

    /**
     * Create a new job instance.
     *
     * @return void
     *
     */

    public $tries = 3;

    public $timeout = 30;

    public function __construct($checker, $para, $emails, $group_id, $user, $url)
    {
        $this->checker = $checker;
        $this->para = $para;
        $this->emails = $emails;
        $this->group_id = $group_id;
        $this->user = $user;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->checker == 'bulkemailchecker') {
                $emailChecker = new EmailChecker('bulkemailchecker', $this->para);
        } elseif ($this->checker == 'emaillistverify') {
                $emailChecker = new EmailChecker('emaillistverify', $this->para);
        } elseif ($this->checker == 'freeemailverify') {
            $emailChecker = new VerifyEmail($this->para);
        }

        $deleted = 0;
        foreach ($this->emails as $email) {
            $json = $emailChecker->check($email->email);

            if ($this->checker == 'bulkemailchecker') {
                if ($json['status'] == 'failed') {
                    $email->update(['bulk_check' => 2, 'bulk_value' => json_encode($json)]);
                    $deleted++;
                } else {
                    $email->update(['bulk_check' => 1, 'bulk_value' => json_encode($json)]);
                }
            } elseif ($this->checker == 'emaillistverify') {
                if ($json == 'ok') {
                    $email->update(['email_list_check' => 1, 'email_list_value' => $json]);
                } else {
                    $email->update(['email_list_check' => 2, 'email_list_value' => $json]);
                    $deleted++;
                }
            } elseif ($this->checker == 'freeemailverify') {
                if ($json) {
                    $email->update(['free_email_check' => 1, 'free_email_value' => 'Valid Email']);
                } else {
                    $email->update(['free_email_check' => 2, 'free_email_value' => 'Invalid Email']);
                    $deleted++;
                }
            }

            if (env('APP_ENV', false) == 'demo') {
                break;
            }
        }

        if ($this->checker == 'bulkemailchecker') {
            Group::find($this->group_id)->update(['bulk_check' => 1, 'bulk_check_date' => time()]);
        } elseif ($this->checker == 'emaillistverify') {
            Group::find($this->group_id)->update(['email_list_check' => 1, 'email_list_verify_date' => time()]);
        } elseif ($this->checker == 'freeemailverify') {
            Group::find($this->group_id)->update(['free_email_check' => 1, 'free_email_check_date' => time()]);
        }


        if (env('APP_ENV', false) == 'demo') {
            Session::flash('error_msg', 'Detected Invalid ' . $deleted . ' email address(es) in demo only 1 email can be checked.');
        } else {
            Session::flash('success_msg', 'Detected Invalid ' . $deleted . ' email address(es).');
        }

        $user = $this->user;
        $email = $user->email;
        $subject = 'Remainder';

        $mail_driver = $user->mail_driver;

        $email_info = Email::where('user_id', $user->id)->first();
        $url = $this->url;
        logger($mail_driver);
        // $jobs = (new EmailQueueJob($email_info, $mail_driver, null, null, $email, $subject, null, $url))->delay(1);

        // $this->dispatch($jobs);

    }
}
