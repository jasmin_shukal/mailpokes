<?php
function validEmail($garbaseEmail)
{
    function myfunction($email)
    {
        $a = trim($email);
        if (!filter_var($a, FILTER_VALIDATE_EMAIL)) {
            // invalid emailaddress
        } else {
            return $a;
        }
    }

    $validEmail = array_map("myfunction", $garbaseEmail);
    $validEmail = array_unique($validEmail);

    if (array_search("", $validEmail)) {
        $position = array_search("", $validEmail);
        unset($validEmail[$position]);
    }
    return $validEmail;
}

function validEmailForFile($data)
{
    $results = [];
    $email_unique_arr = [];

    foreach($data as $key => $value) {
        if (filter_var($value['email'], FILTER_VALIDATE_EMAIL)) {
            if (!in_array($value['email'], $email_unique_arr)) {
                $results[] = $value;
                $email_unique_arr[] = $value['email'];
            }
        }
    }

    // array_walk($data, function ($value, $key) use (&$results, &$email_unique_arr) {
    //     if (filter_var($value['email'], FILTER_VALIDATE_EMAIL)) {
    //         if (!in_array($value['email'], $email_unique_arr)) {
    //             $results[] = $value;
    //             $email_unique_arr[] = $value['email'];
    //         }
    //     }
    // });

    return $results;
}

function validEmailNumber($group_id, $par)
{
    return \App\Models\EmailList\EmailList::where('group_id', $group_id)->where($par, 1)->count();
}

function invalidEmailNumber($group_id, $par)
{
    return \App\Models\EmailList\EmailList::where('group_id', $group_id)->where($par, 2)->count();
}

function seen_unseen_list($email_sent_list_id, $status)
{
    return \App\Models\EmailSentList\EmailSentList::where('email_history_id', $email_sent_list_id)->where('seen_status', $status)->count();
}

function click_rate($email_history_id)
{
    $total_list = \App\Models\EmailSentList\EmailSentList::where('email_history_id', $email_history_id)->count();
    $click_list = \App\Models\EmailSentList\EmailSentList::where('email_history_id', $email_history_id)->where('link_open', 1)->count();
    if($click_list>0)
    {
        return $click_list * 100 / $total_list;
    }
    else
    {
        return $click_list;
    }
}

function email_limit()
{
    $email_limit = 0;
    if (\Auth::check()) {
        $email_limit = \Auth::user()->email_limit;
        if (\Auth::user()->role == 'user') {
            $email_limit = \App\User::find(Auth::user()->customer_id)->email_limit;
        }
    }
    return $email_limit;

}

function user_limit()
{
    $user_limit = 0;
    if (\Auth::check()) {
        $user_limit = \Auth::user()->user_limit;
        if (\Auth::user()->role == 'user') {
            $user_limit = \App\User::find(Auth::user()->customer_id)->user_limit;
        }
    }
    return $user_limit;

}

function superadmin($field) {
    $superAdmin = \App\User::where('role', 'superadmin')->first();
    return $superAdmin->{$field};
}


function user_or_customer_info($id, $field) {
    $user = \App\User::find($id);
    if(isset($user->{$field})) {
        return $user->{$field};
    }
    return false;
}

function formatSizeUnits($bytes){

    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' kB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function file_image_check($fileMimeType) {
    $imageMimeTypes = array(
        'image/png',
        'image/gif',
        'image/jpeg');

    // $fileMimeType = mime_content_type($_FILES['UPLFILE2']['tmp_name']);

    if (in_array($fileMimeType, $imageMimeTypes)) {
        return true;
    } else {
        return false;
    }
}

function department_user($department_id) {
    return \App\User::where('department_id_list', 'like', '%"'.$department_id.'"%')->groupBy('id')->pluck('id')->toArray();
}

function customer_user($customer_id) {
    return \App\User::where('customer_id', $customer_id)->pluck('id')->toArray();
}


//find php cli path
function findPhpCli()
{
        $cliPath = '/usr/bin/php';

        if (!functionExists('exec')) {
            return $cliPath;
        }

        $phps = array('php-cli', 'php5-cli', 'php5', 'php');
        foreach ($phps as $php) {
            $output = @exec(sprintf('command -v %s 2>&1', $php), $lines, $status);
            if ($status != 0 || empty($output)) {
                continue;
            }
            $cliPath = $output;
            break;
        }

        return $cliPath;
}

// check php function exists or not
function functionExists($name)
    {
        static $_exists     = array();
        static $_disabled   = null;
        static $_shDisabled = null;

        if (isset($_exists[$name]) || array_key_exists($name, $_exists)) {
            return $_exists[$name];
        }

        if (!function_exists($name)) {
            return $_exists[$name] = false;
        }

        if ($_disabled === null) {
            $_disabled = ini_get('disable_functions');
            $_disabled = explode(',', $_disabled);
            $_disabled = array_map('trim', $_disabled);
        }

        if (is_array($_disabled) && in_array($name, $_disabled)) {
            return $_exists[$name] = false;
        }

        if ($_shDisabled === null) {
            $_shDisabled = ini_get('suhosin.executor.func.blacklist');
            $_shDisabled = explode(',', $_shDisabled);
            $_shDisabled = array_map('trim', $_shDisabled);
        }

        if (is_array($_shDisabled) && in_array($name, $_shDisabled)) {
            return $_exists[$name] = false;
        }

        return $_exists[$name] = true;
    }

    include 'csv.php';