<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                @if (Auth::check() && (isset(Auth::user()->photo)) && (Auth::user()->photo != ''))
                    @if(strpos(Auth::user()->photo, 'http') !== false)
                        <img src="{{ Auth::user()->photo }}" style="height: 45px; width: 45px;" class="img-circle"
                             alt="User Image"/>
                    @else
                        <img src="{{ url('upload/'.Auth::user()->photo) }}" style="height: 45px; width: 45px;"
                             class="img-circle" alt="User Image"/>
                    @endif
                @else
                    <img src={{ url('img/user2-160x160.jpg') }} class="img-circle" alt="User Image"/>
                @endif
            </div>
            <div class="pull-left info">
                @if(Auth::check())
                    @if(Auth::check())
                        <p>{{ Auth::user()->name }}</p>
                    @else
                        <p>John Doe</p>
                    @endif
                <!-- Status -->
                    @if(Auth::check())
                        <a href="#"><i class="fa fa-circle text-success"></i> {!! trans('common.online') !!}</a>
                    @endif
                @endif
            </div>
        </div>
    <?php $url = Request::segment(1); ?>
    <!-- Sidebar Menu -->
        @if(Auth::user())
            <ul class="sidebar-menu">
                <li class="{{ ($url == 'home' || $url == '')? "active" : ''}}"><a href="{{ url('home') }}"><i
                                class='fa fa-dashboard'></i> <span>{!! trans('common.Dashboard') !!}</span></a></li>
                @if(Auth::check() && Auth::user()->role == 'superadmin')
                    <li class="treeview {{ (($url == 'customers') || ($url == 'customer-groups')) ? "active" : ''}}">
                        <a href="#"><i class='fa fa-cogs'></i> <span>{!! trans('common.customers') !!}</span> <i
                                    class="fa fa-angle-right pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li class="{{ ($url == 'customers')? "active" : ''}}"><a href="{{ url('customers') }}"><i
                                            class='fa fa-users'></i> <span>{!! trans('common.customer_list') !!}</span></a>
                            </li>
                            <li class="{{ ($url == 'customer-groups')? "active" : ''}}"><a
                                        href="{{ url('customer-groups') }}"><i
                                            class='fa fa-users'></i>
                                    <span>{!! trans('common.customer').' '.trans('common.groups') !!}</span></a></li>

                        </ul>
                    </li>
                @endif
                @if(Auth::check() && (Auth::user()->role == 'superadmin'))
                    <li class="{{ ($url == 'departments')? "active" : ''}}"><a
                                href="{{ url('departments') }}"><i
                                    class='fa fa-users'></i>
                            <span>{!! trans('common.department') !!}</span></a></li>
                @endif
                <li class="{{ ($url == 'users')? "active" : ''}}"><a href="{{ url('users') }}"><i
                                class='fa fa-users'></i> <span>{!! trans('common.users') !!}</span>
                        @if(Auth::check())
                            <!-- <span class="badge" data-toggle='tooltip' data-placement='top'
                                  title='{{ trans('common.user_create_limit') }}'>{{ user_limit() }} </span> -->
                        @endif
                    </a></li>
                <li class="{{ ($url == 'email-list')? "active" : ''}}"><a href="{{ url('email-list') }}"><i
                                class='fa fa-list'></i> <span>{!! trans('common.email-list') !!}</span></a></li>
                {{--<li class="{{ ($url == 'message-list')? "active" : ''}}"><a href="{{ url('message-list') }}"><i class='fa fa-envelope-o'></i> <span>{!! trans('common.message_list') !!}</span></a></li>--}}
                <li class="{{ ($url == 'template')? "active" : ''}}"><a href="{{ url('template') }}"><i
                                class='fa fa-newspaper-o'></i> <span>{!! trans('common.template') !!}</span></a></li>
                <li class="{{ ($url == 'media')? "active" : ''}}"><a href="{{ url('media') }}"><i
                                class='fa fa-picture-o'></i> <span>{!! trans('common.media') !!}</span></a></li>
                <li class="{{ ($url == 'attachments')? "active" : ''}}"><a href="{{ url('attachments') }}"><i
                                class='fa fa-upload'></i> <span>{!! trans('common.attachment') !!}</span></a></li>
                <li class="{{ ($url == 'send-mail')? "active" : ''}}"><a href="{{ url('send-mail') }}"><i
                                class='fa fa-envelope'></i> <span>{!! trans('common.send-mail') !!} </span>
                        @if(Auth::check())
                            <span class="badge" data-toggle='tooltip' data-placement='top'
                                  title='{{ trans('common.remaining_email') }}'>{{ email_limit() }}</span>
                        @endif
                    </a></li>

                    
                <li class="{{ (($url == 'history') || ($url == 'sent-email-list'))? "active" : ''}} {{Auth::user()->role)}}"><a
                            href="{{ url('history') }}"><i class='fa fa-list-alt'></i>
                        <span>{!! trans('common.history') !!} 123</span></a></li>

                <li class="{{ ($url == 'tickets')? "active" : ''}}"><a href="{{ url('tickets') }}"><i
                                class='fa fa-ticket'></i> <span>{!! trans('tickets.tickets') !!}</span></a></li>

                <li class="{{ ($url == 'activities')? "active" : ''}}"><a href="{{ url('activities') }}"><i
                                class='fa fa-list-alt'></i> <span>{!! trans('common.Activities') !!}</span></a></li>

                @if(Auth::check() && Auth::user()->role != 'user')
                    <li class="treeview {{ (($url == 'email_blacklist') || ($url == 'word_blacklist') || ($url == 'domain_blacklist') || ($url == 'ip')) ? "active" : ''}}">
                        <a href="#"><i class='fa fa-ban'></i> <span>{!! trans('blacklist.blacklist') !!}</span> <i
                                    class="fa fa-angle-right pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li class="{{ ($url == 'email_blacklist')? "active" : ''}}"><a
                                        href="{{ url('email_blacklist') }}"><i
                                            class='fa fa-envelope-o'></i>
                                    <span>{!! trans('blacklist.blacklist').' '.trans('common.email') !!}</span></a></li>
                            <li class="{{ ($url == 'word_blacklist')? "active" : ''}}"><a
                                        href="{{ url('word_blacklist') }}"><i
                                            class='fa fa-krw'></i>
                                    <span>{!! trans('blacklist.blacklist').' '.trans('blacklist.word') !!}</span></a>
                            </li>

                            <li class="{{ ($url == 'domain_blacklist')? "active" : ''}}"><a
                                        href="{{ url('domain_blacklist') }}"><i
                                            class='fa fa-globe'></i>
                                    <span>{!! trans('blacklist.blacklist').' '.trans('blacklist.domain') !!}</span></a>
                            </li>
                            <li class="{{ ($url == 'ip')? "active" : ''}}"><a href="{{ url('ip') }}"><i
                                            class='fa fa-globe'></i>
                                    <span>{!! trans('blacklist.blacklist').' '.trans('blacklist.ip') !!}</span></a></li>

                        </ul>
                    </li>

                    <li class="treeview {{ (($url == 'system_settings') || ($url == 'oauth') || ($url == 'email') || ($url == 'theme')|| ($url == 'translation') || ($url == 'privacy'))|| ($url == 'api')|| ($url == 'cron')|| ($url == 'paypal-api')|| ($url == 'stripe-api') || ($url == 'ticketstatus') || ($url == 'address') || ($url == 'coupons') ? "active" : ''}}">
                        <a href="#"><i class='fa fa-cogs'></i> <span>{!! trans('common.Settings') !!}</span> <i
                                    class="fa fa-angle-right pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li class="{{ ($url == 'system_settings')? "active" : ''}}"><a
                                        href="{{ url('system_settings') }}"><i
                                            class='fa fa-wrench'></i>
                                    <span>{!! trans('common.system').' '.trans('common.Settings') !!}</span></a></li>
                            <li class="{{ ($url == 'email')? "active" : ''}}"><a href="{{ url('email') }}"><i
                                            class='fa fa-envelope-o'></i>
                                    <span>{!! trans('common.email').' '.trans('common.Settings') !!}</span></a></li>

                            <li class="{{ ($url == 'api')? "active" : ''}}"><a href="{{ url('api') }}"><i
                                            class='fa fa-envelope-o'></i> <span>{!! trans('common.api') !!}</span></a>
                            </li>
                            @if(Auth::check() &&  Auth::user()->role == 'superadmin')

                                <li class="{{ ($url == 'cron')? "active" : ''}}"><a href="{{ url('cron') }}"><i
                                                class='fa fa-cogs'></i>

                                        <span>{!! trans('common.cron') .' '. trans('common.settings') !!}</span></a>
                                </li>
                                <li class="{{ ($url == 'ticketstatus')? "active" : ''}}"><a
                                            href="{{ url('ticketstatus') }}"><i
                                                class='fa fa-bullseye'></i>
                                        <span>{!! trans('common.ticketstatus') !!}</span></a></li>
                                </li>


                                <li class="{{ ($url == 'paypal-api')? "active" : ''}}"><a
                                            href="{{ url('paypal-api') }}"><i
                                                class='fa fa-envelope-o'></i>
                                        <span>{!! trans('common.paypal_api') !!}</span></a>
                                </li>

                                <li class="{{ ($url == 'stripe-api')? "active" : ''}}"><a
                                            href="{{ url('stripe-api') }}"><i
                                                class='fa fa-envelope-o'></i>
                                        <span>{!! trans('common.stripe_api') !!}</span></a>
                                </li>

                                <li class="{{ ($url == 'oauth')? "active" : ''}}"><a href="{{ url('oauth') }}"><i
                                                class='fa fa-facebook'></i>
                                        <span>{!! trans('common.oauth').' '.trans('common.Settings') !!}</span></a>
                                </li>

                                <li class="{{ ($url == 'coupons')? "active" : ''}}"><a href="{{ url('coupons') }}"><i
                                                class='fa fa-certificate'></i>
                                        <span>{!! trans('common.coupons') !!}</span></a>
                                </li>

                                <li class="{{ ($url == 'theme')? "active" : ''}}"><a href="{{ url('theme') }}"><i
                                                class='fa fa-codepen'></i>
                                        <span>{!! trans('common.Theme').' '.trans('common.Settings') !!}</span></a>
                                </li>

                                <li class="{{ ($url == 'translation')? "active" : ''}}"><a
                                            href="{{ url('translation') }}"><i
                                                class='fa fa-language'></i>
                                        <span>{!! trans('common.Translation') !!}</span></a>
                                </li>

                                <li class="{{ ($url == 'privacy')? "active" : ''}}"><a href="{{ url('privacy') }}"><i
                                                class='fa fa-archive'></i> <span>{!! trans('common.privacy') !!}</span></a>
                                </li>

                            @endif
                            <li class="{{ ($url == 'address')? "active" : ''}}"><a href="{{ url('address') }}"><i
                                            class='fa fa-envelope-o'></i>
                                    <span>{!! trans('common.physical') . ' '. trans('common.address') !!}</span></a>
                            </li>
                        </ul>
                    </li>

                    @if(Auth::check() &&  Auth::user()->role == 'superadmin')
                        <li class="{{ ($url == 'package')? "active" : ''}}"><a href="{{ url('package') }}"><i
                                        class='fa fa-list-alt'></i> <span>{!! trans('common.package') !!}</span></a>
                        </li>
                    @endif
                    {{--<li class="{{ ($url == 'newsletter')? "active" : ''}}"><a href="{{ url('newsletter') }}"><i class='fa fa-envelope'></i> <span>{!! trans('common.newsletter') !!}</span></a></li>--}}
                    @if(Auth::check() &&  Auth::user()->role == 'superadmin')
                        {{--<li class="{{ ($url == 'backupDownload')? "active" : ''}}"><a href="{{ url('backupDownload') }}"><i class='fa fa-database'></i><span>{!! trans('common.backup') !!}</span></a></li>--}}
                    @endif
                    <li class="{{ ($url == 'purchase-history')? "active" : ''}}"><a
                                href="{{ url('purchase-history') }}"><i
                                    class='fa fa-list-alt'></i>
                            <span>{!! trans('common.purchase_history') !!}</span></a>
                    </li>
                    <li class="{{ ($url == 'pricing')? "active" : ''}}"><a href="{{ url('pricing') }}"><i
                                    class='fa fa-list-alt'></i>
                            <span>{!! trans('common.package') . ' ' .trans('common.upgrade') !!}</span></a></li>

                    <li class="{{ ($url == 'faq-setting')? "active" : ''}}"><a href="{{ url('faq-setting') }}"><i
                                    class='fa fa-list-alt'></i>
                            <span>{!! trans('common.faq') !!}</span></a></li>

                    @if(Auth::check() && Auth::user()->role == 'superadmin')
                        <li class="{{ ($url == 'frontend')? "active" : ''}}"><a href="{{ url('frontend') }}"><i
                                        class='fa fa-home'></i> <span>{!! trans('common.frontend') !!}</span></a>
                        </li>
                    @endif
                @endif
            </ul><!-- /.sidebar-menu -->
        @endif
    </section>
    <!-- /.sidebar -->
</aside>
