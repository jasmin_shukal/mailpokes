@extends('app')

@section('main-content')
    <style>
        .badge-success {
            background-color: #398439;
        }

        .badge-danger {
            background-color: #d43f3a;
        }
    </style>

    <div class="box box-default">
        <!-- Tabs within a box -->
        <div class="box-header with-border">
            <h3 class="box-title pull-left">{{ $title }}


            </h3>
            <div class="pull-right">
                <a href="{{ url('clearHistory') }}"
                   class="btn btn-xs bg-purple pull-right delete-swl">{{ trans('common.clear').' '. trans('common.history') }}</a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-hover" id="dataTables">
                <thead>
                <tr class="active">
                    <th class="col-sm-1">#</th>
                    @if(isset($emailHistories))
                        <th>{!! trans('common.sender') !!}</th>
                        <th>{!! trans('common.email-list') !!}</th>
                        <th>{!! trans('common.email') . ' ' . trans('common.opens') !!}</th>
                        <th>{!! trans('common.click_rate') !!}</th>
                        <th>{!! trans('common.template') !!}</th>
                        <th>{!! trans('common.subject') !!}</th>
                        <th>{!! trans('common.time') !!}</th>
                        <th>{!! trans('common.action') !!}</th>
                    @elseif(isset($email_sent_lists))
                        <th>{{ trans('common.email') }}</th>
                        <th>{{ trans('common.seen_status') }}</th>
                        <th>{{ trans('common.link_clicked') }}</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @if(isset($emailHistories))
                    @foreach($emailHistories as $key => $emailHistory)
                        <tr>
                            <td>{!! $key+1 !!}</td>
                            <!--<td>{{ App\User::find($emailHistory->sender_id) ? \App\User::find($emailHistory->sender_id)->email : 'Deleted User' }}</td>-->
                            <td>{{ App\User::find($emailHistory->sender_id) ? $emailHistory->emailsettings->from_email : 'Deleted User' }}</td>
                            <td><a href="{{ url('sent-email-list/'. $emailHistory->id) }}" data-toggle='tooltip'
                                   data-placement='top'
                                   title='{{ trans('common.opens'). ' ' .trans('common.detail') }}'>{{ count($emailHistory->emailsentlist). ' email(s)' }}</a>
                            </td>
                            <td>
                                    <span class="badge badge-success" data-toggle='tooltip' data-placement='top'
                                          title='{{ trans('common.seen') }}'>{{ seen_unseen_list($emailHistory->id, 1) }}</span>
                                <span class="badge badge-danger" data-toggle='tooltip' data-placement='top'
                                      title='{{ trans('common.unseen') }}'>{{ seen_unseen_list($emailHistory->id, 0) }}</span>
                            </td>
                            <td>
                                <a href="{{ url('sent-email-list/'. $emailHistory->id) }}"><span
                                            class="badge badge-success" data-toggle='tooltip' data-placement='top'
                                            title='{{ trans('common.click_rate') }}'>{{ click_rate($emailHistory->id).'%' }}</span></a>
                            </td>
                            <td>
                                <?php $template_history = \App\Models\Template\Template::find($emailHistory->template_id); ?>
                                @if($template_history)
                                    {{ $template_history->name }}
                                @else
                                    {{ trans('common.empty') }}
                                @endif
                            </td>
                            <td>{{ $emailHistory->subject }}</td>
                            <td id='date_{{ $emailHistory->id}}'></td>
                            
                                                      
                            <td>
                                <a href="#" data-toggle="modal"
                                   data-target="#history_detail_{{ $emailHistory->id }}" class='btn btn-info btn-xs'
                                   data-toggle='tooltip' data-placement='top'
                                   title='{{ trans('common.detail') }}'><i
                                            class='fa fa-list-alt'></i></a>
                            </td>
                        </tr>
                    @endforeach
                @elseif(isset($email_sent_lists))
                    @foreach($email_sent_lists as $key => $email_sent_list)
                        <tr>
                            <td>{!! $key+1 !!}</td>
                            <td>{{ $email_sent_list->email }}</td>
                            <td>{!! ($email_sent_list->seen_status) ? '<span class="label label-success">'.trans('common.seen').'</span>' : '<span class="label label-danger">'.trans('common.unseen').'</span>' !!}</td>
                            <td>{!! ($email_sent_list->link_open) ? '<span class="label label-success">'.trans('common.clicked').'</span>' : '<span class="label label-danger">'.trans('common.not_clicked').'</span>' !!}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    @if(isset($emailHistories))
        @foreach($emailHistories as $emailHistory)
            <div class="modal fade" id="history_detail_{{ $emailHistory->id }}" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"
                                id="myModalLabel">{{ trans('common.history'). ' ' . trans('common.detail')}}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 form-horizontal">
                                    <div class="form-group">
                                        <label for="field-1"
                                               class="col-sm-2 control-label">{!! trans('common.subject') !!}</label>

                                        <div class="col-sm-9">
                                            <span class="form-control">{{ $emailHistory->subject }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1"
                                               class="col-sm-2 control-label">{!! trans('common.email-list') !!}</label>

                                        <div class="col-sm-9">
                                        <textarea
                                                class="form-control">{{ implode(', ', array_column($emailHistory->emailsentlist->toArray(), 'email')) }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1"
                                               class="col-sm-2 control-label">{!! trans('common.template') !!}</label>

                                        <div class="col-sm-9">
                                            <?php $template_modal = \App\Models\Template\Template::find($emailHistory->template_id); ?>
                                            @if($template_modal)
                                                {!! $template_modal->template !!}
                                            @else
                                                {{ trans('common.empty') }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <script type="text/javascript">                
                $("#date_" + '{{ $emailHistory->id}}' ).text(formatAMPM("{{ date('n/j/Y h:i:s A', strtotime($emailHistory->created_at)) . ' UTC'}}"));
        </script>
        @endforeach
    @endif

 
@endsection