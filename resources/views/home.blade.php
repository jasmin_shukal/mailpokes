@extends('app')

@section('htmlheader_title')
    Home
@endsection

@section('main-content')
    <div class="row">
    
        @if(Auth::check() &&  Auth::user()->role == 'superadmin')
            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-6">
                        <div class="col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">{{trans('common.total')}}</span>
                                    <span class="info-box-number">{!! count($activeUsers)+count($bannedUsers) !!}</span>
                                    <a href="{{ url('users') }}"
                                       class="small-box-footer"><?= trans('common.more_info')?> <i
                                                class="fa fa-arrow-circle-right"></i></a>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="ion-ios-person-outline"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">{!! trans('common.active_users') !!}</span>
                                    <span class="info-box-number">{{ count($activeUsers) }}</span>
                                    <a href="{{ url('users/active') }}"
                                       class="small-box-footer"><?= trans('common.more_info')?> <i
                                                class="fa fa-arrow-circle-right"></i></a>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-red"><i class="fa fa-user-times"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">banned user</span>
                                    <span class="info-box-number">{{ count($bannedUsers) }}</span>
                                    <a href="{{ url('users/banned') }}" class="small-box-footer"><?= trans('common.more_info')?>
                                        <i
                                                class="fa fa-arrow-circle-right"></i></a>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow"><i class="fa fa-star"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">{{trans('common.package')}}</span>
                                    @if($package)
                                        <span class="info-box-number">{{ $package->name }}</span>
                                        <a href="{{ url('profile') }}" class="small-box-footer"><?= trans('common.more_info')?>
                                            <i
                                                    class="fa fa-arrow-circle-right"></i></a>
                                    @endif
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-purple"><i class="fa fa-paper-plane"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">{!! trans('common.sent_email') !!}</span>
                                    @if($package)
                                        <span class="info-box-number">{{ $email_sent }}</span>
                                        <a href="{{ url('profile') }}" class="small-box-footer"><?= trans('common.more_info')?>
                                            <i
                                                    class="fa fa-arrow-circle-right"></i></a>
                                    @endif
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-blue"><i class="fa fa-envelope"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">{{ trans('common.remaining_email') }}</span>
                                    @if($package)
                                        <span class="info-box-number">{{ Auth::user()->email_limit }}</span>
                                        <a href="{{ url('profile') }}" class="small-box-footer"><?= trans('common.more_info')?>
                                            <i
                                                    class="fa fa-arrow-circle-right"></i></a>
                                    @endif
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box box-default" style="margin-top: 0px">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recent Registration</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table no-margin" id="DataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Eamil</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($activeUsers) > 0)
                                            @foreach($activeUsers as $key => $User)
                                                <tr>
                                                    <td><a href="{{ url('profile/'.$User->id) }}">{!! $key+1 !!}</a>
                                                    </td>
                                                    <td><i class="fa fa-{{ $User->platform }}"></i> <a
                                                                href="{{ url('profile/'.$User->id) }}">{!! $User->name !!}</a>
                                                    </td>
                                                    <td>{!! (isset($User->email))? $User->email : '' !!}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>{!! trans('common.no_data') !!}</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>

                        </div>
                    </div>

                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>
                    <!-- /.col -->
                </div>
            </div>


        @endif

        @if(Auth::check() &&  Auth::user()->role == 'superadmin')
            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-6">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recent Registartion</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table no-margin" id="DataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Eamil</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($activeUsers) > 0)
                                            @foreach($activeUsers as $key => $User)
                                                <tr>
                                                    <td><a href="{{ url('profile/'.$User->id) }}">{!! $key+1 !!}</a>
                                                    </td>
                                                    <td><i class="fa fa-{{ $User->platform }}"></i> <a
                                                                href="{{ url('profile/'.$User->id) }}">{!! $User->name !!}</a>
                                                    </td>
                                                    <td>{!! (isset($User->email))? $User->email : '' !!}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>{!! trans('common.no_data') !!}</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recent Banned</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table no-margin" id="DataTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Eamil</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($bannedUsers) > 0)
                                            @foreach($bannedUsers as $key => $User)
                                                <tr>
                                                    <td><a href="{{ url('profile/'.$User->id) }}">{!! $key+1 !!}</a>
                                                    </td>
                                                    <td><i class="fa fa-{{ $User->platform }}"></i> <a
                                                                href="{{ url('profile/'.$User->id) }}">{!! $User->name !!}</a>
                                                    </td>
                                                    <td>{!! (isset($User->email))? $User->email : '' !!}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>{!! trans('common.no_data') !!}</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Recent Email List</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="">
                            <div class="table-responsive">
                                <table class="table no-margin" id="DataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{!! trans('common.name') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($groups) > 0)
                                        @foreach($groups as $key => $group)
                                            <tr>
                                                <td><a href="{{ url('email-list/'.$group->id) }}">{!! $key+1 !!}</a>
                                                </td>
                                                <td><a href="{{ url('email-list/'.$group->id) }}" data-toggle='tooltip'
                                                       data-placement='top'
                                                       title='{{ trans('common.detail') }}'>{{ $group->name }}</a> <span
                                                            class="badge" data-toggle='tooltip' data-placement='top'
                                                            title='{{ trans('common.email') }}'>{{ count($group->emaillist) }}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>{!! trans('common.no_data') !!}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Recent Template</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="">
                            <div class="table-responsive">
                                <table class="table no-margin" id="DataTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{!! trans('common.name') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($templates) > 0)
                                        @foreach($templates as $key => $template)
                                            <tr>
                                                <td>{!! $key+1 !!}</td>
                                                <td>
                                                    <a href="#" data-toggle="modal"
                                                       data-target="#template_detail_{{ $template->id }}"
                                                       data-toggle='tooltip' data-placement='top'
                                                       title='{{ trans('common.detail') }}'>{{ $template->name }}
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>{!! trans('common.no_data') !!}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @foreach($templates as $list)
            <div class="modal fade" id="template_detail_{{ $list->id }}" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"
                                id="myModalLabel">{{ $list->name . ' ' . trans('common.template') }}</h4>
                        </div>
                        <div class="modal-body">
                            {!! $list->template !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
