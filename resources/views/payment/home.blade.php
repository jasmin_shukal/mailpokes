@extends('app')

@section('main-content')

    <div class="box box-default" style="margin-top: 0px">
        <!-- Tabs within a box -->
        <div class="box-header with-border">
            <h3 class="box-title pull-left">
                {{ $pageName }}
            </h3>
            
        </div>
        <div class="box-body">
            {!! Form::open(['url' => "payment",'id'=>'payment','class'=>'require-validation', 'role' => 'form', 'data-toggle' => 'form-ajax', 'data-url' => 'No', 'files' => true,'data-cc-on-file'=>'false','data-stripe-publishable-key'=>configValue('stripe_public')]) !!}

                @include('payment._form',['submit_button' => 'Submit'])

            {!! Form::close() !!}
        </div>
    </div>
    <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
<script>

   $(function() {
  $('form.require-validation').bind('submit', function(e) {
       
    if($("#payment").val()=='card')
    {
    var $form         = $(e.target).closest('form'),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;

    $errorMessage.addClass('hide');
    $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault(); // cancel on first error
      }
    });
  }
  });
});

$(document).on('submit','form', function(e) {
    
    if($("#payment").val()=='card')
    {
    $form=$(this);
    
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
    }
  });
  
  function stripeResponseHandler(status, response) {
    if (response.error) {
        $('.error')
            .removeClass('hide')
            .find('.alert')
            .text(response.error.message);
    } else {
        // token contains id, last4, and card type
        var token = response['id'];
        // insert the token into the form so it gets submitted to the server
        $form.find('input[type=text]').empty();
        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
        $form.get(0).submit();
    }
}
</script>
@endsection        