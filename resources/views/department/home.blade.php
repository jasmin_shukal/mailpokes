@extends('app')

@section('main-content')

    <div class="box box-default" style="margin-top: 0px">
        <!-- Tabs within a box -->
        <div class="box-header with-border">
            <h3 class="box-title pull-left">
                {{ $pageName }}
            </h3>
            <a href="" onclick="new_deparment(event)"
               class="pull-right">{{ trans('common.new').' '.$pageName}}</a>
        </div>
        <div class="box-body">
            <table class="table table-hover" id="department-table">
                <thead>
                    <tr class="active">
                        <th class="col-sm-1">#</th>
                        <th>{{ trans('common.name') }}</th>
                        <th>{{ trans('common.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($departments as $key =>$department)
                    <tr>
                        <td>{!! $key+1 !!}</td>
                        <td>{{ $department->name }}</td>
                        <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['departments.destroy', $department->id], 'class' => 'delete-form']) !!}
                            <a href="" onclick="edit_department(event, '{{ $department->id }}')"
                               class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                            {!! btn_delete_submit() !!}

                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="department_modal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="myModalLabel">{{ $pageName }}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => "departments",'id'=>'department_form','class'=>'form-horizontal', 'role' => 'form', 'data-toggle' => 'form-ajax', 'data-url' => 'No', 'files' => true]) !!}

                    @include('department._form',['submit_button' => 'Submit'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var export_filename = '{{ request()->segment(1) }}';

            $('#department-table').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: 'Copy',
                        extend: 'copy',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    }, {
                        text: 'CSV',
                        extend: 'csvHtml5',
                        title: export_filename,
                        extension: '.csv',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    }, {
                        text: 'XLS',
                        extend: 'excelHtml5',
                        title: export_filename,
                        extension: '.xls',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    }, {
                        text: 'PDF',
                        extend: 'pdf',
                        title: export_filename,
                        extension: '.pdf',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    }, {
                        text: 'Print',
                        extend: 'print',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    }
                ]
            });
        });        

        function edit_department(e, id) {
            e.preventDefault();
            var update_id = id;
            var url = "{{ url('departments') }}";
            $.ajax({
                url: url + '/' + update_id + '/edit',
                method: 'get',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (response) {
                    if(response.error){
                        swal({
                                title: "Warning!",
                                text: response.msg,
                                type: "warning",
                                confirmButtonText: "OK",
                            },
                            function (isConfirm) {
                                location.reload();
                            });
                    } else {
                        $.each(response.data, function (key, value) {
                            if(key == 'user_id') {
                                $('#select_customer').select2('val', $.parseJSON(value));
                            } else {
                                $('#' + key).val(value);
                            }
                        });
                    }
                },
            });

            $('#update_id').val(update_id);
            $('#submit_btn').val('{{ trans('common.update') }}');
            $('#department_modal').modal('show');

            
        }

        function submit_form(e) {
            e.preventDefault();
            var formData = new FormData($('#department_form')[0]);
            var that = this;
            var url = "{{ url('departments') }}";
            var update_id = $('#update_id').val();
            // alert(update_id);return;
            if (update_id > 0) {
                url = url + '/' + update_id;
                formData.append('_method', 'PUT');
            }
            // alert(url);

            $.ajax({
                url: url,
                method: 'POST',
                dataType: 'json',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    if(response.error) {
                        swal({
                                title: "Warning!",
                                text: response.msg,
                                type: "warning",
                                confirmButtonText: "OK",
                            },
                            function (isConfirm) {
                                location.reload();
                            });
                    } else {
                        swal({
                                title: "Saved!",
                                text: response.msg,
                                type: "success",
                                confirmButtonText: "OK",
                            },
                            function (isConfirm) {
                                location.reload();
                            });
                    }
                },
                error: function (errors) {
                    $.each(errors.responseJSON, function (key, error) {
                        $('#' + key + '_error').text(error[0]);
                    });
                }
            });
        }

        function new_deparment(e) {
            e.preventDefault();
            $('#department_form')[0].reset();
            $('#department_modal').modal('show');
        }
    </script>

@endsection