<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Divyang Patel">
        <link href="//fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">
        <title>Mailpokes Email Marketing</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('/new_home/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/new_home/css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ asset('/new_home/css/templatemo-softy-pinko.css') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('plugins/pricing/images/Mailpokes-icon-114-114.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('plugins/pricing/images/Mailpokes-icon-72-72.png') }}">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('plugins/pricing/images/Mailpokes-icon.png') }}">
        <style>
            .container{
                max-width: 85%;
            }
            .div-info{
                text-align: center;
                color: #FFFFFF;
                margin-bottom: 40px;
            }
            .div-ul{
                text-align: left;
                font-weight: 500;
            }
            .div-icon-img{
                width: 30px;
                height: auto;
                position: absolute;
                right: 25px;
                top: 30px;
            }
            @media only screen and (max-width: 1440px) {
                .welcome-area .header-text h1{
                    font-size: 23px;
                    line-height: 36px;
                    margin-bottom: 15px;
                }
                .welcome-area .header-text p{
                    font-size: 15px;
                    line-height: 23px;
                    margin-bottom: 20px;
                }
                .home-feature{
                    margin-top: -335px;
                }
            }
        </style>
    </head>
    <body>
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="{{ url('/') }}" class="logo">
                            <img style="height: 65px;" class="img-fluid" src="{{ asset('plugins/pricing/images/Mailpokes.png') }}" alt="logo">
                        </a>
                        <ul class="nav">
                            <li><a href="#welcome" class="active">Home</a></li>
                            <li><a href="#features">Features</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#pricing-plans">Pricing Tables</a></li>
                            <li><a href="#contact-us">Contact Us</a></li>
                            <li><a href="{{url('login')}}">{{ trans('auth.log_in') }}</a></li>
                            <li><a href="{{url('register')}}">{{ trans('auth.Register') }}</a></li>
                        </ul>
                        <a class='menu-trigger'><span>Menu</span></a>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div class="welcome-area" id="welcome">
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                        <img style="width: 40%;" class="img-fluid" src="{{ asset('plugins/pricing/images/Mailpokes.png') }}" alt="logo">
                        <h1>We <strong>Specialise</strong> in <strong>Bulk Email Marketing</strong><br/>and <strong>Email Feedback System Integration</strong>.</h1>
                        <p>Our state of the art software provides you with a modern user interface which is easy to use and efficient. We offer competitive pricing along with lightning prompt support.</p>
                        <a href="#features" class="main-button-slider">Discover More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="section home-feature" id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="info-text-center">
                                <h1 style="margin-bottom: -92px;margin-top: 166px;">Features</h1>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                            <div class="features-small-item">
                                <div class="icon">
                                    <img class="img-fluid" style="width: 65px;" src="{{ asset('plugins/pricing/images/Mailpokes-icon.png') }}" alt="">
                                </div>
                                <? /*<h5 class="features-title">Modern Strategy</h5> */ ?>
                                <ul class="div-ul">
                                    <li>• Easy to use Email Marketing Tool</li>
                                    <li>• Create Email Template with rich editor</li>
                                    <li>• Send Unlimited Email with your Created Template.</li>
                                    <li>• Multiple SMTP & PHP Mail</li>
                                    <li>• Amazon SES, Mailgun Integration</li>
                                    <li>• Maildrill SMTP Integration</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.6s">
                            <div class="features-small-item">
                                <div class="icon">
                                    <img class="img-fluid" style="width: 65px;" src="{{ asset('plugins/pricing/images/Mailpokes-icon.png') }}" alt="">
                                </div>
                                <? /*<h5 class="features-title">Ultimate Marketing</h5> */ ?>
                                <ul class="div-ul">
                                    <li>• Create Email Group/ list</li>
                                    <li>• Upload Email list form CSV file</li>
                                    <li>• Nice & clean email list / group management</li>
                                    <li>• Login with Social Media.</li>
                                    <li>• Mobile compatible.</li>
                                    <li style="width: 124px">• Multi language support.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.4s">
                            <div class="features-small-item">
                                <div class="icon">
                                    <img class="img-fluid" style="width: 65px;" src="{{ asset('plugins/pricing/images/Mailpokes-icon.png') }}" alt="">
                                </div>
                                <? /*<h5 class="features-title">Best Relationship</h5> */ ?>
                                <ul class="div-ul">
                                    <li>• Beautiful Dashboard.</li>
                                    <li>• Track user activity</li>
                                    <li>• Manage User.</li>
                                    <li>• Schedule Email.</li>
                                    <li>• Validate email address with our own system for free</li>
                                    <li>• Validate with bulkemailchecker, emaillistverify</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section colored" id="pricing-plans">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="center-heading">
                        <h2 class="section-title">Pricing Plans</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($packages as $key => $package)
                <div class="col-lg-3 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                    <?php
                        if($package->id =='1') { $button=asset('/new_home/images/check.png'); }
                        else { $button=asset('/new_home/images/cross.png'); }
                    ?>
                    <img class="div-icon-img" src="<?=$button?>">
                    <div class="pricing-item @if (($key%2) != 0) active @endif">
                        <div class="pricing-header">
                            <h3 class="pricing-title">{{ $package->name }}</h3>
                        </div>
                        <div class="pricing-body">
                            <div class="price-wrapper">
                                <span class="currency">$</span>
                                <span class="price">{{ $package->price }}</span>
                                <span class="period">{{ $package->validity . ' ' .trans('common.days') }}</span>
                            </div>
                            <ul class="list">
                                <li class="active">{{ trans('common.limit') }} {{ $package->limit }} {{ trans('common.emails') }}</li>
                                <li class="active">{{ trans('common.limit') }} {{ $package->user_limit }} {{ trans('common.user(s)') }}</li>
                                <li class="active">{{ trans('common.validity') }} {{ $package->validity }} {{ trans('common.days') }}</li>
                            </ul>
                        </div>
                        <div class="pricing-footer">
                            <a href="{{ url('package'. '/'. $package->id )}}" class="main-button">Purchase Now</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="section padding-top-70 padding-bottom-0" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="info text-center">
                        <h1 style= "margin-bottom: -100px;margin-top: -110px;">About Us</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-12 col-sm-12 align-self-center" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="{{ asset('/new_home/images/left-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-top-fix">
                    <div class="left-heading">
                        <h2 class="section-title">We are both analytical and creative</h2>
                    </div>
                    <div class="left-text">
                        <p>We communicate, design, develop and publish… to influence, experience, move and inspire. We understand that there’s more to marketing than making you look good – we go to work every day to help grow your business.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section colored padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-bottom-fix">
                    <div class="left-heading">
                        <h2 class="section-title">mailpokes.com is where the magic happens</h2>
                    </div>
                    <div class="left-text">
                        <p>Our forward thinking design team will take your vision of generating more revenue  and translate it into the bottom line that not only represents who you are, but captivates and speaks to your future.</p>
                    </div>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-5 col-md-12 col-sm-12 align-self-center mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="{{ asset('/new_home/images/right-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
            </div>
        </div>
    </section>
    <section class="mini" id="work-process">
        <div class="mini-content">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-3 col-lg-6">
                        <div class="info">
                            <h1 style="margin-top: -205px;margin-bottom: -100px;">Work Process</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i><img src="{{ asset('/new_home/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>Create<br/>Account</strong>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i><img src="{{ asset('/new_home/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>Buy or Upgrade<br/>Package</strong>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i><img src="{{ asset('/new_home/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>Create Groups & Upload Contacts</strong>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i><img src="{{ asset('/new_home/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>Create<br/>Email Templates</strong>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i><img src="{{ asset('/new_home/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>Send / Schedule the Campaign</strong>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-6">
                        <a href="#" class="mini-box">
                            <i><img src="{{ asset('/new_home/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>View<br/>Reports</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section" id="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="center-heading">
                        <h2 class="section-title">What do they say?</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="team-item" style="height: 430px;">
                        <div class="team-content">
                            <i><img src="{{ asset('/new_home/images/testimonial-icon.png') }}" alt=""></i>
                            <p>One of the best email marketing portal which we have used. Excellent Service.</p>
                            <div class="user-image">
                                <img src="http://placehold.it/60x60" alt="">
                            </div>
                            <div class="team-info">
                                <h3 class="user-name">Sandip Patel</h3>
                                <span style="padding: 26px;">ESPI, Managing Director</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="team-item" style="height: 430px;">
                        <div class="team-content">
                            <i><img src="{{ asset('/new_home/images/testimonial-icon.png') }}" alt=""></i>
                            <p>Email Automation at its best. The Scheduling feature as of great help. Must Recommend..!!</p>
                            <div class="user-image">
                                <img src="http://placehold.it/60x60" alt="">
                            </div>
                            <div class="team-info">
                                <h3 class="user-name">Hardik Ray</h3>
                                <span>E-Commerce Cosultant</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="team-item">
                        <div class="team-content">
                            <i><img src="{{ asset('/new_home/images/testimonial-icon.png') }}" alt=""></i>
                            <p>Best Email Marketing Solution I have used. Thrid Party API Integration is seamless.</p>
                            <div class="user-image">
                                <img src="http://placehold.it/60x60" alt="">
                            </div>
                            <div class="team-info">
                                <h3 class="user-name">Jeegar Desai</h3>
                                <span>ASC, Director</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="section colored" id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="center-heading">
                        <h2 class="section-title">Send Us an Enquiry</h2>
                    </div>
                </div>
                <div class="offset-lg-3 col-lg-6">
                    <div class="center-text">
                        <p>For Enterprise Package or A Custom Package<br/>Please Fill-up the form and Send us an Enquiry. Thank you.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class=" offset-lg-2 col-lg-8 offset-md-4 col-md-4 col-sm-12">
                    <div class="contact-form">
                        <form id="contact" action="{{ url('/') }}/mail.php" method="POST">
                          <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                              <fieldset>
                                <input name="name" type="text" class="form-control" id="name" placeholder="Full Name" required="">
                              </fieldset>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                              <fieldset>
                                <input name="email" type="email" class="form-control" id="email" placeholder="E-Mail Address" required="">
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your Message" required=""></textarea>
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" id="form-submit" class="main-button">Send Message</button>
                              </fieldset>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">Copyright &copy; <?=date('Y')?> Mailpokes Email Marketing | Designed & Developed : <a href="https://www.anantsoftcomputing.com/">Anant Soft Computing</a></p>
                </div>
            </div>
        </div>
    </footer>
    <script src="{{ asset('/new_home/js/jquery-2.1.0.min.js') }}"></script>
    <script src="{{ asset('/new_home/js/popper.js') }}"></script>
    <script src="{{ asset('/new_home/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/new_home/js/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('/new_home/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('/new_home/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('/new_home/js/imgfix.min.js') }}"></script> 
    <script src="{{ asset('/new_home/js/custom.js') }}"></script>
  </body>
</html>