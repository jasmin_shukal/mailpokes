@extends('app')

@section('main-content')
<style>
    .select_btn_set
    {
        margin-bottom: 1%;
    }
    .bootstrap-tagsinput
    {
        width: 100%!important;
        height: 120px;
        overflow-x: auto;
    }
    select.select{
    position:relative;
    z-index:10;
    width:166px !important;
    height:26px !important;
    line-height:26px;
}

/* dynamically created SPAN, placed below the SELECT */
span.select{
    position:absolute;
    bottom:0;
    float:left;
    left:0;
    width:166px;
    height:26px;
    line-height:26px;
    text-indent:10px;
    background:url(images/bg_select.gif) no-repeat 0 0;
    cursor:default;
    z-index:1;
	}
    </style>
    <link rel="stylesheet" href="{{URL::asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
    <script src="{{URL::asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

    
    <div class="ajax-overlay"><p><i class="fa fa-spin fa-refresh fa-5x"></i></p></div>
    @if(Session::has('block_errors'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ Session::get('block_errors') }}
        </div>
    @endif
    @if(isset($email_settings_info))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <a href="{{ url('email') }}" style="text-decoration: none; color: #a94442">{{ $email_settings_info }}</a>
        </div>
    @endif
    <div style="margin-top: 0px;" class="box box-default">
        <!-- Tabs within a box -->
        <div class="box-header with-border">
            <h3 class="box-title pull-left">
                {!! trans('common.send').' '.trans('common.email')  !!}
            </h3>
        </div>

        <div class="box-body">
            {!! Form::open(['url' => "send-mail",'id'=>'send-mail','class'=>'form-horizontal send-email-form', 'role' => 'form', 'data-toggle' => 'form-ajax', 'data-url' => 'No', 'files' => true]) !!}


            @include('sendMail.single_form',['submit_button' => 'Send e-mail'])

            {!! Form::close() !!}
        </div>

    </div>

    @foreach($template_lists as $list)
        <div class="modal fade" id="template_detail_{{ $list->id }}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"
                            id="myModalLabel">{{ $list->name . ' ' . trans('common.template') }}</h4>
                    </div>
                    <div class="modal-body">
                        {!! $list->template !!}
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tagify/3.21.5/tagify.min.js" integrity="sha512-HlsH/grgbtvw8pW6/FViblU2JJlbucoHK4ju0xxMRdSC00MjD/rrPvViL9HWYUv++Efo/unAsMasqB8yWeAd9w==" crossorigin="anonymous"></script>
    <script>
        function show_template() {
            var template_id = $('#select_template_single').val();
            $('#template_detail_' + template_id).modal('show');
        }


        $(function(){
            $('#timezone').val(-new Date().getTimezoneOffset()/60);

        });

        $('.send-email-form').submit(function (e) {
            //            e.preventDefault();
            $('.ajax-overlay').show();
            return true;
        });


        $('#schedule').click(function (e) {
            e.preventDefault();
            if ($('#datetime_div').is(':hidden')) {
                $('#datetime_div').slideDown('slow');
            } else {
                $('#datetime_div').slideUp('slow');
            }
        });

        $('.schedule_group').click(function (e) {
            e.preventDefault();
            if ($('#datetime_group_div').is(':hidden')) {
                $('#datetime_group_div').slideDown('slow');
                $('#remove_schedule_group').slideDown('slow');
                $('#div_schedule_group').slideUp('slow');
                $('#send_later_identifier_group').val(1);
            } else {
                $('#div_schedule_group').slideDown('slow');
                $('#datetime_group_div').slideUp('slow');
                $('#remove_schedule_group').slideUp('slow');
                $('#send_later_identifier_group').val(0);
            }
        });

        $('.schedule_email').click(function (e) {
            e.preventDefault();
//            alert(-new Date().getTimezoneOffset());
            if ($('#datetime_email_div').is(':hidden')) {
                $('#datetime_email_div').slideDown('slow');
                $('#remove_schedule_email').slideDown('slow');
                $('#div_schedule_email').slideUp('slow');
                $('#send_later_identifier_email').val(1);
            } else {
                $('#div_schedule_email').slideDown('slow');
                $('#datetime_email_div').slideUp('slow');
                $('#remove_schedule_email').slideUp('slow');
                $('#send_later_identifier_email').val(0);
            }
        });

        $("#select2_custom_select").change(function(e){
            // $('.ajax-overlay').show();
            // this.value
            $.ajax({
                url: "{{url('send-mail/group_detail/')}}/"+this.value, 
                success: function(result){
                    var array = $.map(result, function(value, index) {
                        return [value];
                    });
                    append_email(array);

                    // let string = array.toString();
                    // $("#emails").html(string);
                }});
        });

        // jQuery
        $( document ).ready(function() {
            $("#email").tagsinput("jasmin@gmail.com");
        });
        // $('#email').tagify();

        // // Vanilla JavaScript
        // var input = document.querySelector('input[name=tags]'),
        // tagify = new Tagify( input );

        function append_email(key)
        {
            val_currunt =$("#emails").val();
            if(val_currunt!="")
            {
                var numbersArray = val_currunt.split(',');
                var array3 = arrayUnique(numbersArray.concat(key));
            }
            else
            {
                array3=key;
            }
            $('#emails').tagsinput('add', array3.toString());
            // $("#emails").val(array3.toString());
        }

        function arrayUnique(array) {
            var a = array.concat();
            for(var i=0; i<a.length; ++i) {
                for(var j=i+1; j<a.length; ++j) {
                    if(a[i] === a[j])
                        a.splice(j--, 1);
                }
            }

            return a;
        }

        $(".select_btn_set").click(function(e){
            $("#select2_custom_select").val($(this).data("id")).change();
        });
    </script>

@endsection